package fr.tristiisch.emeraldmc.skyfight.fakeinventory;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;

import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools.GuiData;

public class FakeInventoryListener implements Listener {

	@EventHandler
	private void InventoryClickEvent(final InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();
		final Inventory clickedInventory = event.getClickedInventory();

		if(clickedInventory == null) {
			return;
		}

		final GuiData guiData = GuiTools.getGuiData(player);
		if(guiData != null) {
			switch(guiData.getId()) {
			case "fakeInventory":
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	private void PlayerQuitEvent(final PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		FakeInventorys.removeInventorysFor(player);

	}
}

package fr.tristiisch.emeraldmc.skyfight.fakeinventory;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;

public class FakeInventoryCommand implements CommandExecutor {

	public static String getCommand() {
		return "fakeinventory";
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		if(!(sender instanceof Player)) {
			return true;
		}
		final Player player = (Player) sender;
		if(args.length == 1) {
			GuiTools guiTools;
			guiTools = FakeInventorys.getInventory(args[0]);
			if(guiTools == null) {
				player.sendMessage(Utils.color("&2EmeraldMC &7» &cUne erreur est survenu."));
				return true;
			}
			guiTools.openInventory(player);
		}

		return true;
	}

}

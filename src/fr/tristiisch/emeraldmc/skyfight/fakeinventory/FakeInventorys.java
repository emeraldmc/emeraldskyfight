package fr.tristiisch.emeraldmc.skyfight.fakeinventory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Matcher;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;

public class FakeInventorys {

	private static Map<UUID, GuiTools> inventorys = new HashMap<>();

	public static UUID addInventory(final GuiTools guiTools) {
		UUID uuid;
		do {
			uuid = UUID.randomUUID();
		} while(inventorys.containsKey(uuid));
		inventorys.put(uuid, guiTools);
		return uuid;
	}

	public static GuiTools getInventory(final String uuidString) {
		if(!Matcher.isUUID(uuidString)) {
			return null;
		}
		final UUID uuid = UUID.fromString(uuidString);
		return inventorys.get(uuid);
	}

	public static List<Entry<UUID, GuiTools>> getInventorysFor(final Player player) {
		return inventorys.entrySet().stream().filter(entry -> entry.getValue().getData().equals(player.getUniqueId().toString())).collect(Collectors.toList());
	}

	public static void removeInventorysFor(final Player player) {
		for(final Entry<UUID, GuiTools> entry : getInventorysFor(player)) {
			inventorys.remove(entry.getKey());
		}
	}
}

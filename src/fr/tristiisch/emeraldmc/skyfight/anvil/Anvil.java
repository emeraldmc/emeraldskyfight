package fr.tristiisch.emeraldmc.skyfight.anvil;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.ChatMessage;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.PacketPlayOutOpenWindow;

public class Anvil {

	public static void openAnvilInventory(final Player player) {

		final EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
		final FakeAnvil fakeAnvil = new FakeAnvil(entityPlayer);
		final int containerId = entityPlayer.nextContainerCounter();

		((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutOpenWindow(containerId, "minecraft:anvil", new ChatMessage("Repairing", new Object[] {}), 0));

		entityPlayer.activeContainer = fakeAnvil;
		entityPlayer.activeContainer.windowId = containerId;
		entityPlayer.activeContainer.addSlotListener(entityPlayer);
		entityPlayer.activeContainer = fakeAnvil;
		entityPlayer.activeContainer.windowId = containerId;

		/*final Inventory inv = fakeAnvil.getBukkitView().getTopInventory();*/
		/*inv.setItem(0, new ItemStack(Material.PAPER)); //Remove this if you don't want to add stuff into slots. This is what I needed it for my GUI.
		 */
	}
}

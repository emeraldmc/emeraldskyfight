package fr.tristiisch.emeraldmc.skyfight.anvil;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;

public class AnvilListener implements Listener {

	@EventHandler
	public void PlayerInteractEvent(final PlayerInteractEvent event){
		if(event.isCancelled()){
			return;
		}
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock() != null && event.getClickedBlock().getType() == Material.ANVIL) {
			Anvil.openAnvilInventory(event.getPlayer());
			event.setCancelled(true);
		}
	}


	@EventHandler(priority = EventPriority.MONITOR)
	public void onInventoryClick(final InventoryClickEvent event){
		if(event.isCancelled()) {
			return;
		}

		final Inventory inv = event.getInventory();

		if(!(inv instanceof AnvilInventory)) {
			return;
		}
		final AnvilInventory anvilInventory = (AnvilInventory) inv;
		final InventoryView view = event.getView();
		final int rawSlot = event.getRawSlot();

		// compare the raw slot with the inventory view to make sure we are talking about the upper inventory
		if(rawSlot == view.convertSlot(rawSlot)){
			/*
	slot 0 = left item slot
	slot 1 = right item slot
	slot 2 = result item slot

	see if the player clicked in the result item slot of the anvil inventory
			 */
			if(rawSlot == 2){
				/*
	get the current item in the result slot
	I think inv.getItem(rawSlot) would be possible too
				 */
				final ItemStack item = event.getCurrentItem();
				ItemStack firstItem = anvilInventory.getItem(0);

				// check if there is an item in the result slot
				if(item != null && firstItem != null) {
					firstItem = ItemTools.setRepairCost(firstItem, 1);
					anvilInventory.setItem(0, firstItem);
					final ItemMeta meta = item.getItemMeta();
					final ItemMeta metaFirstItem = firstItem.getItemMeta();

					// it is possible that the item does not have meta data
					if(meta != null && metaFirstItem != null) {
						// see whether the item is beeing renamed
						if(meta.hasDisplayName() && metaFirstItem.hasDisplayName()) {
							final String firstItemName = metaFirstItem.getDisplayName();
							final ChatColor color = ChatColor.getByChar(firstItemName.charAt(1));
							if(color != null && firstItemName.substring(1).equals(meta.getDisplayName())) {
								meta.setDisplayName(color + meta.getDisplayName().substring(1));
								item.setItemMeta(meta);
							}
						}
					}
				}
			}
		}

	}



	/*
		@EventHandler
		public void InventoryOpenEvent(final InventoryOpenEvent event){
			final Inventory inventory = event.getInventory();
			if(!(inventory instanceof AnvilInventory)){
				return;
			}
			final Player player = (Player) event.getPlayer();
			if(event.isCancelled()){
				this.anvils.remove(player);
			}
		}
		@EventHandler
		public void InventoryCloseEvent(final InventoryCloseEvent event){
			final Inventory inventory = event.getInventory();
			if(!(inventory instanceof AnvilInventory)){
				return;
			}
			this.anvils.remove(event.getPlayer());
		}

		@SuppressWarnings("deprecation")
		@EventHandler
		public void InventoryClickEvent(final InventoryClickEvent event){
			final Inventory inventory = event.getInventory();
			if(!(inventory instanceof AnvilInventory)){
				return;
			}
			final Player player = (Player) event.getWhoClicked();
			if(this.anvils.containsKey(player)) {
				final Location location = this.anvils.get(player);
				final Block block = location.getBlock();
				final byte data = block.getData();
				block.setType(Material.ANVIL);
				block.setData(data, true);
			}
		}*/

}

package fr.tristiisch.emeraldmc.skyfight.enderpearl;

import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Projectile;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.entity.Player;
import java.util.ArrayList;
import org.bukkit.event.Listener;

public class NoFallDamage implements Listener {

	private ArrayList<Player> playersToLastTeleport = new ArrayList<>();
	
	
	@EventHandler
	public void onQuit(final PlayerQuitEvent e) {
		if (this.playersToLastTeleport.contains(e.getPlayer())) {
			this.playersToLastTeleport.remove(e.getPlayer());
		}
	}
	
	@EventHandler
	public void onHit(final ProjectileHitEvent e) {
		final Projectile pr = e.getEntity();
		if (!pr.getType().equals((Object)EntityType.ENDER_PEARL)) {
			return;
		}
		if (!(pr.getShooter() instanceof Player)) {
			return;
		}
		if (!pr.getType().equals((Object)EntityType.ENDER_PEARL)) {
			return;
		}
		if (!(pr.getShooter() instanceof Player)) {
			return;
		}
		final Player player = (Player)pr.getShooter();
		if (!this.playersToLastTeleport.contains(player)) {
			this.playersToLastTeleport.add(player);
			for (Double d = 0.0; d < 4.5; ++d) {
				final Block block = player.getLocation().add(0.0, -d, 0.0).getBlock();
				if (!block.getType().equals((Object)Material.AIR)) {
					this.playersToLastTeleport.remove(player);
				}
			}
		}
	}
	
	@EventHandler
	public void onEntityDamageEvent(final EntityDamageEvent e) {
		if (!(e.getEntity() instanceof Player)) {
			return;
		}
		if (e.getCause() != EntityDamageEvent.DamageCause.FALL) {
			return;
		}
		if (e.getEntity() instanceof Player) {
			final Player player = (Player)e.getEntity();
			if (!this.playersToLastTeleport.contains(player)) {
				return;
			}
			e.setCancelled(true);
			this.playersToLastTeleport.remove(player);
		}
	}
}

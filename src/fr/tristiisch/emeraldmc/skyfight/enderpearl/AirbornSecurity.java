package fr.tristiisch.emeraldmc.skyfight.enderpearl;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;
import fr.tristiisch.emeraldmc.api.spigot.title.ActionBar;
	
public class AirbornSecurity implements Listener {

	private HashMap<Player, Projectile> playersAirborn = new HashMap<>();
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		if (this.isPlayersAirborn(event.getPlayer())) {
			this.removePlayersAirborn(event.getPlayer());
		}
	}
	
	@EventHandler
	public void onHit(ProjectileHitEvent event) {
		Projectile projectile = event.getEntity();
		if (!(projectile.getShooter() instanceof Player)) {
			return;
		}
		if (projectile.getType().equals(EntityType.ENDER_PEARL)) {
			if (!(projectile.getShooter() instanceof Player)) {
				return;
			}
			Player player = (Player) projectile.getShooter();
			
			this.removePlayersAirborn(player);
		}
	}
	
	@EventHandler
	public void PlayerInteractEvent(PlayerInteractEvent event) {
		if (event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) {
			return;
		}
		if (!event.hasItem() || event.getItem().getType() != Material.ENDER_PEARL) {
			return;
		}
		Player player = event.getPlayer();
		Projectile projectile = this.getPlayersAirborn(player);
		if(projectile != null && !projectile.isDead() && projectile.getLocation().getBlockY() > 0) {
			ActionBar.sendActionBar(player, "&c&lVous ne pouvez utiliser qu'une seul enderpearl à la fois");
			event.setCancelled(true);
		} else {
			this.removePlayersAirborn(player);
		}
	}
	
	@EventHandler
	public void ProjectileLaunchEvent(ProjectileLaunchEvent event) {
		if(event.getEntityType() != EntityType.ENDER_PEARL) {
			return;
		}
		Projectile projectile = event.getEntity();
		if(!(projectile.getShooter() instanceof Player)) {
			return;
		}
		Player player = (Player) projectile.getShooter();
		if (!this.isPlayersAirborn(player)) {
			this.setPlayersAirborn(player, projectile);
		}
/*		Main.getInstance().getServer().getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
			@Override
			public void run() {
				if(projectile.isDead()) {
					Main.getInstance().getServer().getScheduler().cancelTask(id);
				}
				projectile.getWorld().playEffect(projectile.getLocation(), Effect.SMOKE, 0);
			}
		}, 0, 1000);*/
		String taskname = TaskManager.getTaskName("EnderPearlRunnable");
		TaskManager.scheduleSyncRepeatingTask(taskname, new EnderPearlRunnable(taskname, projectile), 0, 1 / 2);
		/*Main.getInstance().getServer().getScheduler().scheduleSyncRepeatingTask();*/
	}
	
	public boolean isPlayersAirborn(Player player) {
		return this.playersAirborn.containsKey(player);
	}
	
	public Projectile getPlayersAirborn(Player player) {
		return this.playersAirborn.get(player);
	}
	
	public void setPlayersAirborn(Player player, Projectile projectile) {
		this.playersAirborn.put(player, projectile);
	}
	
	public void removePlayersAirborn(Player player) {
		this.playersAirborn.remove(player);
	}
	
	public void clearPlayersAirborn() {
		this.playersAirborn.clear();
	}
}

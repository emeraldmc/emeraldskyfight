package fr.tristiisch.emeraldmc.skyfight.enderpearl;

import org.bukkit.Effect;
import org.bukkit.entity.Projectile;

import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;

public class EnderPearlRunnable implements Runnable {

	private String name;
	private Projectile projectile;

	public EnderPearlRunnable(String name, Projectile projectile) {
		this.name = name;
		this.projectile = projectile;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		if(projectile.isDead()) {
			TaskManager.cancelTaskByName(name);
			return;
		}
		for(int i = 0; i < 100; i++) {
			projectile.getWorld().playEffect(projectile.getLocation(), Effect.COLOURED_DUST, 0);
		}
	}

}

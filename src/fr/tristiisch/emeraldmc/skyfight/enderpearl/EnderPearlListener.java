package fr.tristiisch.emeraldmc.skyfight.enderpearl;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class EnderPearlListener implements Listener {

	private final Map<Player, Location> playersToLastTime = new HashMap<>();
	/*private HashMap<Player, Projectile> projectiles = new HashMap<>();*/


	public void debug(final Player player) {
		final Block by = player.getLocation().getBlock();
		if(!by.getType().equals(Material.AIR) && this.playersToLastTime.containsKey(player)) {
			player.teleport(this.playersToLastTime.get(player));
			this.playersToLastTime.remove(player);
		}
	}

	@EventHandler
	public void ProjectileLaunchEvent(final ProjectileLaunchEvent event) {
		final Projectile pro = event.getEntity();
		if (!pro.getType().equals(EntityType.ENDER_PEARL)) {
			return;
		}
		if (pro.getType().equals(EntityType.ENDER_PEARL)) {
			final Player player = (Player) pro.getShooter();
			final Location loc = player.getLocation();
			this.playersToLastTime.put(player, loc);
		}
	}

	@EventHandler
	public void PlayerQuitEvent(final PlayerQuitEvent event) {
		this.playersToLastTime.remove(event.getPlayer());
	}

	@EventHandler
	public void EntityDamageByEntityEvent(final EntityDamageByEntityEvent event) {
		final Entity damager = event.getDamager();
		if(damager.getType().equals(EntityType.ENDER_PEARL)) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void ProjectileHitEvent(final ProjectileHitEvent event) {
		final Projectile projectile = event.getEntity();

		if (projectile.getType().equals(EntityType.ENDER_PEARL)) {
			if (!(projectile.getShooter() instanceof Player)) {
				return;
			}
			final Player player = (Player) projectile.getShooter();
			final Block bxn = player.getLocation().add(-1.0, 0.0, 0.0).getBlock();
			final Block bxn2 = player.getLocation().add(-0.5, 0.0, 0.0).getBlock();
			final Block bxp = player.getLocation().add(1.0, 0.0, 0.0).getBlock();
			final Block bxp2 = player.getLocation().add(0.5, 0.0, 0.0).getBlock();
			final Block bzn = player.getLocation().add(0.0, 0.0, -1.0).getBlock();
			final Block bzn2 = player.getLocation().add(0.0, 0.0, -0.5).getBlock();
			final Block bzp = player.getLocation().add(0.0, 0.0, 1.0).getBlock();
			final Block bzp2 = player.getLocation().add(0.0, 0.0, 0.5).getBlock();
			final Block by = player.getLocation().getBlock();
			final Block by2 = player.getLocation().add(0.0, 1.0, 0.0).getBlock();
			if (!by2.getType().equals(Material.AIR)) {
				for (boolean isOnLand = false; !isOnLand; isOnLand = true) {
					player.teleport(player.getLocation().add(player.getLocation().getDirection().multiply(-1.0)));
					if (by2.getType() != Material.AIR) {
						player.teleport(player.getLocation().add(player.getLocation().getDirection().multiply(-1.0)));
						this.debug(player);
					}
				}
			}
			if (!by.getType().equals(Material.AIR)) {
				player.teleport(player.getLocation().add(0.0, 0.5, 0.0));
			}
			if (!bxn.getType().equals(Material.AIR) || !bxn2.getType().equals(Material.AIR)) {
				player.teleport(player.getLocation().add(0.5, 0.0, 0.0));
			}
			if (!bxp.getType().equals(Material.AIR) || !bxp2.getType().equals(Material.AIR)) {
				player.teleport(player.getLocation().add(-0.5, 0.0, 0.0));
			}
			if (!bzn.getType().equals(Material.AIR) || !bzn2.getType().equals(Material.AIR)) {
				player.teleport(player.getLocation().add(0.0, 0.0, 0.5));
			}
			if (!bzp.getType().equals(Material.AIR) || !bzp2.getType().equals(Material.AIR)) {
				player.teleport(player.getLocation().add(0.0, 0.0, -0.5));
			}
		}
	}
}

package fr.tristiisch.emeraldmc.skyfight.enchant;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.EnchantingInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Dye;

import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;

public class EnchantListener implements Listener {

	@EventHandler
	public void EnchantItemEvent(final EnchantItemEvent event) {
		if(event.getInventory() instanceof EnchantingInventory) {
			TaskManager.runTaskLater(() -> this.setLapis((EnchantingInventory) event.getInventory()), 1);
		}
	}

	@EventHandler
	public void InventoryOpenEvent(final InventoryOpenEvent event) {
		if(event.getInventory() instanceof EnchantingInventory) {
			this.setLapis((EnchantingInventory) event.getInventory());
		}
	}

	@EventHandler
	public void InventoryCloseEvent(final InventoryCloseEvent event) {
		if(event.getInventory() instanceof EnchantingInventory) {
			this.removeLapis((EnchantingInventory) event.getInventory());
		}
	}

	@EventHandler
	public void InventoryClickEvent(final InventoryClickEvent event) {
		if(event.getInventory() instanceof EnchantingInventory && event.getRawSlot() == 1) {
			event.setCancelled(true);
		}
	}

	private void setLapis(final EnchantingInventory inv) {
		final ItemStack itemStack = new Dye(DyeColor.BLUE).toItemStack(3);
		ItemTools.setDisplayName(itemStack, "&3Lapis");
		inv.setItem(1, itemStack);
	}

	private void removeLapis(final EnchantingInventory inv) {
		inv.setItem(1, new ItemStack(Material.AIR));
	}
}

package fr.tristiisch.emeraldmc.skyfight.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.DatabaseManager;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayer;

public class MySQLSkyFight {

	public static SkyFightPlayer getPlayer(final UUID playerUUID) {
		SkyFightPlayer skyfightPlayer = null;
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM users WHERE uuid = '" + playerUUID + "';");
			if(resultSet.next()) {
				skyfightPlayer = SkyFightPlayer.fromJson(resultSet.getString("skyfight"));
				/*				if(skyfightPlayer == null || skyfightPlayer.getPlayerInfo() == null) {
									skyfightPlayer = new SkyFightPlayer(resultSet.getInt("skyfight_kill"), resultSet.getInt("skyfight_death"), resultSet.getInt("skyfight_xp"));
								}*/

			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return skyfightPlayer;
	}

	public static Map<UUID, SkyFightPlayer> getAllPlayers() {
		final Map<UUID, SkyFightPlayer> skyfightPlayers = new HashMap<>();
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM users;");
			while(resultSet.next()) {
				final String uuidString = resultSet.getString("uuid");
				final String result = resultSet.getString("skyfight");
				if(result != null && uuidString != null) {
					skyfightPlayers.put(UUID.fromString(uuidString), SkyFightPlayer.fromJson(resultSet.getString("skyfight")));
				}
			}
		} catch(final SQLException e) {
			e.printStackTrace();
		}
		return skyfightPlayers;
	}

	public static void savePlayer(final UUID uuid, final SkyFightPlayer skyfightPlayer) {
		try {
			final PreparedStatement pstate = DatabaseManager.getConnection().prepareStatement(
					"UPDATE users SET `skyfight_kill` = ?, `skyfight_death` = ?, `skyfight_xp` = ?, `skyfight` = ? WHERE uuid = ?;");
			int i = 1;
			pstate.setInt(i++, skyfightPlayer.getKill());
			pstate.setInt(i++, skyfightPlayer.getDeath());
			pstate.setInt(i++, skyfightPlayer.getXP());
			pstate.setString(i++, skyfightPlayer.toJson());
			pstate.setString(i++, uuid.toString());
			pstate.executeUpdate();
		} catch(final SQLException e) {
			e.printStackTrace();
		}
	}
}

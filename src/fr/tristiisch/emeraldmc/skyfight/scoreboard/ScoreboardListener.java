package fr.tristiisch.emeraldmc.skyfight.scoreboard;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.customevent.AsyncEmeraldPlayerChangeEvent;
import fr.tristiisch.emeraldmc.api.spigot.customevent.PlayerScoreboardReloadEvent;

public class ScoreboardListener implements Listener {

	@EventHandler
	public void AsyncEmeraldPlayerChange(final AsyncEmeraldPlayerChangeEvent event) {
		final Player player = event.getPlayer();
		final EmeraldPlayer emeraldPlayer = event.getEmeraldPlayer();

		Scoreboards.updateScoreboards(player, emeraldPlayer);
	}

	@EventHandler
	public void AsyncPlayerScoreboardReload(final PlayerScoreboardReloadEvent event) {
		final Player player = event.getPlayer();
		Scoreboards.updateScoreboards(player);
	}

}

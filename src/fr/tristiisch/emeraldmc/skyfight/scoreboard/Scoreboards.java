package fr.tristiisch.emeraldmc.skyfight.scoreboard;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.scoreboard.EmeraldScoreboardSign;
import fr.tristiisch.emeraldmc.api.spigot.scoreboard.ScoreboardList;
import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;
import fr.tristiisch.emeraldmc.skyfight.Main;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayer;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayers;
import fr.tristiisch.emeraldmc.skyfight.guild.GuildUtils;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.Guild;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.GuildRole;

public class Scoreboards {

	static {
		EmeraldScoreboardSign.animation(Main.getInstance(), 0, "play.emeraldmc.fr", ChatColor.GOLD, ChatColor.YELLOW);
	}

	public static void setScoreboards(final Player player, final EmeraldPlayer emeraldPlayer) {
		TaskManager.runTask(() -> {
			final EmeraldScoreboardSign emeraldScoreboard = new EmeraldScoreboardSign("&2EmeraldMC", player);
			emeraldScoreboard.create();

			updateScoreboardTabList(player, emeraldPlayer);
			updateScoreboardSign(player, emeraldPlayer, emeraldScoreboard);
		});
	}

	public static void updateScoreboards(final UUID uuid) {
		updateScoreboards(Bukkit.getPlayer(uuid));
	}

	public static void updateScoreboards(final Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			updateScoreboards(player, new AccountProvider(player.getUniqueId()).getEmeraldPlayer());
		});
	}

	public static void updateScoreboards(final Player player, final EmeraldPlayer emeraldPlayer) {
		updateScoreboardTabList(player, emeraldPlayer);
		updateScoreboardSign(player, emeraldPlayer);
	}

	public static void setScoreboards(final Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			setScoreboards(player, new AccountProvider(player.getUniqueId()).getEmeraldPlayer());
		});
	}

	public static void updateScoreboardSign(final UUID uuid) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			updateScoreboardSign(Bukkit.getPlayer(uuid), new AccountProvider(uuid).getEmeraldPlayer());
		});
	}

	public static void updateScoreboardSign(final EmeraldPlayer emeraldPlayer) {
		updateScoreboardSign(Bukkit.getPlayer(emeraldPlayer.getUniqueId()), emeraldPlayer);
	}

	public static void updateScoreboardSign(final Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			updateScoreboardSign(player, new AccountProvider(player.getUniqueId()).getEmeraldPlayer());
		});
	}

	public static void removeScoreboard(final Player player) {
		EmeraldScoreboardSign.removeScoreboard(player);
	}

	public static void updateScoreboardTabList(final Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			updateScoreboardTabList(player, new AccountProvider(player.getUniqueId()).getEmeraldPlayer());
		});
	}

	public static void updateScoreboardTabList(final Player player, final EmeraldPlayer emeraldPlayer) {

		TaskManager.runTask(() -> {
			final Guild guild = GuildUtils.getGuild(player.getUniqueId());
			if(guild != null) {
				final GuildRole role = guild.getRole(player.getUniqueId());
				ScoreboardList.setNameTag(player,
						Utils.getOppositeCharForNumber(emeraldPlayer.getGroup().getID()) + guild.getID() + Utils.getOppositeCharForNumber(guild.getRole(player.getUniqueId()).getID()),
						emeraldPlayer.getGroup().getPrefix(),
						" " + ChatColor.GRAY + role.getSymbol() + "[" + guild.getGuildType().getColor() + guild.getTag() + ChatColor.GRAY + "] ");
				return;
			}
			ScoreboardList.setNameTag(player, Utils.getOppositeCharForNumber(emeraldPlayer.getGroup().getID()), emeraldPlayer.getGroup().getPrefix(), "");
		});
	}

	public static void updateScoreboardSign(final Player player, final EmeraldPlayer emeraldPlayer) {
		updateScoreboardSign(player, emeraldPlayer, EmeraldScoreboardSign.getScoreboard(player));
	}

	public static void updateScoreboardSign(final Player player, final EmeraldPlayer emeraldPlayer, final EmeraldScoreboardSign emeraldScoreboard) {
		if(emeraldScoreboard == null) {
			return;
		}

		TaskManager.runTask(() -> {
			final Guild guild = GuildUtils.getGuild(player.getUniqueId());
			final SkyFightPlayer skyfightPlayer = SkyFightPlayers.getPlayer(emeraldPlayer.getUniqueId());
			emeraldScoreboard.clearLines();
			if(guild != null) {
				emeraldScoreboard.addLine("");
				emeraldScoreboard.addLine(Utils.color("&7Guilde: &a" + guild.getGuildType().getColor() + guild.getName()));
				final String s = Utils.withOrWithoutS(guild.getMembersOnline().size());
				emeraldScoreboard.addLine(Utils.color("&7Connecté" + s + ": &a" + guild.getMembersOnline().size() + "&7/&a" + guild.getMembers().size()));
			}
			emeraldScoreboard.addLine("");
			emeraldScoreboard.addLine(Utils.color("&7Tué" + Utils.withOrWithoutS(skyfightPlayer.getKill()) + ": &a" + skyfightPlayer.getKill()));
			emeraldScoreboard.addLine(Utils.color("&7Mort" + Utils.withOrWithoutS(skyfightPlayer.getDeath()) + ": &a" + skyfightPlayer.getDeath()));
			emeraldScoreboard.addLine("");
			/*emeraldScoreboard.addLine("&7Pierre" + Utils.withOrWithoutS(emeraldPlayer.getPierres()) + ": &a" + emeraldPlayer.getPierres());*/
			emeraldScoreboard.addLine(Utils.color("&7Minerais: &a" + emeraldPlayer.getMinerais()));
			emeraldScoreboard.addLine(Utils.color("&7Niveau: &a" + skyfightPlayer.getLevel()));
			emeraldScoreboard.addLine("");
			emeraldScoreboard.addLine(Utils.color("&6play.emeraldmc.fr"));
			emeraldScoreboard.reverseLines();
			emeraldScoreboard.updateLines();
		});
	}

}

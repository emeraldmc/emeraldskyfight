package fr.tristiisch.emeraldmc.skyfight.fight;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerKickEvent;

import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayer;
import fr.tristiisch.emeraldmc.skyfight.data.customevent.SkyFightPlayerSaveEvent;
import fr.tristiisch.emeraldmc.skyfight.listeners.AreaListeners;

public class PreventDisconnectFightListener implements Listener {

	@EventHandler
	public void EntityDamageEvent(final EntityDamageByEntityEvent event) {
		if(event.isCancelled()) {
			return;
		}
		final Entity damager = event.getDamager();
		if(!(event.getEntity() instanceof Player)) {
			return;
		}
		final Player victim = (Player) event.getEntity();
		if(damager instanceof Player) {
			final Player attacker = (Player) event.getDamager();
			PreventDisconnectFight.add(attacker);
			PreventDisconnectFight.add(victim);
		} else if(damager instanceof Projectile) {
			final Projectile proj = (Projectile) damager;
			if(proj.getShooter() instanceof Player) {
				final Player attacker2 = (Player) proj.getShooter();
				PreventDisconnectFight.add(attacker2);
			}
			PreventDisconnectFight.add(victim);
		} else if(damager instanceof Arrow) {
			final Arrow proj2 = (Arrow) damager;
			if(proj2.getShooter() instanceof Player) {
				final Player attacker2 = (Player) proj2.getShooter();
				PreventDisconnectFight.add(attacker2);
			}
			PreventDisconnectFight.add(victim);
		} else if(damager instanceof Snowball) {
			final Snowball proj3 = (Snowball) damager;
			if(proj3.getShooter() instanceof Player) {
				final Player attacker2 = (Player) proj3.getShooter();
				PreventDisconnectFight.add(attacker2);
			}
			PreventDisconnectFight.add(victim);
		} else if(damager instanceof Egg) {
			final Egg proj4 = (Egg) damager;
			if(proj4.getShooter() instanceof Player) {
				final Player attacker2 = (Player) proj4.getShooter();
				PreventDisconnectFight.add(attacker2);
			}
			PreventDisconnectFight.add(victim);
		} else if(damager instanceof ThrownPotion) {
			final ThrownPotion proj5 = (ThrownPotion) damager;
			if(proj5.getShooter() instanceof Player) {
				final Player attacker2 = (Player) proj5.getShooter();
				PreventDisconnectFight.add(attacker2);
			}
			PreventDisconnectFight.add(victim);
		}
	}

	@EventHandler
	public void PlayerKickEvent(final PlayerKickEvent event) {
		final Player player = event.getPlayer();
		if(PreventDisconnectFight.isIn(player)) {
			PreventDisconnectFight.remove(player);
		}
	}

	@EventHandler
	public void SkyFightPlayerSaveEvent(final SkyFightPlayerSaveEvent event) {
		final Player player = event.getPlayer();
		if(PreventDisconnectFight.isIn(player) && AreaListeners.isLocationSafe(player.getLocation())) {
			final SkyFightPlayer skyfightPlayer = event.getSkyFightPlayer();
			skyfightPlayer.addDeath();
			skyfightPlayer.getPlayerInfo().clearInventory();
			player.setHealth(0.0);
			System.out.println(player.getName() + " a décoCombat.");
		}
	}

	/*	@EventHandler
		public void PlayerQuitEvent(final PlayerQuitEvent event) {
			final Player player = event.getPlayer();
			if(PreventDisconnectFight.isIn(player) && AreaListeners.isLocationSafe(player.getLocation())) {
				final SkyFightPlayer skyfightPlayer = SkyFightPlayers.getPlayer(player);
				skyfightPlayer.addDeath();
				player.setHealth(0.0);
				System.out.println(player.getName() + " a décoCombat.");
			}
		}*/

	@EventHandler
	public void PlayerDeathEvent(final PlayerDeathEvent event) {
		final Player player = event.getEntity().getPlayer();
		if(PreventDisconnectFight.isIn(player)) {
			PreventDisconnectFight.remove(player);
		}
	}
}
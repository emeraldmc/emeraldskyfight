package fr.tristiisch.emeraldmc.skyfight.fight;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.customevent.ArrowHitBlockEvent;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;
import fr.tristiisch.emeraldmc.api.spigot.title.Title;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;
import fr.tristiisch.emeraldmc.skyfight.Main;
import fr.tristiisch.emeraldmc.skyfight.customcraft.CustomCraft;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayer;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayers;
import fr.tristiisch.emeraldmc.skyfight.data.enderchest.EnderChest;
import fr.tristiisch.emeraldmc.skyfight.guild.GuildUtils;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.Guild;
import fr.tristiisch.emeraldmc.skyfight.scoreboard.Scoreboards;
import fr.tristiisch.emeraldmc.skyfight.stockender.StockEnder;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand.EnumClientCommand;

public class DeadListener implements Listener {

	@EventHandler
	public void PlayerDeathEvent(final PlayerDeathEvent event) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			final List<Material> allowInv = Arrays.asList(Material.WOOD_SWORD, Material.EMERALD, Material.ARROW, Material.POTION);
			final List<Material> allowArmor = Arrays.asList(Material.AIR, Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.LEATHER_BOOTS);

			final Player victim = event.getEntity();
			Player attacker = victim.getKiller();
			final EntityDamageEvent lastDamageCause = victim.getLastDamageCause();
			if(lastDamageCause != null && attacker == null) {
				final Entity lastDamageCauseEntity = lastDamageCause.getEntity();

				if(lastDamageCauseEntity instanceof Player) {
					attacker = (Player) lastDamageCauseEntity;

				} else if(lastDamageCauseEntity instanceof Projectile) {
					final Projectile projectile = (Projectile) lastDamageCauseEntity;

					if(projectile.getShooter() instanceof Player) {
						attacker = (Player) projectile.getShooter();
					}
				}

			}

			final Player attacker2 = attacker;

			if(attacker2 != null) {

				final AccountProvider accountProviderAttacker = new AccountProvider(attacker2.getUniqueId());
				final EmeraldPlayer emeraldAttacker = accountProviderAttacker.getEmeraldPlayer();
				if(!SpigotUtils.isSamePlayer(victim, attacker2)) {
					/*
					 * int amountAttacker = 0; for(final ItemStack item :
					 * attacker.getInventory().getContents()) { if(item != null) { amountAttacker +=
					 * item.getAmount(); } }
					 */

					final boolean noInvPlayer = Arrays.stream(victim.getInventory().getContents()).filter(item -> item != null && !allowInv.contains(item.getType())).count() == 0;
					final boolean noInvAttacker = Arrays.stream(attacker2.getInventory().getContents()).filter(item -> item != null && !allowInv.contains(item.getType())).count() == 0;

					final boolean noArmorPlayer = Arrays.stream(victim.getInventory().getArmorContents()).filter(item -> item != null && !allowArmor.contains(item.getType())).count() == 0;
					final boolean noArmorAttacker = Arrays.stream(attacker2.getInventory().getArmorContents()).filter(item -> item != null && !allowArmor.contains(item.getType())).count() == 0;

					final boolean noStuffPlayer = noInvPlayer || noArmorPlayer;
					final boolean noStuffAttacker = noInvAttacker || noArmorAttacker;

					if(!noStuffPlayer && !noStuffAttacker || noStuffAttacker /*|| noStuffPlayer && noStuffAttacker*/) {
						TextComponent msgToVictim;
						if(victim.getLastDamageCause().getCause() == DamageCause.ENTITY_ATTACK) {

							attacker2.sendMessage(Utils.color("&aVous avez tué &2" + victim.getName() + "&a."));
							msgToVictim = new TextComponent(Utils.colorFix("&cVous avez été tué par &4" + attacker2.getName() + "&c."));

						} else if(victim.getLastDamageCause().getCause() == DamageCause.PROJECTILE) {

							attacker2.sendMessage(Utils.color("&2" + victim.getName() + " &aest mort de votre flêche."));
							msgToVictim = new TextComponent(Utils.colorFix("&cVous êtes mort d'une flêche de &4" + attacker2.getName() + "&c."));

						} else if(victim.getLastDamageCause().getCause() == DamageCause.VOID) {

							attacker2.sendMessage(Utils.color("&2" + victim.getName() + " &aest mort dans le vide en vous échappant."));
							msgToVictim = new TextComponent(Utils.colorFix("&cVous êtes mort dans le vide en échappant à &4" + attacker2.getName() + "&c."));

						} else if(victim.getLastDamageCause().getCause() == DamageCause.FALL) {

							attacker2.sendMessage(Utils.color("&2" + victim.getName() + " &aest mort de chute en vous échappant."));
							msgToVictim = new TextComponent(Utils.colorFix("&cVous êtes mort de chute en échappant à &4" + attacker2.getName() + "&c."));

						} else {
							attacker2.sendMessage(Utils.color("&2" + victim.getName() + " &aest mort en vous échappant."));
							msgToVictim = new TextComponent(Utils.colorFix("&cVous êtes mort en échappant à &4" + attacker2.getName() + "&c."));
						}

						if(victim.isOnline()) {
							final GuiTools gui = Dead.getDeadGui(victim, attacker2, emeraldAttacker);
							Dead.addGuiToMessage(msgToVictim, "&7Cliquez pour afficher l'inventaire de " + attacker2.getName(), gui);
							victim.spigot().sendMessage(msgToVictim);
						}
						/* DeadBodies.playFakeBed(player); */

						final SkyFightPlayer skyfightAttacker = SkyFightPlayers.getPlayer(attacker2);
						skyfightAttacker.addKill();
						final Guild guild = GuildUtils.getGuild(attacker2.getUniqueId());
						if(guild != null) {
							guild.addXP(1);
							skyfightAttacker.addXP(2);
						} else {
							skyfightAttacker.addXP(3);
						}
						accountProviderAttacker.sendAccountToRedis(emeraldAttacker);

						final ItemStack emeraldcoin = CustomCraft.getEmerald();
						if(victim.getItemInHand().containsEnchantment(Enchantment.LOOT_BONUS_MOBS)) {
							final int enchantLevel = victim.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_MOBS);
							emeraldcoin.setAmount(emeraldcoin.getAmount() + enchantLevel);
						}

						final ItemStack portalItem = StockEnder.getItem();
						if(StockEnder.hasEnchant(attacker2)) {
							final Inventory enderChestAttacker = EnderChest.getEnderChest(attacker2);
							if(SpigotUtils.hasEnoughPlace(enderChestAttacker, emeraldcoin)) {
								String name;
								if(emeraldcoin.getAmount() == 1) {
									name = "Un &2" + emeraldcoin.getItemMeta().getDisplayName() + " &aa";
								} else {

									name = "Des &2" + emeraldcoin.getItemMeta().getDisplayName() + "s &aont";
								}

								final TextComponent msg = new TextComponent(Utils.colorFix("&2EmeraldMC &7» &a" + ChatColor.stripColor(name) + " &aété mis dans l'enderchest grâce au "));

								final BaseComponent[] components = new ComponentBuilder(portalItem.getItemMeta().getDisplayName()).color(ChatColor.DARK_GREEN).bold(true).create();
								for(final BaseComponent component : components) {
									msg.addExtra(component);
								}
								msg.addExtra(Utils.colorFix("&a."));

								enderChestAttacker.addItem(emeraldcoin);

							} else {
								Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
									victim.getWorld().dropItemNaturally(victim.getLocation(), emeraldcoin);
								});
							}
						} else {
							Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
								victim.getWorld().dropItemNaturally(victim.getLocation(), emeraldcoin);
							});
						}

						Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
							Scoreboards.updateScoreboardSign(attacker2, emeraldAttacker);
						});

						attacker2.playSound(attacker2.getLocation(), Sound.BURP, 1, 1);
					} else {
						TextComponent msgToVictim;
						if(victim.getLastDamageCause().getCause() == DamageCause.ENTITY_ATTACK) {
							msgToVictim = new TextComponent(Utils.colorFix("&cVous avez été tué par &4" + attacker2.getName() + "&c."));
						} else if(victim.getLastDamageCause().getCause() == DamageCause.PROJECTILE) {

							msgToVictim = new TextComponent(Utils.colorFix("&cVous êtes mort d'une flêche de &4" + attacker2.getName() + "&c."));

						} else if(victim.getLastDamageCause().getCause() == DamageCause.VOID) {

							msgToVictim = new TextComponent(Utils.colorFix("&cVous êtes mort dans le vide en échappant à &4" + attacker2.getName() + "&c."));

						} else if(victim.getLastDamageCause().getCause() == DamageCause.FALL) {
							msgToVictim = new TextComponent(Utils.colorFix("&cVous êtes mort de chute en échappant à &4" + attacker2.getName() + "&c."));

						} else {
							msgToVictim = new TextComponent(Utils.colorFix("&cVous êtes mort en échappant à &4" + attacker2.getName() + "&c."));
						}
						if(victim.isOnline()) {
							final GuiTools gui = Dead.getDeadGui(victim, attacker2, emeraldAttacker);
							Dead.addGuiToMessage(msgToVictim, "&7Cliquez pour afficher l'inventaire de " + attacker2.getName(), gui);
							victim.spigot().sendMessage(msgToVictim);
						}

						attacker2.sendMessage(Utils.color("&cVous avez tué  " + victim.getName() + ". Tuer des nostuff ne rapporte pas de loots"));
						attacker2.playSound(attacker2.getLocation(), Sound.BURP, 1, 0);
					}

				} else {
					victim.sendMessage(Utils.color("&cVous vous êtes suicider."));
				}

			} else if(victim.getLastDamageCause() == null) {
				victim.sendMessage(Utils.color("&cVous vous êtes suicider."));

			} else if(victim.getLastDamageCause().getCause() == DamageCause.VOID) {
				victim.sendMessage(Utils.color("&cVous êtes mort dans un trou noir, c'est troublant."));
			} else if(victim.getLastDamageCause().getCause() == DamageCause.FALL) {
				victim.sendMessage(Utils.color("&cVous êtes mort de chute."));
			}
			/*
			 * if(attacker != null) { event.setKeepInventory(true); } else {
			 * event.setKeepInventory(false); }
			 */

			event.setKeepLevel(false);
			event.setDroppedExp(0);
			event.getDrops().removeIf(item -> (allowArmor.contains(item.getType()) || item.getType() == Material.WOOD_SWORD) && item.getEnchantments().size() == 0);

			event.setDeathMessage(null);

			/*
			 * final PacketPlayInClientCommand packet = new
			 * PacketPlayInClientCommand(EnumClientCommand.PERFORM_RESPAWN);
			 */
			/* Reflection.sendPacket(player, packet); */
			/* ((CraftPlayer) player).getHandle().playerConnection.a(packet); */
			/* Reflection.sendPacket(player, packet); */
			/*
			 * // TODO in reflection try { Object nmsPlayer =
			 * Reflection.getNmsPlayer(player); System.out.println("Name: " +
			 * nmsPlayer.getClass().getPackage().getName()); Object packet =
			 * Class.forName(nmsPlayer.getClass().getPackage().getName() +
			 * ".Packet205ClientCommand").newInstance();
			 * packet.getClass().getField("a").set(packet, 1); Object con =
			 * Reflection.getPlayerConnection(player); con.getClass().getMethod("a",
			 * packet.getClass()).invoke(con, packet); } catch (Throwable e) {
			 * e.printStackTrace(); }
			 */

			if(victim.isOnline()) {

				final SkyFightPlayer skyfightPlayer = SkyFightPlayers.getPlayer(victim);
				skyfightPlayer.addDeath();

				Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
					final PacketPlayInClientCommand packet = new PacketPlayInClientCommand(EnumClientCommand.PERFORM_RESPAWN);
					((CraftPlayer) victim).getHandle().playerConnection.a(packet);
					Title.sendTitle(victim, "&cVous êtes mort", null, 0, 30, 5);
					Scoreboards.updateScoreboardSign(victim);
				}, 5);
			}

		});
		event.setKeepLevel(false);
		event.setDroppedExp(0);

		final List<Material> material = Arrays.asList(Material.WOOD_SWORD, Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.LEATHER_BOOTS);
		event.getDrops().removeIf(item -> material.contains(item.getType()) && item.getEnchantments().size() == 0);

		event.setDeathMessage(null);
	}

	@EventHandler
	public void PlayerRespawnEvent(final PlayerRespawnEvent event) {
		final Player player = event.getPlayer();
		event.setRespawnLocation(EmeraldSpigot.getSpawn());
		player.setVelocity(new Vector(0, 0, 0));
		Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
			Stuff.give(player);
			SpigotUtils.addNightVision(player);
		}, 5);
	}

	@EventHandler
	public void ProjectileHitEvent(final ArrowHitBlockEvent event) {
		TaskManager.runTask(() -> event.getArrow().remove());
	}
	/*
	 * @EventHandler public void PlayerMoveBEvent(PlayerMoveBlockEvent event) {
	 * Player player = event.getPlayer(); if(player.getLocation().getY() < 0) {
	 * player.damage(1000, DamageCause.VOID); player.setLastDamageCause(new
	 * EntityDamageEvent(player, DamageCause.VOID, 1000)); } }
	 */

}

package fr.tristiisch.emeraldmc.skyfight.fight;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.anticheat.PlayerCheat;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;
import fr.tristiisch.emeraldmc.skyfight.fakeinventory.FakeInventorys;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class Dead {

	public static void addGuiToMessage(final TextComponent msg, final String showText, final GuiTools gui) {

		final UUID fakeInventoryUuid = FakeInventorys.addInventory(gui);
		msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color(showText)).create()));
		msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/fakeinventory " + fakeInventoryUuid.toString()));
	}

	public static GuiTools getDeadGui(final Player victim, final Player attacker, final EmeraldPlayer emeraldAttacker) {
		final PlayerInventory attackerInventory = attacker.getInventory();
		final GuiTools gui = new GuiTools("&c" + attacker.getName() + " ⚔ " + victim.getName(), "fakeInventory", 5 * 9, victim.getUniqueId().toString());

		int slot = 0;
		gui.setItem(slot++, attackerInventory.getHelmet());
		gui.setItem(slot++, attackerInventory.getChestplate());
		gui.setItem(slot++, attackerInventory.getLeggings());
		gui.setItem(slot++, attackerInventory.getBoots());
		slot = 9;
		int slot2 = 4 * 9;
		for(final ItemStack itemStack : attackerInventory.getContents()) {
			if(slot2 < PlayerCheat.slotHotBar + 9) {
				gui.setItem(slot2, itemStack);
				slot2++;
			} else {
				gui.setItem(slot, itemStack);
				slot++;
			}

		}

		ItemStack itemStack;

		itemStack = ItemTools.createPlayerSkull(attacker.getName(),
				1,
				emeraldAttacker.getGroup().getPrefix() + attacker.getName(),
				Arrays.asList("", Utils.color("&7Vie: &e" + Math.round(attacker.getHealth()) + " &c❤")));
		gui.setItem(7, itemStack);

		int effect = 0;
		final List<String> lore = new ArrayList<>();
		lore.add("");
		for(final PotionEffect effects : attacker.getActivePotionEffects()) {
			if(effects.hasParticles()) {
				lore.add(Utils.color("&7" + SpigotUtils.translateEffects(effects.getType().getName()) + " " + (effects.getAmplifier() + 1)));
				effect++;
			}
		}

		String name;
		Material material = Material.GLASS_BOTTLE;
		switch(effect) {
		case 0:
			lore.add(Utils.color("&7Aucun effet."));
			lore.add("");
			material = Material.GLASS_BOTTLE;
		case 1:
			name = Utils.color("&6Effet");
			break;
		default:
			name = Utils.color("&6Effets");
		}
		itemStack = ItemTools.create(material, effect, (byte) effect, name, lore);
		gui.setItem(8, itemStack);
		return gui;
	}
}

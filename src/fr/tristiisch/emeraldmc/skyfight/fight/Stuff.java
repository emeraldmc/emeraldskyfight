package fr.tristiisch.emeraldmc.skyfight.fight;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.ColorUtil;
import fr.tristiisch.emeraldmc.api.spigot.ColorUtil.ColorSet;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayer;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayers;
import fr.tristiisch.emeraldmc.skyfight.sign.SkyFightSignItem;
import net.md_5.bungee.api.ChatColor;

public class Stuff {

	public static void give(final Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			give(player, new AccountProvider(player.getUniqueId()).getEmeraldPlayer());
		});
	}

	public static void give(final Player player, final EmeraldPlayer emeraldPlayer) {
		final PlayerInventory playerInventory = player.getInventory();

		final ItemStack wood_sword = SkyFightSignItem.WOOD_SWORD.getItemStack();
		if(playerInventory.getItem(0) == null && !playerInventory.contains(wood_sword)) {
			playerInventory.setItem(0, wood_sword);
		}

		final ItemStack enderpearl = SkyFightSignItem.ENDER_PEARL.getItemStack();
		enderpearl.setAmount(1);
		if(!playerInventory.contains(enderpearl)) {
			playerInventory.addItem(enderpearl);
		}

		if(Arrays.stream(playerInventory.getArmorContents()).filter(item -> item != null && item.getType() != Material.AIR).count() == 0) {

			setArmor(player);
		}
	}

	public static void setArmor(final Player player) {
		final PlayerInventory playerInventory = player.getInventory();
		final SkyFightPlayer skyFightPlayer = SkyFightPlayers.getPlayer(player);

		Color color;
		if(skyFightPlayer.getArmorColor() != null) {
			color = skyFightPlayer.getArmorColor();
		} else {
			final ChatColor groupColor = EmeraldGroup.JOUEUR.getColor();
			final ColorSet<Integer, Integer, Integer> co = ColorUtil.toRGB(groupColor);
			color = Color.fromBGR(co.getBlue(), co.getGreen(), co.getRed());
		}

		Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
			final List<ItemStack> armors = Arrays.asList(SkyFightSignItem.LEATHER_HELMET.getItemStack(),
					SkyFightSignItem.LEATHER_CHESTPLATE.getItemStack(),
					SkyFightSignItem.LEATHER_LEGGINGS.getItemStack(),
					SkyFightSignItem.LEATHER_BOOTS.getItemStack());

			Collections.reverse(armors);

			playerInventory.setArmorContents(armors.stream().map(itemStack -> ItemTools.setColoredArmor(itemStack, color)).toArray(size -> new ItemStack[size]));
		});
	}
}

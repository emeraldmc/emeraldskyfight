package fr.tristiisch.emeraldmc.skyfight.fight;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;

public class CombatCommand implements CommandExecutor {

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		if(!(sender instanceof Player)) {
			return true;
		}
		final Player player = (Player) sender;
		if(PreventDisconnectFight.isIn(player)) {
			player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous êtes toujours en combat."));
		} else {
			player.sendMessage(Utils.color("&2EmeraldMC &7» &aVous n'êtes pas en combat."));
		}

		return true;
	}

}

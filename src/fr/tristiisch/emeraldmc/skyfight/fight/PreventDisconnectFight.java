package fr.tristiisch.emeraldmc.skyfight.fight;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;

public class PreventDisconnectFight {

	private static Map<UUID, Long> players = new HashMap<>();

	public static boolean isIn(final Player player) {
		if(players.containsKey(player.getUniqueId()) && players.get(player.getUniqueId()) > Utils.getCurrentTimeinSeconds()) {
			return true;
		} else {
			remove(player);
			return false;
		}
	}

	public static void add(final Player player) {
		players.put(player.getUniqueId(), Utils.getCurrentTimeinSeconds() + 10);
	}

	public static Long remove(final Player player) {
		return players.remove(player.getUniqueId());
	}
}

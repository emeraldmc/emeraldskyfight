package fr.tristiisch.emeraldmc.skyfight.fight;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class DamageEvent  implements Listener {
	/*
	 * @EventHandler public void EntityShootBowEvent(final EntityShootBowEvent
	 * event) { if(!(event.getEntity() instanceof Player)) { return; } final Player
	 * player = (Player) event.getEntity(); }
	 */

	@EventHandler
	public void EntityDamageEvent(final EntityDamageEvent event) {
		if(!(event.getEntity() instanceof Player)){
			return;
		}
		if(event.getCause() == DamageCause.FALL) {
			event.setDamage(event.getFinalDamage() / 2.5);
		}
	}
}

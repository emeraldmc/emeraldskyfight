package fr.tristiisch.emeraldmc.skyfight.stockender;

import fr.tristiisch.emeraldmc.api.spigot.items.ItemEnchant;
import fr.tristiisch.emeraldmc.api.spigot.items.enchant.CustomEnchantment;

public enum CustomEnchants {

	PICK_EMERALD(new CustomEnchantment("Récupérateur d'émeraude", 254));

	static {
		for(final CustomEnchants enchant : CustomEnchants.values()) {
			enchant.getCustomEnchantment().registerEnchant();
			System.out.println("CustomEnchants register: " + enchant.getCustomEnchantment().getName());
		}
	}

	final CustomEnchantment customEnchantment;

	private CustomEnchants(final CustomEnchantment customEnchantment) {
		this.customEnchantment = customEnchantment;
	}

	public CustomEnchantment getCustomEnchantment() {
		return this.customEnchantment;
	}

	public ItemEnchant getItemEnchant() {
		return new ItemEnchant(this.customEnchantment);
	}
}

package fr.tristiisch.emeraldmc.skyfight.stockender;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;
import fr.tristiisch.emeraldmc.api.spigot.utils.CustomConfigUtils;
import fr.tristiisch.emeraldmc.skyfight.Main;

public class StockEnderRunnable extends BukkitRunnable {

	public static void runTask() {
		TaskManager.runTaskLater("stockender", new StockEnderRunnable(), (new Random().nextInt(30) + 60) * 60 * 20);
	}

	@Override
	public void run() {
		final ItemStack stockEnder = StockEnder.getItem();

		final List<Location> locations = CustomConfigUtils.getLocationList(Main.getInstance().getConfig(), "stockender");
		final Collection<Entity> entitys = new ArrayList<>();
		locations.stream().forEach(location -> entitys.addAll(location.getWorld().getNearbyEntities(location, 1, 1, 1)));

		final List<Item> items = entitys.stream().filter(entity -> entity.getType() == EntityType.DROPPED_ITEM).map(entity -> (Item) entity).collect(Collectors.toList());

		final Item itemStockEnder = items.stream().filter(item -> item.getItemStack().isSimilar(stockEnder)).findFirst().orElse(null);
		if(itemStockEnder == null) {
			final int index = StockEnder.getRandom().nextInt(locations.size());
			final Location location = locations.get(index);
			final Item newitem = location.getWorld().dropItemNaturally(location, StockEnder.getItem());
			StockEnder.currentItem = newitem;
			/*			Bukkit.broadcastMessage(Utils.color("&6[&2StockEnder&6] &aUn StockEnder est apparut dans l'une des salles secretes ! "));*/
		} else {
			StockEnder.currentItem = itemStockEnder;
			/*			final Location location = itemStockEnder.getLocation();
						Bukkit.broadcastMessage(Utils.color("&6[&2StockEnder&6] &aUn StockEnder est déjà apparut dans l'une des salles secretes ! " + location.getBlockX() + " " + location.getBlockZ()));*/
		}
		Bukkit.broadcastMessage(Utils.color("&6[&2StockEnder&6] &aUn StockEnder est apparut dans l'une des salles secretes ! "));
		runTask();
	}
}

package fr.tristiisch.emeraldmc.skyfight.stockender;

import java.util.Arrays;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;

public class StockEnder {

	public static Item currentItem;
	public static ItemStack stockEnder = ItemTools
			.create(Material.ENDER_PORTAL_FRAME, 1, "&dStockEnder", Arrays.asList("", " &5➤ &7Il met les emeraudes", "&7    dans l'enderchest."), CustomEnchants.PICK_EMERALD.getItemEnchant());
	public static Random random = new Random();

	public static Random getRandom() {
		return random;
	}

	public static ItemStack getItem() {
		return stockEnder;
	}

	public static boolean hasEnchant(final Player player) {
		return Arrays.stream(player.getInventory().getContents())
				.filter(itemStack -> itemStack != null && itemStack.getEnchantments().containsKey(CustomEnchants.PICK_EMERALD.getCustomEnchantment().getEnchantment()))
				.findFirst()
				.isPresent();
	}
}

package fr.tristiisch.emeraldmc.skyfight.stockender;

import org.bukkit.Bukkit;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;
import fr.tristiisch.emeraldmc.skyfight.customcraft.CustomCraft;
import fr.tristiisch.emeraldmc.skyfight.data.enderchest.EnderChest;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;

public class StockEnderListener implements Listener {

	public StockEnderListener() {
		// Register custom enchants
		CustomEnchants.values();
		StockEnderRunnable.runTask();
	}

	@EventHandler
	public void PlayerPickupItemEvent(final PlayerPickupItemEvent event) {
		if(event.isCancelled()) {
			return;
		}

		final Player player = event.getPlayer();
		final Item itemPickup = event.getItem();
		final ItemStack itemStackPickup = itemPickup.getItemStack();

		if(StockEnder.currentItem != null && itemPickup.getUniqueId().equals(StockEnder.currentItem.getUniqueId())) {

			Bukkit.broadcastMessage(Utils.color("&6[&2StockEnder&6] &2" + player.getName() + " &avient de récupérer le StockEnder."));

		} else if(itemStackPickup.isSimilar(CustomCraft.getEmerald()) && StockEnder.hasEnchant(player)) {

			final Inventory enderChest = EnderChest.getEnderChest(player);
			if(SpigotUtils.hasEnoughPlace(enderChest, itemStackPickup)) {
				event.setCancelled(true);
				itemPickup.remove();
				enderChest.addItem(itemStackPickup);

				String name;
				if(itemStackPickup.getAmount() == 1) {
					name = "Un &2" + itemStackPickup.getItemMeta().getDisplayName() + " &aa";
				} else {

					name = "Des &2" + itemStackPickup.getItemMeta().getDisplayName() + "s &aont";
				}

				final TextComponent msg = new TextComponent(Utils.colorFix("&2EmeraldMC &7» &a" + ChatColor.stripColor(name) + " &aété mis dans l'enderchest grâce au "));

				final ItemStack portalItem = StockEnder.getItem();
				/*final HoverEvent hoverEvent = ItemTools.getItemHoverEvent(portalItem);*/

				final BaseComponent[] components = new ComponentBuilder(portalItem.getItemMeta().getDisplayName()).color(ChatColor.DARK_GREEN).bold(true)/*.event(hoverEvent)*/.create();
				for(final BaseComponent component : components) {
					msg.addExtra(component);
				}
				msg.addExtra(Utils.colorFix("&a."));

				player.spigot().sendMessage(msg);
			} else {
				player.sendMessage(Utils.color(
						"&2EmeraldMC &7» &cL'enderchest n'a pas la place pour " + ChatColor.stripColor(itemStackPickup.getAmount() + " " + itemStackPickup.getItemMeta().getDisplayName()) + " &c."));
			}

		}

	}
}

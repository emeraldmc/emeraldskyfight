package fr.tristiisch.emeraldmc.skyfight.sign;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;

public enum SkyFightSignItem {

	DIAMOND_SWORD(1, "Epée en diamant", ChatColor.AQUA, Material.DIAMOND_SWORD, (byte) 0, null),
	DIAMOND_HELMET(2, "Casque en diamant", ChatColor.AQUA, Material.DIAMOND_HELMET, (byte) 0, null),
	DIAMOND_CHESTPLATE(3, "Plastron en diamant", ChatColor.AQUA, Material.DIAMOND_CHESTPLATE, (byte) 0, null),
	DIAMOND_BOOTS(4, "Bottes en diamant", ChatColor.AQUA, Material.DIAMOND_BOOTS, (byte) 0, null),

	IRON_SWORD(5, "Epée en fer", ChatColor.GRAY, Material.IRON_SWORD, (byte) 0, null),
	IRON_HELMET(6, "Casque en fer", ChatColor.GRAY, Material.IRON_HELMET, (byte) 0, null),
	IRON_CHESTPLATE(7, "Plastron en fer", ChatColor.GRAY, Material.IRON_CHESTPLATE, (byte) 0, null),
	IRON_LEGGINGS(8, "Pantalon en fer", ChatColor.GRAY, Material.IRON_LEGGINGS, (byte) 0, null),
	IRON_BOOTS(9, "Bottes en fer", ChatColor.GRAY, Material.IRON_BOOTS, (byte) 0, null),

	CHAINMAIL_HELMET(10, "Casque en maille", ChatColor.GRAY, Material.CHAINMAIL_HELMET, (byte) 0, null),
	CHAINMAIL_CHESTPLATE(11, "Plastron en maille", ChatColor.GRAY, Material.CHAINMAIL_CHESTPLATE, (byte) 0, null),
	CHAINMAIL_LEGGINGS(12, "Pantalon en maille", ChatColor.GRAY, Material.CHAINMAIL_LEGGINGS, (byte) 0, null),
	CHAINMAIL_BOOTS(13, "Bottes en maille", ChatColor.GRAY, Material.CHAINMAIL_BOOTS, (byte) 0, null),
	STONE_SWORD(14, "Epée en pierre", ChatColor.DARK_GRAY, Material.STONE_SWORD, (byte) 0, null),

	BOW(15, "Arc", ChatColor.GOLD, Material.BOW, (byte) 0, null),
	ARROW(16, "Flèches", ChatColor.GOLD, Material.ARROW, (byte) 0, null),

	FISHING_ROD(17, "Canne à pêche", ChatColor.GOLD, Material.FISHING_ROD, (byte) 0, null),

	POTION_FIRE_RESISTANCE(18, "Potion de résistance au feu", ChatColor.YELLOW, Material.POTION, (byte) 0, new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 8 * 60 * 20, 0, true, true)),
	POTION_HEAL(19, "Potion de soin", ChatColor.YELLOW, Material.POTION, (byte) 0, new PotionEffect(PotionEffectType.HEAL, 0 * 20, 0, true, true)),
	POTION_SPEED(20, "Potion de vitesse", ChatColor.YELLOW, Material.POTION, (byte) 0, new PotionEffect(PotionEffectType.SPEED, 3 * 60 * 20, 0, true, true)),
	POTION_JUMP(21, "Potion de saut III", ChatColor.YELLOW, Material.POTION, (byte) 0, new PotionEffect(PotionEffectType.JUMP, 3 * 60 * 20, 2, true, true)),
	POTION_INVISIBILITY(22, "Potion d'invisibilité", ChatColor.YELLOW, Material.POTION, (byte) 0, new PotionEffect(PotionEffectType.INVISIBILITY, 30 * 20, 0, true, true)),
	POTION_SLOW(23, "Potion de lenteur", ChatColor.YELLOW, Material.POTION, (byte) 0, new PotionEffect(PotionEffectType.SLOW, 30 * 20, 0, true, true)),

	GOLDEN_APPLE(24, "Pomme en or", ChatColor.GOLD, Material.GOLDEN_APPLE, (byte) 0, null),

	ENDER_PEARL(25, "Ender Pearl", ChatColor.DARK_PURPLE, Material.ENDER_PEARL, (byte) 0, null),

	EXP_BOTTLE(26, "Fiole d'XP", ChatColor.GOLD, Material.EXP_BOTTLE, (byte) 0, null),

	SNOW_BALL(27, "Boule de neige", ChatColor.WHITE, Material.SNOW_BALL, (byte) 0, null),

	WOOD_SWORD(28, "Epée en bois", ChatColor.GOLD, Material.WOOD_SWORD, (byte) 0, null),
	LEATHER_HELMET(29, "Casque en cuir", ChatColor.GOLD, Material.LEATHER_HELMET, (byte) 0, null),
	LEATHER_CHESTPLATE(30, "Plastron en cuir", ChatColor.GOLD, Material.LEATHER_CHESTPLATE, (byte) 0, null),
	LEATHER_LEGGINGS(31, "Pantalon en cuir", ChatColor.GOLD, Material.LEATHER_LEGGINGS, (byte) 0, null),
	LEATHER_BOOTS(32, "Bottes en cuir", ChatColor.GOLD, Material.LEATHER_BOOTS, (byte) 0, null),
	;

	final private int id;
	final private String name;
	final private ChatColor color;
	final private Material material;
	final private Byte data;
	final private PotionEffect potionEffect;

	private SkyFightSignItem(final int id, final String name, final ChatColor color, final Material material, final Byte data, final PotionEffect potionEffect) {
		this.id = id;
		this.name = name;
		this.color = color;
		this.material = material;
		this.data = data;
		this.potionEffect = potionEffect;
	}

	public int getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public ChatColor getColor() {
		return this.color;
	}

	public Material getMaterial() {
		return this.material;
	}

	public ItemStack getItemStack() {
		if(this.getPotionEffect() != null) {
			return ItemTools.customPotionEffect(this.potionEffect, true, false, this.getMaterial().getMaxStackSize(), this.getColor() + this.getName(), null);
		} else {
			return ItemTools.create(this.getMaterial(), this.getMaterial().getMaxStackSize(), this.getData(), this.getColor() + this.getName());
		}
	}

	public Byte getData() {
		return this.data;
	}

	public PotionEffect getPotionEffect() {
		return this.potionEffect;
	}

	public static SkyFightSignItem getByID(final int id) {
		return Arrays.stream(SkyFightSignItem.values()).filter(skyFightSignItem -> skyFightSignItem.getId() == id).findFirst().orElse(null);
	}
}

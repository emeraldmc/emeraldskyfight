package fr.tristiisch.emeraldmc.skyfight.sign;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools.GuiData;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.api.spigot.sneak.Sneak;
import fr.tristiisch.emeraldmc.api.spigot.utils.LocationUtils;
import fr.tristiisch.emeraldmc.skyfight.sign.Signs.SkyFightSign;

public class SignListener implements Listener {

	public SignListener(){
		Signs.getSignsFromConfig();
	}

	@EventHandler
	private void BlockBreakEvent(final BlockBreakEvent event) {
		final Player player = event.getPlayer();
		if(event.isCancelled()) {
			return;
		}

		final Block block = event.getBlock();
		if(SignUtils.isSign(block.getType())) {
			final Sign sign = (Sign) block.getState();

			final SkyFightSign skyFightSign = Signs.getSignByLocation(sign.getLocation());
			if(skyFightSign != null) {
				event.setCancelled(true);
				player.sendMessage(Utils.color("&2EmeraldMC &7» &cIl faut enlever l'item du panneau avant de supprimer le panneau."));
			}
		}

	}

	@EventHandler
	public void PlayerInteractEvent(final PlayerInteractEvent event) {
		final Player player = event.getPlayer();
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			final Block block = event.getClickedBlock();
			if(SignUtils.isSign(block.getType())) {
				final Sign sign = (Sign) block.getState();

				if(Sneak.isSneak(player) && player.getGameMode().equals(GameMode.CREATIVE)) {
					final List<ItemStack> items = Signs.getSignsItemsAmount();
					final GuiTools guiTools = new GuiTools("&6✦  &2" + "Selectionne l'item", "SkyFightSign3", items.size() + 1, LocationUtils.convertLocationToString(sign.getLocation()));

					guiTools.setItem(0, ItemTools.create(Material.BARRIER, 1, (short) 0, "&cSupprimer"));

					for(int i = 1; i < items.size() + 1; i++) {
						guiTools.setItem(i, items.get(i - 1));
					}
					guiTools.openInventory(player);
					return;
				}

				final SkyFightSign skyFightSign = Signs.getSignByLocation(sign.getLocation());
				if(skyFightSign != null) {
					final SkyFightSignItem skyFightSignItem = skyFightSign.getItem();

					SignUtils.setSignLinesWithFormat(sign, skyFightSign);
					Signs.openInventory(player, skyFightSignItem.getName(), skyFightSignItem.getItemStack());
				}
			} else if(block.getType() == Material.BEACON) {
				event.setCancelled(true);
				final List<ItemStack> items = Signs.getSignsItemsAmount()
						.stream()
						.filter(itemStack -> itemStack.getAmount() != 0)
						.peek(itemStack -> itemStack.setAmount(itemStack.getMaxStackSize()))
						.collect(Collectors.toList());
				final GuiTools guiTools = new GuiTools("&6✦  &2" + "Tous les items", "SkyFightSign2", items.size());

				int i = 0;
				for(final ItemStack itemStack : items) {
					guiTools.setItem(i, itemStack);
					i++;
				}

				guiTools.openInventory(player);
			}
		}
	}

	@EventHandler
	private void InventoryClickEvent(final InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();
		final Inventory clickedInventory = event.getClickedInventory();

		if(clickedInventory == null) {
			return;
		}

		final GuiData guiData = GuiTools.getGuiData(player);
		if(guiData != null) {
			switch(guiData.getId()) {
			case "SkyFightSign2":
				if(player.getGameMode().equals(GameMode.CREATIVE)) {
					return;
				}
				event.setCancelled(true);
				break;

			case "SkyFightSign3":
				event.setCancelled(true);
				final String data = guiData.getData();

				final Location signLocation = LocationUtils.convertStringToLocation(data);
				final Block block = signLocation.getBlock();
				final Sign sign = (Sign) block.getState();
				final SkyFightSign skyFightSignOld = Signs.getSignByLocation(sign.getLocation());


				final int slotInt = event.getRawSlot();
				if(slotInt == 0) {
					if(skyFightSignOld != null) {
						Signs.removeSign(skyFightSignOld);
						SignUtils.clearSign(sign);
						player.sendMessage(Utils.color("&2EmeraldMC &7» &aPanneau supprimé !"));
					} else {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe panneau n'existait pas"));
					}
					return;
				}

				final SkyFightSignItem item = SkyFightSignItem.getByID(slotInt);
				if(item == null) {
					return;
				}

				if(skyFightSignOld != null) {
					Signs.removeSign(skyFightSignOld);
				}

				final SkyFightSign skyFightSign = new SkyFightSign(Signs.getSigns().size() + 1, sign.getLocation(), item);
				SignUtils.setSignLinesWithFormat(sign, skyFightSign);
				Signs.addSign(skyFightSign);
				Signs.saveSigns();
				player.sendMessage(Utils.color("&2EmeraldMC &7» &aPanneau ajouté !"));
				break;
			}
		}
	}
}

package fr.tristiisch.emeraldmc.skyfight.sign;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.api.spigot.utils.LocationUtils;
import fr.tristiisch.emeraldmc.skyfight.Main;

public class Signs {

	public static class SkyFightSign {

		final private int id;
		final private Location location;
		final private SkyFightSignItem item;

		public SkyFightSign(final int id, final Location location, final SkyFightSignItem item) {
			this.id = id;
			this.location = location;
			this.item = item;
		}

		public int getId() {
			return this.id;
		}

		public Location getLocation() {
			return this.location;
		}

		public SkyFightSignItem getItem() {
			return this.item;
		}
	}

	private static List<String> signformat = new ArrayList<>();
	private static List<SkyFightSign> signs = new ArrayList<>();

	public static List<String> getSignLinesFormat() {
		return signformat;
	}

	public static void setSignFormat(final List<String> signformat) {
		Signs.signformat = signformat;
	}

	public static String getSignFormatLine(final int i) {
		return signformat.get(i - 1);
	}

	public static SkyFightSign getSignByLocation(final Location location) {
		return signs.stream().filter(skyFightSign -> LocationUtils.isIn(skyFightSign.getLocation(), location)).findFirst().orElse(null);
	}

	public static List<ItemStack> getSignsItemsAmount() {
		return Arrays.stream(SkyFightSignItem.values()).map(SkyFightSignItem::getItemStack).peek(itemStack -> itemStack.setAmount(getSignsAmount(itemStack))).collect(
				Collectors.toList());
	}

	public static int getSignsAmount(final ItemStack itemStack) {
		return (int) signs.stream().filter(sign -> sign.getItem().getItemStack().isSimilar(itemStack)).count();
	}

	public static List<SkyFightSign> getSigns() {
		return signs;
	}

	public static void addSign(final SkyFightSign sign) {
		Signs.signs.add(sign);
	}

	public static void removeSign(final SkyFightSign sign) {
		Signs.signs.remove(sign);
	}

	public static void setSigns(final List<SkyFightSign> signs) {
		Signs.signs = signs;
	}

	public static List<String> getLinesOfSign(final SkyFightSign skyFightSignItem) {
		return SignUtils.setSignLines(new ArrayList<>(signformat), SignUtils.breakSentenceToList(skyFightSignItem.getItem().getName(), 15), ChatColor.DARK_GREEN);
	}

	public static void openInventory(final Player player, final String name, ItemStack itemStack) {
		final GuiTools guiTools = new GuiTools(ChatColor.DARK_GREEN + name, "SkyFightSign", 4 * 9);
		if(itemStack.getType() == Material.POTION) {
			for(int i = 0; i < guiTools.getMiddleSlot(); i++) {
				guiTools.setItem(i, itemStack);
			}
			itemStack = ItemTools.setSplashPotion(itemStack, true);
			for(int i = guiTools.getMiddleSlot(); i < guiTools.getSize(); i++) {
				guiTools.setItem(i, itemStack);
			}

		} else {
			for(int i = 0; i < guiTools.getSize(); i++) {
				guiTools.setItem(i, itemStack);
			}
		}
		guiTools.openInventory(player);
	}

	public static void saveSigns() {
		final FileConfiguration config = Main.getInstance().getConfig();

		for(final SkyFightSign sign : Signs.getSigns()) {
			config.set("sign.save." + sign.getId() + ".pos", LocationUtils.convertLocationToString(sign.getLocation()));
			config.set("sign.save." + sign.getId() + ".item", sign.getItem().getId());
		}
		Main.getInstance().saveConfig();
		getSignsFromConfig();
	}

	public static void getSignsFromConfig() {

		final FileConfiguration config = Main.getInstance().getConfig();
		// get string signs format
		Signs.setSignFormat(config.getStringList("sign.line"));

		// get signs info from config
		final ConfigurationSection section = config.getConfigurationSection("sign.save");
		Signs.getSigns().clear();
		if(section != null) {
			for(final String key : section.getKeys(false)) {
				final SkyFightSign skyFightSign = new SkyFightSign(
						section.getInt(key),
						LocationUtils.convertStringToLocation(section.getString(key + ".pos")),
						SkyFightSignItem.getByID(section.getInt(key + ".item")));
				final Block block = skyFightSign.getLocation().getBlock();
				if(block.getState() instanceof Sign) {
					SignUtils.setSignLinesWithFormat((Sign) block.getState(), skyFightSign);
				}
				Signs.addSign(skyFightSign);
			}
		}

	}
}

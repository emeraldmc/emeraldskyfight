package fr.tristiisch.emeraldmc.skyfight.sign;

import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.skyfight.Main;
import fr.tristiisch.emeraldmc.skyfight.sign.Signs.SkyFightSign;

public class SignUtils {

	public static List<String> replaceInSign(final List<String> signLines, final String toReplace, final String replaced) {
		for(int i = 0; i < signLines.size(); i++) {
			signLines.set(i, signLines.get(i).replaceAll(toReplace, replaced));
		}
		return signLines;
	}

	public static List<String> setSignLines(final List<String> signLines, final List<String> msg, final ChatColor color) {
		int startline = Main.getInstance().getConfig().getInt("sign.startline") - 1;
		if(msg.size() == 1) {
			startline += 1;
		}
		for(final String s : msg) {
			signLines.set(startline, color + s);
			startline++;
			if(startline > signLines.size()) {
				break;
			}
		}
		return signLines;
	}

	public static boolean isSign(final Material material) {
		return material != null && (material == Material.SIGN_POST || material == Material.WALL_SIGN || material == Material.SIGN);
	}

	public static boolean isValidSign(final SkyFightSign skyFightSignItem, final Sign sign) {
		final List<String> signformat2 = Signs.getLinesOfSign(skyFightSignItem);
		for(int i = 0; i < sign.getLines().length; i++) {
			if(!signformat2.get(i).equals(sign.getLine(i))) {
				return false;
			}
		}
		return true;
	}

	public static String breakSentence(final String sentence, final int lineLimit) {
		final String[] words = sentence.split("\\s");
		final StringBuilder builder = new StringBuilder();

		int currentPage = 0;
		int currentWordIndex = 0;
		int totalCharCount = 0;

		while(currentWordIndex < words.length) {
			final int potentialWordLength = words[currentWordIndex].length();
			if(totalCharCount + potentialWordLength < lineLimit) {
				builder.append(words[currentWordIndex]);
				builder.append(" ");
				totalCharCount = builder.length() + 1 - (lineLimit * currentPage - 1);
				currentWordIndex++;
			} else {
				builder.append("\n");
				currentPage++;
				totalCharCount = 0;
			}
		}
		return builder.toString();
	}

	public static List<String> breakSentenceToList(final String sentence, final int lineLimit) {
		return Arrays.asList(breakSentence(sentence, lineLimit).split("\n"));
	}

	public static void setSignLinesWithFormat(final Sign sign, final SkyFightSign skyFightSignItem) {
		final List<String> linesFormatted = Signs.getLinesOfSign(skyFightSignItem);
		for(int i = 0; i < linesFormatted.size(); i++) {
			sign.setLine(i, Utils.color(linesFormatted.get(i)));
		}
		sign.update();
	}

	public static void clearSign(final Sign sign) {
		for(int i = 0; i < sign.getLines().length; i++) {
			sign.setLine(i, "");
		}
		sign.update();
	}

}

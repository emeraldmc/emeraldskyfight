package fr.tristiisch.emeraldmc.skyfight.sign;

import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Matcher;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.skyfight.sign.Signs.SkyFightSign;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class SignCommand implements CommandExecutor {

	int power;

	public SignCommand(final int power) {
		this.power = power;
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			if(sender instanceof Player) {
				final Player player = (Player) sender;
				final EmeraldPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();

				if(emeraldPlayer.hasPowerLessThan(this.power)) {
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
					return;
				}
			}
			if(args.length > 0) {
				switch(args[0].toLowerCase()) {

				case "add":
					if(!(sender instanceof Player)) {
						sender.sendMessage(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole"));
						return;
					}
					final Player player = (Player) sender;

					final Block block = player.getTargetBlock((Set<Material>) null, 5);
					if(!SignUtils.isSign(block.getType())) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous devez viser un panneau."));
						return;
					}

					if(args.length < 2 || !Matcher.isInt(args[1])) {
						sender.sendMessage(Utils.color("&cUsage &7» &c/sign <add> <skyfightsignitemid>"));
						return;
					}

					final SkyFightSignItem item = SkyFightSignItem.getByID(Integer.parseInt(args[1]));
					if(item == null) {
						sender.sendMessage(Utils.color("&cUsage &7» &c/sign <add> <skyfightsignitemid>"));
					}
					final SkyFightSign skyFightSign = new SkyFightSign(Signs.getSigns().size() + 1, block.getLocation(), item);
					SignUtils.setSignLinesWithFormat((Sign) block.getState(), skyFightSign);
					Signs.addSign(skyFightSign);
					Signs.saveSigns();
					sender.sendMessage(Utils.color("&2EmeraldMC &7» &aPanneau ajouté !"));
					break;

				case "remove":
					break;

				case "list":
					if(!(sender instanceof Player)) {
						sender.sendMessage(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole"));
						return;
					}
					final Player player1 = (Player) sender;

					final List<SkyFightSign> signs = Signs.getSigns();
					for(final SkyFightSign sign : signs) {
						final TextComponent msg = new TextComponent(Utils.color("&2EmeraldMC &7» &3Panneau n°" + sign.getId() + " (" + sign.getItem().getMaterial().toString() + ")"));
						final Location loc = sign.getLocation();
						msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tp " + loc.getBlockX() + ".5 " + loc.getBlockY() + " " + loc.getBlockZ() + ".5 "));
						player1.spigot().sendMessage(msg);
					}
					break;

				case "listall":
					if(!(sender instanceof Player)) {
						sender.sendMessage(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole"));
						return;
					}
					final Player player2 = (Player) sender;
					player2.sendMessage(Utils.color("&2EmeraldMC &7» &aRecherche en cours de panneaux..."));
					int i = 1;
					final World w = Bukkit.getWorlds().get(0);
					for(final Chunk c : Bukkit.getWorlds().get(0).getLoadedChunks()) {
						final int cx = c.getX() << 4;
						final int cz = c.getZ() << 4;
						for (int x = cx; x < cx + 16; x++) {
							for (int z = cz; z < cz + 16; z++) {
								for (int y = 0; y < 256; y++) {
									final Block block2 = w.getBlockAt(x, y, z);
									if(SignUtils.isSign(block2.getType())) {
										final Location loc = block2.getLocation();
										if(!((Sign) block2.getState()).getLine(0).isEmpty() || !((Sign) block2.getState()).getLine(1).isEmpty() || !((Sign) block2.getState()).getLine(2).isEmpty() || !((Sign) block2.getState()).getLine(3).isEmpty()) {
											final TextComponent msg = new TextComponent(Utils.color("&2EmeraldMC &7» &3Panneau n°" + i));
											msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tp " + loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ()));
											player2.spigot().sendMessage(msg);
											i++;
										}
									}
								}
							}
						}
					}
					player2.sendMessage(Utils.color("&2EmeraldMC &7» &aRecherche terminée !"));
					break;

				case "reload":
					Signs.getSignsFromConfig();
					break;

				default:
					sender.sendMessage(Utils.color("&cUsage &7» &c/sign <add|remove|list|reload> [item|signid]"));
				}


			} else {
				sender.sendMessage(Utils.color("&cUsage &7» &c/sign <add|remove|list>"));
			}
		});

		return true;
	}
}

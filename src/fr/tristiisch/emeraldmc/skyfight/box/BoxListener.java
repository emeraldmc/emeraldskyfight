package fr.tristiisch.emeraldmc.skyfight.box;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.Prefix;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.customevent.PlayerBuyKeyEvent;
import fr.tristiisch.emeraldmc.api.spigot.group.SpigotGroup;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools.GuiData;
import fr.tristiisch.emeraldmc.api.spigot.sneak.Sneak;
import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;
import fr.tristiisch.emeraldmc.skyfight.box.objects.BoxItem;
import fr.tristiisch.emeraldmc.skyfight.box.objects.BoxItemType;

public class BoxListener implements Listener {

	@EventHandler
	public void onInventoryClick(final InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();
		final Inventory clickedInventory = event.getClickedInventory();

		if(clickedInventory == null) {
			return;
		}

		final GuiData guiData = GuiTools.getGuiData(player);
		if(guiData != null) {
			switch(guiData.getId()) {
			case "boxView":
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onPlayerBuyKey(final PlayerBuyKeyEvent event) {
		final Player player = event.getPlayer();
		final ItemStack itemStack = Box.getKeyWithGlow();
		itemStack.setAmount(event.getAmount());
		player.getInventory().addItem(itemStack);
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerInteract(final PlayerInteractEvent event) {
		final Player player = event.getPlayer();
		final Block block = event.getClickedBlock();
		if(block != null) {
			final Location blockLocation = block.getLocation();

			if(Box.getBoxLocation().equals(blockLocation)) {
				event.setCancelled(true);
				if(Sneak.isSneak(player)) {
					final List<BoxItem> boxItems = BoxItem.getBoxItems();
					final ItemStack[] items = boxItems.stream().map(BoxItem::getItemInfo).collect(Collectors.toList()).toArray(new ItemStack[boxItems.size()]);
					final GuiTools gui = new GuiTools("&2Box", "boxView", boxItems.size());
					gui.setItem(items);
					gui.openInventory(player);
				} else {
					/*					if(!player.isOp()) {
											player.sendMessage(Prefix.DEFAULT + Utils.color("&bLa box arrive bientôt, faites &e&lshift-clic&b pour voir les lots !"));
											return;
										}*/
					if(!Box.hasKey(player)) {
						player.sendMessage(Prefix.DEFAULT_BAD + Utils.color("Vous devez avoir une &2Clef de la Box &cpour ouvrir la box."));
						return;
					}
					Box.removeKey(player);
					this.openBox(player);

				}
			}
		}
	}

	public void openBox(final Player player) {
		/*				final GuiTools gui = new GuiTools("&2Box", "box", 3L);
		gui.setData("0");
		final List<BoxItem> items = Arrays.asList(BoxItem.values());
		final String taskName = TaskManager.getTaskName("box");
		gui.openInventory(player);
		TaskManager.scheduleSyncRepeatingTask(taskName, () -> {

			final GuiData guiData = GuiTools.getGuiData(player);
			if(guiData == null) {
				TaskManager.cancelTaskByName(taskName);
				return;
			}

			int i2 = 0;
			for(int slot = 9; slot < 18; slot++) {

				final ItemStack item = items.get(i3).getItem().clone();
				if(gui.getMiddleSlot() == slot) {
					ItemTools.addGlow(item);
				}
				gui.setItem(slot, item);
			}
			i2++;
		}, 0, 2);*/
		TaskManager.runTaskAsynchronously(() -> {
			final AccountProvider accountProvider = new AccountProvider(player.getUniqueId());
			final EmeraldPlayer emeraldPlayer = accountProvider.getEmeraldPlayer();
			final BoxItem boxItem = BoxItem.getRandomBoxItem();
			if(boxItem.getBoxType().equals(BoxItemType.IG)) {

				boxItem.give(player);

			} else if(boxItem.getBoxType().equals(BoxItemType.RANK)) {

				final EmeraldGroup group = EmeraldGroup.getByName(boxItem.toString());

				if(group == null) {
					player.sendMessage(Prefix.DEFAULT_BAD + "Une erreur est survenu.");
					return;
				}

				if(emeraldPlayer.getGroup().getPower() > group.getPower()) {
					final ConsoleCommandSender console = Bukkit.getConsoleSender();
					console.sendMessage(
							Prefix.DEFAULT_BAD + Utils.color(player.getName() + " Votre grade est supérieur au grade &a(" + group.getColor() + group.getName() + "&a) que vous venez de gagné."));

					this.openBox(player);
					return;
				}

				SpigotGroup.addGroup(player, accountProvider, emeraldPlayer, group, Calendar.MONTH, 2);

				Box.sendMessage("&6&l" + player.getName() + "&e vient de gagner un grade &6&l" + group.getColor() + group.getName() + " 2 mois&e dans une box !");
				Bukkit.getOnlinePlayers().forEach(players -> {
					players.playSound(players.getLocation(), Sound.FIREWORK_LAUNCH, 3.0F, 0.5F);
				});

				player.sendMessage(Prefix.DEFAULT_GOOD + Utils.color("Vous venez de gagner le grade " + group.getColor() + group.getName() + "&a 2 mois !"));

			} else {
				Box.sendMessage("&6&l" + player.getName() + "&e vient de gagner le lot IRL &6&l" + boxItem.getName() + " &edans une box !");
				Bukkit.getOnlinePlayers().forEach(players -> {
					players.playSound(players.getLocation(), Sound.FIREWORK_LAUNCH, 3.0F, 0.5F);
				});
				player.sendMessage(
						Prefix.DEFAULT_GOOD + Utils.color("Vous venez de gagner le lot IRL &2" + boxItem.getName() + " &a; venez sur le teamspeak &e&n&lts.emeraldmc.fr&a pour recevoir votre lot."));
				boxItem.removeItem();
			}
			BoxMySQL.add(emeraldPlayer, boxItem);
		});
	}
}

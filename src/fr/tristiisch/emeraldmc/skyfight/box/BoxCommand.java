package fr.tristiisch.emeraldmc.skyfight.box;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import fr.tristiisch.emeraldmc.api.commons.Prefix;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.command.SpigotCommand;
import fr.tristiisch.emeraldmc.skyfight.box.objects.BoxItemInfo;

public class BoxCommand extends SpigotCommand {

	public BoxCommand(final Plugin plugin, final EmeraldGroup... groups) {
		super(plugin, "box", groups);
		this.minArg = 1;
		this.usageString = "/box <key|info> [player]";
		this.allowConsole = false;
		this.register();
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		if(args[0].equalsIgnoreCase("key")) {

			final Player target;
			if(args.length >= 2) {
				target = Bukkit.getPlayer(args[1]);
				if(target == null) {
					this.sendUnknownPlayer(args[1]);
					return true;
				}
			} else {
				target = this.player;
			}
			target.getInventory().addItem(Box.getKeyWithGlow());
			this.sendMessage(target, Prefix.DEFAULT_GOOD, "Vous venez de reçevoir une clef !");

		} else if(args[0].equalsIgnoreCase("info")) {
			if(args.length < 2) {
				this.sendUsage();
				return true;
			}

			final EmeraldPlayer emeraldTarget = MySQL.getPlayer(args[1]);
			if(emeraldTarget == null) {
				this.sendUnknownPlayer(args[1]);
				return true;
			}

			final List<BoxItemInfo> boxItemInfos = BoxMySQL.get(emeraldTarget);
			if(boxItemInfos.isEmpty()) {
				this.sendErreur("Le joueur &4" + emeraldTarget.getName() + "&c n'a jamais utilisé de clef.");
				return true;
			}
			final List<String> msgs = new ArrayList<>();
			ChatColor color = null;
			for(final BoxItemInfo boxItemInfo : boxItemInfos) {
				if(color == ChatColor.GOLD) {
					color = ChatColor.YELLOW;
				} else {
					color = ChatColor.GOLD;
				}
				msgs.add(color + boxItemInfo.getTime() + " ➜ " + boxItemInfo.getBoxItem().getName());
			}
			this.sendMessage(String.join("\n", msgs));

		} else {
			this.sendUsage();
		}
		return true;

	}

}

package fr.tristiisch.emeraldmc.skyfight.box;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemGlow;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.api.spigot.utils.LocationUtils;
import fr.tristiisch.emeraldmc.skyfight.Main;

public class Box {

	final private static Location BOX_LOCATION = LocationUtils.convertStringToLocation(Main.getInstance().getConfig().getString("box"));
	final private static ItemStack KEY = ItemTools.create(Material.TRIPWIRE_HOOK, 1, "&2Clef de la Box", Arrays.asList("", "&aUtilise la sur la box au spawn !"), ItemGlow.getItemGlowEnchant());
	final private static String PREFIX = "&7[&6&lBOX&7] &a";

	public static Location getBoxLocation() {
		return BOX_LOCATION;
	}

	public static ItemStack getKey() {
		final ItemStack key2 = KEY.clone();
		ItemTools.removeGlow(key2);
		return key2;
	}

	public static ItemStack getKey(final int i) {
		final ItemStack key2 = getKey();
		key2.setAmount(i);
		return key2;
	}

	public static ItemStack getKeyWithGlow() {
		return KEY;
	}

	public static ItemStack getKeyWithGlow(final int i) {
		final ItemStack key2 = KEY.clone();
		key2.setAmount(i);
		return key2;
	}

	public static boolean hasKey(final Player player) {
		final PlayerInventory playerInventory = player.getInventory();
		final ItemStack[] items = playerInventory.getContents();
		final ItemStack key = getKeyWithGlow();

		return Arrays.stream(items).filter(item -> item != null && item.isSimilar(key)).findFirst().isPresent();
	}

	public static void removeKey(final Player player) {
		player.getInventory().removeItem(Box.getKeyWithGlow());
	}

	public static void sendMessage(final String message) {
		Bukkit.broadcastMessage(Utils.color(PREFIX + message));
	}
}

package fr.tristiisch.emeraldmc.skyfight.box;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.DatabaseManager;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.skyfight.box.objects.BoxItem;
import fr.tristiisch.emeraldmc.skyfight.box.objects.BoxItemInfo;
import fr.tristiisch.emeraldmc.skyfight.box.objects.BoxItemType;

public class BoxMySQL {

	public static boolean add(final EmeraldPlayer emeraldPlayer, final BoxItem boxItem) {
		try {
			final PreparedStatement pstate = DatabaseManager.getConnection().prepareStatement("INSERT INTO box (player_id, time, boxitem) VALUES (?, ?, ?);");
			pstate.setInt(1, emeraldPlayer.getId());
			pstate.setLong(2, Utils.getCurrentTimeinSeconds());
			pstate.setString(3, boxItem.toString());
			pstate.executeUpdate();
		} catch(final SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static List<BoxItem> get(final BoxItemType boxItemType) {
		final List<BoxItem> boxItems = new ArrayList<>();
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM box;");
			while(resultSet.next()) {
				final BoxItem boxItem = BoxItem.getBoxItem(resultSet.getString("boxitem"));

				if(boxItem.getBoxType().equals(boxItemType)) {
					boxItems.add(boxItem);
				}
			}
		} catch(final SQLException e) {
			e.printStackTrace();
			return null;
		}
		return boxItems;
	}

	public static List<BoxItemInfo> get(final EmeraldPlayer emeraldPlayer) {
		final List<BoxItemInfo> boxItemInfos = new ArrayList<>();
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM box WHERE player_id = " + emeraldPlayer.getId() + ";");
			while(resultSet.next()) {
				boxItemInfos.add(new BoxItemInfo(resultSet.getInt("id"), resultSet.getLong("time"), BoxItem.getBoxItem(resultSet.getString("boxitem"))));
			}
		} catch(final SQLException e) {
			e.printStackTrace();
			return null;
		}
		return boxItemInfos;
	}
}

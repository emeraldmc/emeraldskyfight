package fr.tristiisch.emeraldmc.skyfight.box.objects;

import fr.tristiisch.emeraldmc.api.commons.Utils;

public class BoxItemInfo {

	private final int emeraldPlayerId;
	private final long time;
	private final BoxItem boxItem;

	public BoxItemInfo(final int emeraldPlayerId, final long time, final BoxItem boxItem) {
		this.emeraldPlayerId = emeraldPlayerId;
		this.time = time;
		this.boxItem = boxItem;
	}

	public int getEmeraldPlayerId() {
		return this.emeraldPlayerId;
	}

	public String getTime() {
		return Utils.timestampToDateAndHour(this.time);
	}

	public BoxItem getBoxItem() {
		return this.boxItem;
	}

}

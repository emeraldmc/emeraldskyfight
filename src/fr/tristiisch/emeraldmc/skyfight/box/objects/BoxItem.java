package fr.tristiisch.emeraldmc.skyfight.box.objects;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import fr.tristiisch.emeraldmc.api.commons.Prefix;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemArmor;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemEnchant;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;
import fr.tristiisch.emeraldmc.skyfight.box.Box;
import fr.tristiisch.emeraldmc.skyfight.box.BoxMySQL;
import fr.tristiisch.emeraldmc.skyfight.customcraft.CustomCraft;
import fr.tristiisch.emeraldmc.skyfight.stockender.StockEnder;

public enum BoxItem {

	BOW(BoxItemType.IG, "Arc Puissance 5", ItemTools.create(Material.BOW, new ItemEnchant(Enchantment.ARROW_DAMAGE, 5)), 0, 11f),

	CLAVIER(BoxItemType.IRL, "Clavier mécanique", ItemTools.create(Material.STAINED_CLAY, (short) 1), 60, 0.25f),

	DIAMANT(BoxItemType.RANK, "Grade Diamant 2 mois 22€", ItemTools.create(Material.DIAMOND_BLOCK), 11, 2f),

	DIAMOND_ARMOR(BoxItemType.IG, "Armure en Diamant P4", ItemTools.create(Material.DIAMOND_CHESTPLATE), 0, 7f,
			ItemTools.createArmor(ItemArmor.DIAMOND, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4))),

	DIAMOND_ARMOR2(BoxItemType.IG, "Armure en Diamant P2", ItemTools.create(Material.DIAMOND_HELMET), 0, 12f,
			ItemTools.createArmor(ItemArmor.DIAMOND, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2))),

	DIAMOND_SWORD(BoxItemType.IG, "Épée en Diamant T4 KB 1", ItemTools.create(Material.DIAMOND_SWORD, new ItemEnchant(Enchantment.DAMAGE_ALL, 4), new ItemEnchant(Enchantment.KNOCKBACK)), 0, 12f),

	EMERAUDE(BoxItemType.RANK, "Grade Emeraude 2 mois 28€", ItemTools.create(Material.EMERALD_BLOCK), 14, 1f),

	EMERAUDES(BoxItemType.IG, "Bloc d'émeraudes x20", CustomCraft.getEmeraldBlock(20), 0, 15f, CustomCraft.getEmeraldBlock(20)),

	EMERAUDES2(BoxItemType.IG, "Bloc d'émeraudes x40", CustomCraft.getEmeraldBlock(40), 0, 9f, CustomCraft.getEmeraldBlock(40)),

	KEY(BoxItemType.IG, "2x Clef de la Box", Box.getKey(2), 3, 10f, Box.getKeyWithGlow(2)),

	ROD(BoxItemType.IG, "Canne à pêches", ItemTools.create(Material.FISHING_ROD), 0, 6f),

	RUBIS(BoxItemType.RANK, "Grade Rubis 2 mois 10€", ItemTools.create(Material.NETHERRACK), 5, 4f),

	SAPHIR(BoxItemType.RANK, "Grade Saphir 2 mois 16€", ItemTools.create(Material.LAPIS_BLOCK), 8, 3f),

	SOURIS(BoxItemType.IRL, "Souris Gamer RGB AUKEY 30€", ItemTools.create(Material.STAINED_CLAY, (short) 2), 30, 0.25f),

	SOURIS2(BoxItemType.IRL, "Souris Gamer AUKEY 30€", ItemTools.create(Material.STAINED_CLAY, (short) 3), 30, 0.25f),

	SOURIS3(BoxItemType.IRL, "Souris Ergonomique AUKEY 30€", ItemTools.create(Material.STAINED_CLAY, (short) 4), 30, 1f),

	SOURIS4(BoxItemType.IRL, "Souris dédicacée par Kimdead à 1€ sur Ebay", ItemTools.create(Material.STAINED_CLAY, (short) 5), 30, 1f),

	STOCKENDER(BoxItemType.IG, "StockEnder", StockEnder.getItem(), 0, 5f),

	WEBCAM(BoxItemType.IRL, "Webcam HD 1080p AUKEY 30€", ItemTools.create(Material.STAINED_CLAY), 30, 0.25f);

	private static final Random RANDOM = new Random();
	private static final List<BoxItem> removed;

	static {
		removed = BoxMySQL.get(BoxItemType.IRL);
		calculePercentageAccrued();
	}

	public static void calculePercentageAccrued() {
		final double chance = (int) Arrays.stream(BoxItem.values()).mapToDouble(BoxItem::getPercentage).sum();
		if(chance != 100) {
			final ConsoleCommandSender console = Bukkit.getConsoleSender();
			console.sendMessage(Utils.color("&cERROR BOX &7» &cLe pourcentage de chance total est de " + chance + "%."));
		}

		final double rest = Arrays.stream(BoxItem.values()).filter(boxItem -> removed.contains(boxItem)).mapToDouble(boxItem -> boxItem.getPercentage()).sum();
		final int nbBoxItemIG = (int) Arrays.stream(BoxItem.values()).filter(boxItem -> boxItem.getBoxType().equals(BoxItemType.IG)).count();
		final double toGive = rest / nbBoxItemIG;

		for(final BoxItem boxItem : BoxItem.values()) {
			if(boxItem.getBoxType().equals(BoxItemType.IG)) {
				boxItem.setPercentage(boxItem.getPercentage() + toGive);
			}
		}

		double accrued = 0;

		for(final BoxItem boxItem : getBoxItems()) {
			accrued += boxItem.getPercentage();
			boxItem.setPercentageAccrued(accrued);

			final ConsoleCommandSender console = Bukkit.getConsoleSender();
			console.sendMessage(Utils.color("&5DEBUG &7» &e" + boxItem.getBoxType() + " " + boxItem.getName() + " " + boxItem.getPercentage() + "% " + boxItem.getPercentageAccrued() + "%"));

		}

	}

	public static BoxItem getBoxItem(final double percentageAccrued) {
		final List<BoxItem> boxItems = getBoxItems();

		for(int i = 0; i < boxItems.size(); i++) {
			final BoxItem boxItem1 = boxItems.get(i);
			if(boxItems.size() - 1 == i) {
				return boxItem1;
			}
			final BoxItem boxItem2 = boxItems.get(i + 1);

			if(percentageAccrued >= boxItem1.getPercentageAccrued() && percentageAccrued < boxItem2.getPercentageAccrued()) {
				final ConsoleCommandSender console = Bukkit.getConsoleSender();
				console.sendMessage(
						Utils.color("&5DEBUG2 &7» &e" + percentageAccrued + " >= " + boxItem1.getPercentageAccrued() + " && " + percentageAccrued + " < " + boxItem2.getPercentageAccrued()));

				return boxItem2;
			}
		}
		return null;
	}

	public static BoxItem getBoxItem(final String name) {
		return Arrays.stream(BoxItem.values()).filter(boxItem -> boxItem.toString().equalsIgnoreCase(name)).findFirst().orElse(null);
	}

	public static List<BoxItem> getBoxItems() {
		return Arrays.stream(BoxItem.values()).filter(boxItem -> !removed.contains(boxItem)).sorted(Comparator.comparing(BoxItem::getPercentage)).collect(Collectors.toList());
	}

	public static BoxItem getRandomBoxItem() {
		return getBoxItem(Utils.nextFloat(RANDOM, 0, 100));
	}

	final BoxItemType boxType;
	final ItemStack item;
	final ItemStack[] items;

	final String name;

	double percentage;

	double percentageAccrued;

	final int price;

	private BoxItem(final BoxItemType boxType, final String name, final ItemStack item, final int price, final double percentage, final ItemStack... items) {
		this.boxType = boxType;
		this.name = name;
		this.item = item;
		this.price = price;
		this.percentage = percentage;
		this.items = items;
	}

	public BoxItemType getBoxType() {
		return this.boxType;
	}

	public ItemStack getItemInfo() {
		final ItemStack itemStack = this.item.clone();
		ItemTools.setDisplayName(itemStack, "&7" + this.name);

		ItemTools.setLore(itemStack, Arrays.asList("", " &6➤ &e" + Utils.round(this.getPercentage(), 2) + "% de chance"));
		return itemStack;
	}

	public String getName() {
		return this.name;
	}

	public double getPercentage() {
		return this.percentage;
	}

	public double getPercentageAccrued() {
		return this.percentageAccrued;
	}

	public int getPrice() {
		return this.price;
	}

	public void give(final Player player) {
		ItemStack[] items = this.items;
		if(items.length == 0) {
			items = new ItemStack[1];
			items[0] = this.item;
		}
		System.out.println("GIVE " + items[0].getType() + " " + items[0].getAmount());
		final PlayerInventory playerInventory = player.getInventory();
		if(SpigotUtils.hasEnoughPlace(playerInventory, items)) {
			playerInventory.addItem(items);
		} else {
			player.sendMessage(Prefix.DEFAULT_BAD + "Votre inventaire est plein, les objets sont au sol.");
			final ItemStack[] items2 = items;
			TaskManager.runTask(() -> Arrays.stream(items2).forEach(item -> player.getWorld().dropItemNaturally(player.getEyeLocation(), item)));
		}

		player.sendMessage(Prefix.DEFAULT_GOOD + "Vous venez de gagné " + this.getName() + " !");
	}

	public void removeItem() {
		removed.add(this);
		calculePercentageAccrued();
	}

	public void setPercentage(final double percentage) {
		this.percentage = percentage;
	}

	public void setPercentageAccrued(final double percentageAccrued) {
		this.percentageAccrued = percentageAccrued;
	}

}

package fr.tristiisch.emeraldmc.skyfight.stats;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayers;
import fr.tristiisch.emeraldmc.skyfight.mysql.MySQLSkyFight;
import fr.tristiisch.emeraldmc.skyfight.scoreboard.Scoreboards;

public class StatsCommand implements CommandExecutor {

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		Player player = null;
		if(sender instanceof Player) {
			player = (Player) sender;
		}

		if(args.length == 0) {

		} else {
			final Player player2 = player;
			Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
				if(player2 != null) {
					final EmeraldPlayer emeraldPlayer = new AccountProvider(player2.getUniqueId()).getEmeraldPlayer();
					if(emeraldPlayer.hasPowerLessThan(75)) {
						player2.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
						return;
					}
				}

				if(args[0].equalsIgnoreCase("reset")) {
					MySQLSkyFight.getAllPlayers().entrySet().stream().forEach(entry -> {
						entry.getValue().resetKill();
						entry.getValue().resetDeath();
						entry.getValue().resetXP();
						MySQLSkyFight.savePlayer(entry.getKey(), entry.getValue());
					});

					SkyFightPlayers.getPlayers().entrySet().stream().forEach(entry -> {
						entry.getValue().resetKill();
						entry.getValue().resetDeath();
						entry.getValue().resetXP();
						Scoreboards.updateScoreboards(entry.getKey());
					});

					sender.sendMessage(Utils.color("&2EmeraldMC &7» &cLes kills, morts et XP ont été reset pour tous les joueurs."));
				}
			});
		}

		return true;
	}

}

package fr.tristiisch.emeraldmc.skyfight.zap;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.spigot.utils.Cuboid;

public class SkyFightZap {

	private static int ID = 0;

	final private String name;
	final private Cuboid cuboidLocation;
	final private Location tpLocation;
	private ItemStack item;
	private int slot;
	private int price = 0;
	final private int id;

	public SkyFightZap(final String name, final Cuboid cuboidLocation, final Location tpLocation) {
		this.name = name;
		this.tpLocation = tpLocation;
		this.cuboidLocation = cuboidLocation;
		this.id = ID++;
	}

	public String getName() {
		return this.name;
	}

	public Location getTpLocation() {
		return this.tpLocation;
	}

	public Cuboid getCuboidLocation() {
		return cuboidLocation;
	}

	public ItemStack getItem() {
		return this.item;
	}

	public void setItem(final int slot, final ItemStack item) {
		this.slot = slot;
		this.item = item;
	}

	public int getSlot() {
		return this.slot;
	}

	public int getId() {
		return this.id;
	}

	public int getPrice() {
		return this.price;
	}

	public void setPrice(final int price) {
		this.price = price;
	}
}

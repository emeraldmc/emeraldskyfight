package fr.tristiisch.emeraldmc.skyfight.zap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.customevent.PlayerMoveBlockEvent;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools.GuiData;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.api.spigot.utils.CustomConfigUtils;
import fr.tristiisch.emeraldmc.api.spigot.utils.LocationUtils;
import fr.tristiisch.emeraldmc.skyfight.Main;
import fr.tristiisch.emeraldmc.skyfight.scoreboard.Scoreboards;

public class ZapListener implements Listener {

	List<SkyFightZap> zaps = new ArrayList<>();
	ItemStack cancelItem = ItemTools.create(Material.REDSTONE_BLOCK, 1, (short) 0, Utils.color("&4✖ &lImpossible"), Arrays.asList("", Utils.color("&c%reason%"), ""));

	int price = 5;

	public ZapListener() {
		final FileConfiguration config = CustomConfigUtils.getConfig(Main.getInstance(), "config");

		final ConfigurationSection section = config.getConfigurationSection("zap.1");
		for(final String key : section.getKeys(false)) {
			final SkyFightZap zap = new SkyFightZap(
					section.getString(key + ".item.name"),
					LocationUtils.getCuboid(section, key),
					LocationUtils.convertStringToLocation(section.getString(key + ".tp")));

			final String name = Utils.color("&7" + section.getString(key + ".item.name"));
			final Material material = Material.getMaterial(section.getString(key + ".item.type"));
			if(material == null) {
				throw new NullPointerException("Le type de l'item du zap n°" + key + " est introuvable.");
			}
			final short data = Short.parseShort(section.getString(key + ".item.data"));
			final int slot = section.getInt(key + ".item.slot");

			final List<String> lore = section.getStringList(key + ".item.lore");
			for(int i = 0; lore.size() > i; i++) {
				lore.set(i, Utils.color(lore.get(i)));
			}

			zap.setItem(slot, ItemTools.create(material, 1, data, name, lore));
			zap.setPrice(section.getInt(key + ".price"));
			this.zaps.add(zap);
		}

	}

	@EventHandler
	public void PlayerMoveBlockEvent(final PlayerMoveBlockEvent event) {
		final Player player = event.getPlayer();
		final SkyFightZap zap = this.zaps.stream().filter(zaap -> zaap.getCuboidLocation().isIn(event.getPlayerMoveEvent().getTo())).findFirst().orElse(null);
		if(zap != null) {

			final SkyFightZap zapOld = this.zaps.stream().filter(zaap -> zaap.getCuboidLocation().isIn(event.getPlayerMoveEvent().getFrom())).findFirst().orElse(null);
			if(zapOld != null) {
				return;
			}

			final GuiTools gui = new GuiTools("&7Zap " + zap.getName(), "zap", 3 * 9, String.valueOf(zap.getId()));
			for(final SkyFightZap zaap : this.zaps) {
				if(zap.getId() == zaap.getId()) {
					final ItemStack zaap2 = ItemTools
							.create(Material.BARRIER, 1, (short) 0, Utils.color(zaap.getItem().getItemMeta().getDisplayName()), Arrays.asList("", Utils.color("&4➤ &cVous êtes déjà sur ce zap."), ""));
					gui.setItem(zaap.getSlot(), zaap2);
				} else {
					gui.setItem(zaap.getSlot(), zaap.getItem());
				}
			}
			gui.openInventory(player);
		}
	}

	@EventHandler
	private void InventoryClickEvent(final InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();
		final GuiData guiData = GuiTools.getGuiData(player);
		if(guiData == null) {
			return;
		}
		switch(guiData.getId()) {
		case "zap":
			event.setCancelled(true);
			final SkyFightZap thisZap = this.zaps.get(Integer.parseInt(guiData.getData()));
			SkyFightZap newZap = null;
			for(final SkyFightZap zap : this.zaps) {
				if(event.getRawSlot() == zap.getSlot() && event.getCurrentItem().isSimilar(zap.getItem())) {
					newZap = zap;
				}
			}
			if(newZap == null) {
				return;
			}

			if(thisZap.equals(newZap)) {
				this.cancel(event, "Vous êtes déjà sur ce zap.");
				return;
			}
			final SkyFightZap newZap2 = newZap;
			Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
				String StringPrice = "";
				if(newZap2.getPrice() > 0) {
					final AccountProvider accountProvider = new AccountProvider(player.getUniqueId());
					final EmeraldPlayer emeraldPlayer = accountProvider.getEmeraldPlayer();
					if(!emeraldPlayer.removePierres(newZap2.getPrice())) {
						this.cancel(event, "Vous n'avez pas assez de pierres.");
						return;
					}
					Scoreboards.updateScoreboardSign(player, emeraldPlayer);
					accountProvider.sendAccountToRedis(emeraldPlayer);
					StringPrice = " (&c- " + newZap2.getPrice() + " pierres&3)";
				}
				player.sendMessage(Utils.color("&2EmeraldMC &7» &3Téléportation au zap " + newZap2.getName() + StringPrice + "."));
				player.teleport(newZap2.getTpLocation());
				player.playSound(player.getEyeLocation(), Sound.PORTAL_TRAVEL, (float) 0.2, 0);
			});

		}
	}

	private void cancel(final InventoryClickEvent event, final String reason) {
		event.setCancelled(true);
		final Player player = (Player) event.getWhoClicked();
		final ItemStack item = event.getCurrentItem();
		final ItemStack cancelItem2 = ItemTools.replaceInLore(this.cancelItem, "%reason%", reason);

		event.getInventory().setItem(event.getRawSlot(), cancelItem2);
		player.playSound(player.getLocation(), Sound.VILLAGER_NO, 1, 1);
		Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> event.getInventory().setItem(event.getRawSlot(), item), 30);
	}
}

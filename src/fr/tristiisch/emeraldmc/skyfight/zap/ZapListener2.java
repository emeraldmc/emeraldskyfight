package fr.tristiisch.emeraldmc.skyfight.zap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.customevent.PlayerMoveBlockEvent;
import fr.tristiisch.emeraldmc.api.spigot.utils.CustomConfigUtils;
import fr.tristiisch.emeraldmc.api.spigot.utils.LocationUtils;
import fr.tristiisch.emeraldmc.skyfight.Main;

public class ZapListener2 implements Listener {

	List<SkyFightZap> zaps = new ArrayList<>();

	public ZapListener2() {
		final FileConfiguration config = CustomConfigUtils.getConfig(Main.getInstance(), "config");

		final ConfigurationSection section = config.getConfigurationSection("zap.2");
		for(final String key : section.getKeys(false)) {
			final SkyFightZap zap = new SkyFightZap(
					section.getString(key + ".name"),
					LocationUtils.getCuboid(section, key),
					LocationUtils.convertStringToLocation(section.getString(key + ".tp")));
			this.zaps.add(zap);
		}
	}

	@EventHandler
	public void PlayerMoveBlockEvent(final PlayerMoveBlockEvent event) {
		final Player player = event.getPlayer();
		final SkyFightZap thisZap = this.zaps.stream().filter(zaap -> zaap.getCuboidLocation().isIn(event.getPlayerMoveEvent().getTo())).findFirst().orElse(null);
		if(thisZap != null) {
			final List<SkyFightZap> zaps2 = new ArrayList<>(this.zaps);
			zaps2.remove(thisZap);
			Collections.shuffle(this.zaps);
			for(final SkyFightZap allZaps : this.zaps) {
				if(thisZap.getId() != allZaps.getId()) {
					player.teleport(allZaps.getTpLocation());
					player.sendMessage(Utils.color("&2EmeraldMC &7» &3Téléportation au zap " + allZaps.getName()));
					return;
				}
			}
		}
	}
}

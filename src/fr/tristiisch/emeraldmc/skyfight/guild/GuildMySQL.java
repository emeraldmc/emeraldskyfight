package fr.tristiisch.emeraldmc.skyfight.guild;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.DatabaseManager;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.Guild;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.GuildType;

public class GuildMySQL {

	public static boolean createNewGuild(final Guild guild){
		try {
			final PreparedStatement pstate = DatabaseManager.getConnection()
					.prepareStatement("INSERT INTO `guild` (`id`, `name`, `tag`, `type`, `members`, `limit`, `xp`) VALUES (?, ?, ?, ?, ?, ?, ?)");
			int i = 1;
			pstate.setInt(i++, guild.getID());
			pstate.setString(i++, guild.getName());
			pstate.setString(i++, guild.getTag());
			pstate.setInt(i++, guild.getGuildType().getID());
			pstate.setString(i++, guild.getMembersToString());
			pstate.setLong(i++, guild.getLimit());
			pstate.setFloat(i++, guild.getXP());
			pstate.executeUpdate();
			pstate.close();
		} catch (final SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean saveGuild(final Guild guild){

		try {
			final PreparedStatement pstate = DatabaseManager.getConnection().prepareStatement("UPDATE `guild` SET `members` = ?, `xp` = ?, `friendlyfire` = ? WHERE `id` = ?;");
			int i = 1;
			pstate.setString(i++, guild.getMembersToString());
			pstate.setFloat(i++, guild.getXP());
			pstate.setBoolean(i++, guild.isFriendlyFire());
			pstate.setInt(i++, guild.getID());
			pstate.executeUpdate();
			pstate.close();
		} catch (final SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static boolean dissolvedGuild(final Guild guild){

		try {
			final PreparedStatement pstate = DatabaseManager.getConnection().prepareStatement("UPDATE `guild` SET `members` = ?, `dissolved` = ? WHERE `id` = ?;");
			int i = 1;
			pstate.setString(i++, guild.getMembersToString());
			pstate.setInt(i++, 1);
			pstate.setInt(i++, guild.getID());
			pstate.executeUpdate();
			pstate.close();
		} catch (final SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;

	}

	public static Guild getGuild(final UUID member){
		Guild guild = null;
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM `guild` WHERE `members` LIKE '%{" + member + "}%' AND `dissolved` = 0");
			if(resultSet.next()) {
				guild = new Guild(
						resultSet.getInt("id"),
						resultSet.getString("name"),
						resultSet.getString("tag"),
						GuildType.getGuildType(resultSet.getInt("type")),
						Guild.getMembersFromString(resultSet.getString("members")),
						resultSet.getInt("limit"),
						resultSet.getInt("xp"),
						resultSet.getBoolean("friendlyFire")
						);
			}
		} catch (final SQLException e) {
			e.printStackTrace();
			return null;
		}


		return guild;
	}

	public static Guild getGuild(final int id) {
		Guild guild = null;
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM `guild` WHERE `id` = " + id + " AND `dissolved` = 0");
			while(resultSet.next()) {
				guild = new Guild(
						resultSet.getInt("id"),
						resultSet.getString("name"),
						resultSet.getString("tag"),
						GuildType.getGuildType(resultSet.getInt("type")),
						Guild.getMembersFromString(resultSet.getString("members")),
						resultSet.getInt("limit"),
						resultSet.getInt("xp"),
					resultSet.getBoolean("friendlyFire"));
			}
		} catch(final SQLException e) {
			e.printStackTrace();
			return null;
		}

		return guild;
	}

	public static Guild getGuild(final String name) {
		Guild guild = null;
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM `guild` WHERE `name` = " + name + " AND `dissolved` = 0");
			while(resultSet.next()) {
				guild = new Guild(
					resultSet.getInt("id"),
					resultSet.getString("name"),
					resultSet.getString("tag"),
					GuildType.getGuildType(resultSet.getInt("type")),
					Guild.getMembersFromString(resultSet.getString("members")),
					resultSet.getInt("limit"),
					resultSet.getInt("xp"),
					resultSet.getBoolean("friendlyFire"));
			}
		} catch(final SQLException e) {
			e.printStackTrace();
			return null;
		}

		return guild;
	}

	public static Guild getGuildByTag(final String tag) {
		Guild guild = null;
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM `guild` WHERE `tag` = " + tag + " AND `dissolved` = 0");
			while(resultSet.next()) {
				guild = new Guild(
					resultSet.getInt("id"),
					resultSet.getString("name"),
					resultSet.getString("tag"),
					GuildType.getGuildType(resultSet.getInt("type")),
					Guild.getMembersFromString(resultSet.getString("members")),
					resultSet.getInt("limit"),
					resultSet.getInt("xp"),
						resultSet.getBoolean("friendlyFire"));
			}
		} catch(final SQLException e) {
			e.printStackTrace();
			return null;
		}

		return guild;
	}

	public static List<Guild> getGuilds(){

		final List<Guild> guilds = new ArrayList<>();
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM `guild` WHERE `dissolved` = 0");
			while(resultSet.next()) {
				guilds.add(new Guild(
						resultSet.getInt("id"),
						resultSet.getString("name"),
						resultSet.getString("tag"),
						GuildType.getGuildType(resultSet.getInt("type")),
						Guild.getMembersFromString(resultSet.getString("members")),
						resultSet.getInt("limit"),
						resultSet.getInt("xp"),
						resultSet.getBoolean("friendlyFire")
						));
			}
		} catch (final SQLException e) {
			e.printStackTrace();
			return null;
		}

		return guilds;
	}


	public static List<Guild> getAllGuilds(){

		final List<Guild> guilds = new ArrayList<>();
		try {
			final Statement state = DatabaseManager.getConnection().createStatement();
			final ResultSet resultSet = state.executeQuery("SELECT * FROM `guild`");
			while(resultSet.next()) {
				guilds.add(new Guild(
						resultSet.getInt("id"),
						resultSet.getString("name"),
						resultSet.getString("tag"),
						GuildType.getGuildType(resultSet.getInt("type")),
						Guild.getMembersFromString(resultSet.getString("members")),
						resultSet.getInt("limit"),
						resultSet.getInt("xp"),
						resultSet.getBoolean("friendlyFire")
						));
			}
		} catch (final SQLException e) {
			e.printStackTrace();
			return null;
		}

		return guilds;
	}

}

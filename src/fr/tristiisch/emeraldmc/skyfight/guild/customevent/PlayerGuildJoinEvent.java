package fr.tristiisch.emeraldmc.skyfight.guild.customevent;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import fr.tristiisch.emeraldmc.skyfight.guild.objects.Guild;

public class PlayerGuildJoinEvent extends PlayerEvent {

	public static final HandlerList handlers = new HandlerList();

	private final Guild guild;
	private final boolean created;

	public PlayerGuildJoinEvent(final Player who, final Guild guild) {
		super(who);
		this.guild = guild;
		this.created = false;
	}

	public PlayerGuildJoinEvent(final Player who, final Guild guild, final boolean created) {
		super(who);
		this.guild = guild;
		this.created = created;
	}

	public Guild getGuild() {
		return this.guild;
	}

	public boolean isCreated() {
		return this.created;
	}

	@Override
	public HandlerList getHandlers(){
		return handlers;
	}

	public static HandlerList getHandlerList(){
		return handlers;
	}

}

package fr.tristiisch.emeraldmc.skyfight.guild.customevent;

import org.bukkit.OfflinePlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import fr.tristiisch.emeraldmc.skyfight.guild.objects.Guild;

public class PlayerGuildQuitEvent extends Event {

	public static final HandlerList handlers = new HandlerList();

	private final OfflinePlayer offlinePlayer;
	private final Guild guild;

	public PlayerGuildQuitEvent(final OfflinePlayer offlinePlayer, final Guild guild) {
		this.offlinePlayer = offlinePlayer;
		this.guild = guild;
	}

	public Guild getGuild() {
		return this.guild;
	}

	@Override
	public HandlerList getHandlers(){
		return handlers;
	}

	public static HandlerList getHandlerList(){
		return handlers;
	}

	public OfflinePlayer getOfflinePlayer() {
		return this.offlinePlayer;
	}

}

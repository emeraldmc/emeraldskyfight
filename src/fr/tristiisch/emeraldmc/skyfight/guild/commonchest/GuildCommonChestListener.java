package fr.tristiisch.emeraldmc.skyfight.guild.commonchest;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools.GuiData;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayer;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayers;

public class GuildCommonChestListener implements Listener {

	/*	@EventHandler
		public void PlayerInteractEvent(final PlayerInteractEvent event) {
			if(event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock().getType() == Material.ENDER_CHEST) {
				event.setCancelled(true);
				final Player player = event.getPlayer();
				final SkyFightPlayer skyFightPlayer = SkyFightPlayers.getPlayer(player);
				if(skyFightPlayer == null || skyFightPlayer.getEnderChest() == null) {
					player.sendMessage(Utils.color("&2EmeraldMC &7» &cUne erreur est survenu lors du chargement de votre enderchest."));
					return;
				}

				skyFightPlayer.getEnderChest().openToPlayer(player);
			}
		}*/

	@EventHandler(priority = EventPriority.HIGH)
	public void InventoryCloseEvent(final InventoryCloseEvent event) {

		final Player player = (Player) event.getPlayer();
		final Inventory enderChest = event.getInventory();

		if(enderChest == null) {
			return;
		}

		final GuiData guiData = GuiTools.getGuiData(player);
		if(guiData != null) {
			switch(guiData.getId()) {
			case "GuildCommonEnderChest":
				final SkyFightPlayer skyFightPlayer = SkyFightPlayers.getPlayer(player);
				skyFightPlayer.getEnderChest().setItems(enderChest.getContents());
			}

		}
	}
}

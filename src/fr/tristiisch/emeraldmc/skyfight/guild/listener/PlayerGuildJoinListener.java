package fr.tristiisch.emeraldmc.skyfight.guild.listener;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import fr.tristiisch.emeraldmc.skyfight.guild.customevent.PlayerGuildJoinEvent;
import fr.tristiisch.emeraldmc.skyfight.guild.customevent.PlayerGuildQuitEvent;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.Guild;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.GuildMember;
import fr.tristiisch.emeraldmc.skyfight.scoreboard.Scoreboards;

public class PlayerGuildJoinListener implements Listener {

	@EventHandler
	public void PlayerGuildJoinEvent(final PlayerGuildJoinEvent event) {
		final Player player = event.getPlayer();
		final Guild guild = event.getGuild();

		if(!event.isCreated()) {
			guild.addMember(player.getUniqueId());

			guild.sendGuildMessage("Bienvenue a " + player.getName() + " dans la guilde " + guild.getName() + "!");
			guild.getMembersOnline().stream().map(me -> me.getUniqueID()).forEach(uuid -> Scoreboards.updateScoreboards(uuid));
		}
	}

	@EventHandler
	public void PlayerGuildQuitEvent(final PlayerGuildQuitEvent event) {
		final OfflinePlayer offlinePlayer = event.getOfflinePlayer();
		final Guild guild = event.getGuild();

		final List<GuildMember> oldMemberOnline = new ArrayList<>(guild.getMembersOnline());

		guild.removeMember(offlinePlayer.getUniqueId());
		oldMemberOnline.stream().map(me -> me.getUniqueID()).forEach(uuid -> Scoreboards.updateScoreboards(uuid));
	}
}

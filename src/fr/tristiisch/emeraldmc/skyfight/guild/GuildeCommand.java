package fr.tristiisch.emeraldmc.skyfight.guild;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.tristiisch.emeraldmc.api.commons.Matcher;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.commons.usernamehistory.Usernames;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;
import fr.tristiisch.emeraldmc.skyfight.guild.customevent.PlayerGuildJoinEvent;
import fr.tristiisch.emeraldmc.skyfight.guild.customevent.PlayerGuildQuitEvent;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.Guild;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.GuildRequest;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.GuildRole;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.GuildType;
import fr.tristiisch.emeraldmc.skyfight.guild.pnj.GuildPNJ;
import fr.tristiisch.emeraldmc.skyfight.listeners.AreaListeners;
import fr.tristiisch.emeraldmc.skyfight.scoreboard.Scoreboards;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;

public class GuildeCommand implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			Player player = null;
			if(sender instanceof Player) {
				player = (Player) sender;
			}

			if(args.length > 0) {
				if(args[0].equalsIgnoreCase("create")) {
					if(player == null) {
						sender.sendMessage(Utils.color("&cImpossibe de créé une guilde avec la console."));
						return;
					}

					Guild guild = GuildUtils.getGuild(player.getUniqueId());
					if(guild != null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous devez quitter votre guilde avant d'en créer une."));
						return;
					}

					if(args.length < 3) {
						player.sendMessage(Utils.color("&cUsage &7» &c/guilde create <Nom> <Tag>"));
						return;
					}

					final GuildType type = GuildCreate.getType(player);
					if(type == null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous devez parler au seigneur de la nation pour créer votre guilde."));
						return;
					}

					if(!AreaListeners.isInArea(player, type.getName())) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous devez être dans la salle de votre nation pour créer votre guilde."));
						return;
					}

					final String name = args[1];
					if(name.length() > 16) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe nom de guilde &4" + name + "&c est trop long (&4" + name.length() + "&c/16)."));
						return;
					}

					if(name.length() < 3) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe nom de guilde &4" + name + "&c est trop petit (&4" + name.length() + "&c/4)."));
						return;
					}

					final Pattern pattern = Pattern.compile("((?![a-zA-Z]).)+");
					java.util.regex.Matcher matcher = pattern.matcher(name);
					if(matcher.find()) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &Le nom de guilde ne doit contenir que des lettres."));
						return;
					}

					if(GuildUtils.getGuild(name) != null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cUne guilde avec le même nom existe déjà."));
						return;
					}

					final String tag = args[2].toUpperCase();
					if(tag.length() > 4) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe tag de guilde &4" + tag + "&c est trop long (&4" + tag.length() + "&c/4)."));
						return;
					}

					if(tag.length() == 1) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe tag de guilde &4" + tag + "&c est trop petit (&4" + tag.length() + "&c/2)."));
						return;
					}

					matcher = pattern.matcher(tag);
					if(matcher.find()) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &Le tag ne doit contenir que des lettres."));
						return;
					}

					if(GuildUtils.getGuildTag(tag) != null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cUne guilde avec le même tag existe déjà."));
						return;
					}
					final ItemStack emeraude = ItemTools.create(Material.EMERALD, 64, (short) 0, "&2Emeraude", Arrays.asList("", "&2➤ &aMonnaie du shop"));
					if(!player.getInventory().contains(emeraude)) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cPour crée une guilde, vous devez payer 64 emeraude."));
						return;
					}
					guild = GuildUtils.createNewGuild(name, tag, type, player.getUniqueId());
					if(guild == null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cUne erreur est survenu."));
						return;
					}

					player.getInventory().remove(emeraude);
					GuildCreate.remove(player);
					player.sendMessage(Utils.color("&2EmeraldMC &7» &7Votre guilde " + guild.getNameColored() + " &7a été créée."));
					final PlayerGuildJoinEvent event = new PlayerGuildJoinEvent(player, guild, true);
					Bukkit.getPluginManager().callEvent(event);

				} else if(args[0].equalsIgnoreCase("invite")) {
					if(player == null) {
						sender.sendMessage(Utils.color("&cImpossibe avec la console."));
						return;
					}

					if(args.length == 1) {
						player.sendMessage(Utils.color("&cUsage &7» &c/guilde invite <joueur>"));
						return;
					}

					final Guild guild = GuildUtils.getGuild(player.getUniqueId());
					if(guild == null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous n'avez pas de guilde."));
						return;
					}

					final GuildRole role = guild.getRole(player.getUniqueId());
					if(role == null || role.getPower() < GuildRole.GERANT.getPower()) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous n'avez pas la permission d'inviter un joueur dans votre guilde."));
						return;
					}

					final String name = args[1];
					if(!Matcher.isUsername(name)) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &4" + name + "&c n'est pas un pseudo valide."));
						return;
					}

					final Player target = Bukkit.getPlayer(name);
					if(target == null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &4" + name + "&c n'est pas connecté."));
						return;
					}

					final Guild targetGuild = GuildUtils.getGuild(target.getUniqueId());
					if(targetGuild != null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &4" + target.getName() + "&c est déjà dans une guilde."));
						return;
					}
					final GuildRequest alreadyReq = GuildRequest.getRequest(target.getUniqueId(), guild.getID());
					if(alreadyReq != null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &4" + target.getName() + "&c a déjà reçu une invitation dans votre guilde de la part de &4" + SpigotUtils
								.getName(alreadyReq.getAuthorUUID()) + "&c."));
						return;
					}

					GuildRequest.addRequest(target.getUniqueId(), player.getUniqueId(), guild.getID());
					// {"command":"/tellraw @p %s","jobject":[{"text":"&2EmeraldMC &7» &2%player%
					// &avous à rejoindre sa guilde
					// %guild%&a.","clickEvent":{"action":"run_command","value":"/g join
					// %guildName%"},"hoverEvent":{"action":"show_text","value":{"text":"","extra":[{"text":"&aClic
					// pour rejoindre la guilde %guild%"}]}}}],"jtemplate":"tellraw"}
					target.spigot().sendMessage(ComponentSerializer.parse(Utils
							.color("{\"text\":\"&2EmeraldMC &7» &2%player% &avous a invité rejoindre sa guilde %guild%&a.\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/g join %guildName%\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\"&aClic pour rejoindre la guilde %guild%\"}]}}}")
							.replaceAll("%player%", player.getName())
							.replaceAll("%guild%", guild.getGuildType().getColor() + guild.getName())
							.replaceAll("%guildName%", guild.getName())));
					guild.sendGuildMessage(target.getName() + " a reçu une invitation dans votre guilde de la part de " + player.getName());

				} else if(args[0].equalsIgnoreCase("join")) {
					if(player == null) {
						sender.sendMessage(Utils.color("&cImpossibe avec la console."));
						return;
					}

					if(args.length == 1) {
						player.sendMessage(Utils.color("&cUsage &7» &c/guilde join <guilde|tag>"));
						return;
					}

					final Guild guild = GuildUtils.getGuild(player.getUniqueId());
					if(guild != null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous avez déjà une guilde, &4/g leave&c pour la quitter."));
						return;
					}

					final List<GuildRequest> reqs = GuildRequest.getRequests(player.getUniqueId());
					Guild newGuild = null;
					GuildRequest newReq = null;
					for(final GuildRequest req : reqs) {
						final Guild tempGuild = GuildUtils.getGuild(req.getGuildID());
						if(tempGuild.getName().equalsIgnoreCase(args[1]) || tempGuild.getTag().equalsIgnoreCase(args[1])) {
							newGuild = tempGuild;
							newReq = req;
							break;
						}
					}
					if(newGuild == null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous devez être invité par un gérant ou créateur pour rejoindre une guilde."));
						return;
					}

					if(Utils.getCurrentTimeinSeconds() > newReq.getExpireTime()) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVotre invitation pour la guilde " + newGuild.getGuildType().getColor() + newGuild.getName() + "&c a expirée."));
						GuildRequest.removeRequest(newReq);
						return;
					}

					if(newGuild.getLimit() <= newGuild.getMembers().size()) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cLa guilde " + newGuild.getGuildType().getColor() + newGuild.getName() + "&c est au complet (" + newGuild.getMembers()
						.size() + "/" + newGuild.getLimit() + ")."));

						newGuild.sendGuildMessage(player.getName() + " essaye de renjoindre la guilde, mais elle est au complet (" + newGuild.getMembers().size() + "/" + newGuild.getLimit() + ").");
						return;
					}

					final PlayerGuildJoinEvent event = new PlayerGuildJoinEvent(player, newGuild);
					Bukkit.getPluginManager().callEvent(event);
					GuildRequest.removeRequest(newReq);

				} else if(args[0].equalsIgnoreCase("leave")) {
					if(player == null) {
						sender.sendMessage(Utils.color("&cImpossibe avec la console."));
						return;
					}

					final Guild guild = GuildUtils.getGuild(player.getUniqueId());
					if(guild == null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous n'avez pas de guilde."));
						return;
					}

					final GuildRole role = guild.getRole(player.getUniqueId());
					if(role == GuildRole.CREATEUR) {
						player.sendMessage(Utils
								.color("&2EmeraldMC &7» &cVous ne pouvez pas quitter votre guilde lorsque vous l'avez créé. Pour quitter votre guilde, vous devez la dissoudre avec &4/g disband&c."));
						return;
					}

					guild.sendGuildMessage(player.getName() + "&c a quitté votre guilde.");
					player.sendMessage(Utils.color("&2EmeraldMC &7» &aVous avez quitté la guilde " + guild.getGuildType().getColor() + guild.getName() + "&a."));

					final PlayerGuildQuitEvent event = new PlayerGuildQuitEvent(Bukkit.getOfflinePlayer(player.getUniqueId()), guild);
					Bukkit.getPluginManager().callEvent(event);

				} else if(args[0].equalsIgnoreCase("disband")) {
					if(player == null) {
						sender.sendMessage(Utils.color("&cImpossibe avec la console."));
						return;
					}

					final Guild guild = GuildUtils.getGuild(player.getUniqueId());
					if(guild == null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous n'avez pas de guilde."));
						return;
					}

					final GuildRole role = guild.getRole(player.getUniqueId());
					if(role != GuildRole.CREATEUR) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous devez avoir créé la guilde pour la dissoudre. Pour quitter la guide, faites &4/g leave&c."));
						return;
					}

					if(args.length <= 2 || !(args[1].equals(guild.getName()) && args[2].equals("confirm"))) {
						final TextComponent msg = new TextComponent(Utils.color("&2EmeraldMC &7» &4&l⚠&c Etes vous sûr de dissoudre votre guilde ?"));
						msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&4&l⚠&c Cliquez pour dissoudre votre guilde")).create()));
						msg.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/g disband " + guild.getName() + " confirm"));
						player.spigot().sendMessage(msg);
						return;
					}

					GuildUtils.removeGuild(guild);
					guild.sendGuildMessage(player.getName() + "&c a dissous votre guilde.");
					player.sendMessage(Utils.color("&2EmeraldMC &7» &aVous avez dissous " + guild.getGuildType().getColor() + guild.getName() + "&a."));
					final PlayerGuildQuitEvent event = new PlayerGuildQuitEvent(player, guild);
					Bukkit.getPluginManager().callEvent(event);

				} else if(args[0].equalsIgnoreCase("info")) {
					if(args.length > 1) {
						final String arg = args[1];
						Guild guild = GuildUtils.getGuild(arg);

						if(guild == null) {
							guild = GuildUtils.getGuildTag(arg);
						}

						if(guild == null) {
							final Player target = Bukkit.getPlayer(arg);
							if(target != null) {
								guild = GuildUtils.getGuild(target.getUniqueId());
							}
						}

						if(guild == null) {
							final EmeraldPlayer emeraldTarget = MySQL.getPlayer(arg);
							if(emeraldTarget != null) {
								guild = GuildUtils.getGuild(emeraldTarget.getUniqueId());

								if(guild == null) {
									player.sendMessage(Utils.color("&2EmeraldMC &7» &4" + arg + "&c n'a pas de guilde."));
									return;
								}
							}
						}

						if(guild == null) {
							player.sendMessage(Utils.color("&2EmeraldMC &7» &4" + arg + "&c ne correspond à aucun joueur ou nom/tag de guilde."));
							return;
						}

						player.spigot().sendMessage(GuildUtils.getGuildInfo(guild));

					} else {
						if(player == null) {
							sender.sendMessage(Utils.color("&cImpossibe avec la console."));
							return;
						}

						final Guild guild = GuildUtils.getGuild(player.getUniqueId());
						if(guild == null) {
							player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous n'avez pas de guilde."));
							return;
						}

						player.spigot().sendMessage(GuildUtils.getGuildInfo(guild));
					}

				} else if(args[0].equalsIgnoreCase("role")) {
					if(player == null) {
						sender.sendMessage(Utils.color("&cImpossibe avec la console."));
						return;
					}

					if(args.length == 1) {
						final TextComponent msg = new TextComponent("");
						msg.addExtra(Utils.color("&7&m=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="));
						msg.addExtra("\n");
						msg.addExtra(Utils.color("&2Créateur &7> &aIl a les pleins pouvoirs."));
						msg.addExtra("\n");
						msg.addExtra(Utils.color("&2Gérant &7>   &aIl peux inviter/supprimer des membres."));
						msg.addExtra("\n");
						msg.addExtra(Utils.color("&2Membre &7>   &aMembre simple."));
						msg.addExtra("\n");
						msg.addExtra(Utils.color("&2Nouveau &7>  &aRôle du bizu."));
						msg.addExtra("\n");
						msg.addExtra(Utils.color("&7&m=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="));
						player.spigot().sendMessage(msg);
					} else {
						final Guild guild = GuildUtils.getGuild(player.getUniqueId());
						if(guild == null) {
							player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous n'avez pas de guilde."));
							return;
						}
						final Player target = Bukkit.getPlayer(args[1]);
						EmeraldPlayer emeraldPlayer;
						if(target != null) {
							emeraldPlayer = new AccountProvider(target.getUniqueId()).getEmeraldPlayer();
							if(emeraldPlayer == null) {
								player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &4" + args[1] + "&c est introuvable."));
								return;
							}
						} else {
							emeraldPlayer = MySQL.getPlayer(args[1]);
							if(emeraldPlayer == null) {
								player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &4" + args[1] + "&c est introuvable."));
								return;
							}
						}

						if(emeraldPlayer.getUniqueId().equals(player.getUniqueId())) {
							player.sendMessage(Utils.color("&2EmeraldMC &7» &cIl est impossible de changer votre rôle."));
							return;
						}

						if(!guild.containsMember(emeraldPlayer.getUniqueId())) {
							player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &4" + emeraldPlayer.getName() + "&c n'est pas dans votre guilde."));
							return;
						}

						if(args.length > 2) {
							final GuildRole playerRole = guild.getRole(player.getUniqueId());
							if(playerRole != GuildRole.CREATEUR) {
								player.sendMessage(Utils.color("&2EmeraldMC &7» &cSeul le créateur peux changer les rôles."));
								return;
							}

							final GuildRole newrole = GuildRole.getRole(args[2]);
							if(newrole == null) {
								player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe rôle &4" + args[2] + "&c n'existe pas. Faites &4/g role&c pour voir les différents rôle."));
								return;
							}

							if(newrole.hasPower(GuildRole.CREATEUR)) {
								player.sendMessage(Utils.color("&2EmeraldMC &7» &cIl est impossible de changer le créateur."));
								return;
							}

							guild.changeRole(emeraldPlayer.getUniqueId(), newrole);
							guild.sendGuildMessage(emeraldPlayer.getName() + " a désormais le rôle " + newrole.getName() + ".");
						}
					}

				} else if(args[0].equalsIgnoreCase("kick")) {
					final Guild guild = GuildUtils.getGuild(player.getUniqueId());
					if(guild == null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous n'avez pas de guilde."));
						return;
					}

					if(args.length < 2) {
						player.sendMessage(Utils.color("&cUsage &7» &c/guilde kick <joueur>"));
						return;
					}

					final OfflinePlayer offlineTarget = Bukkit.getOfflinePlayer(args[1]);
					System.out.println("DEBUG 1: " + new Gson().toJson(offlineTarget));
					if(offlineTarget == null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &4" + args[1] + "&c est introuvable."));
						return;
					}

					final GuildRole playerRole = guild.getRole(player.getUniqueId());
					if(playerRole.hasPowerLessThan(GuildRole.GERANT)) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cSeul les gérents & créateurs peuvent explusés les joueurs."));
						return;
					}

					if(offlineTarget.getUniqueId().equals(player.getUniqueId())) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cIl est impossible de vous expluser de la guilde. Pour quitter votre guilde, faites &4/g leave&c."));
						return;
					}

					System.out.println("DEBUG UUID: " + offlineTarget.getUniqueId());
					System.out.println("DEBUG UUID 2: " + guild.getMembersToJSON());
					if(!guild.containsMember(offlineTarget.getUniqueId())) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &4" + offlineTarget.getName() + "&c n'est pas dans votre guilde."));
						return;
					}

					final GuildRole offlineTargetRole = guild.getRole(offlineTarget.getUniqueId());
					if(offlineTargetRole.getPower() >= playerRole.getPower()) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous pouvez expulser seulement vos subordonnés."));
						return;
					}
					guild.removeMember(offlineTarget.getUniqueId());
					guild.sendGuildMessage(offlineTarget.getName() + " a été expulsé par " + player.getName());

					final Player target = Bukkit.getPlayer(offlineTarget.getUniqueId());
					if(target != null) {
						target.sendMessage(Utils.color("&2EmeraldMC &7» &cVous avez été expulsé de la guilde " + guild.getNameColored() + "."));
						Scoreboards.updateScoreboardSign(target);
					}
					final PlayerGuildQuitEvent event = new PlayerGuildQuitEvent(offlineTarget, guild);
					Bukkit.getPluginManager().callEvent(event);

				} else if(args[0].equalsIgnoreCase("friendlyFire")) {

					final Guild guild = GuildUtils.getGuild(player.getUniqueId());
					if(guild == null) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous n'avez pas de guilde."));
						return;
					}

					final GuildRole role = guild.getRole(player.getUniqueId());
					if(role == null || role.getPower() < GuildRole.GERANT.getPower()) {
						player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous n'avez pas la permission de changer le FrendlyFire."));
						return;
					}

					if(guild.isFriendlyFire()) {
						guild.sendGuildMessage("&c" + player.getName() + " a désactivé le FrendlyFire.");
						guild.setFriendlyFire(false);
					} else {
						guild.sendGuildMessage("&a" + player.getName() + " a activé le FrendlyFire.");
						guild.setFriendlyFire(true);
					}
				} else if(args[0].equalsIgnoreCase("reload")) {
					if(player != null) {
						final EmeraldPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
						if(emeraldPlayer.hasPowerLessThan(EmeraldGroup.ADMIN)) {
							player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
							return;
						}
					}
					Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
						GuildPNJ.init();
						sender.sendMessage(Utils.color("&aLes PNJ ont été reload"));
					});
				} else if(args[0].equalsIgnoreCase("debug")) {
					if(player != null) {
						final EmeraldPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
						if(emeraldPlayer.hasPowerLessThan(EmeraldGroup.ADMIN)) {
							player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
							return;
						}
					}
					ChatColor color = null;
					final Gson gson = new GsonBuilder().setPrettyPrinting().create();
					int i = 0;
					for(final Guild guild : GuildUtils.getGuilds()) {
						if(color == ChatColor.YELLOW) {
							color = ChatColor.GOLD;
						} else {
							color = ChatColor.YELLOW;
						}
						sender.sendMessage(color + gson.toJson(guild));
						i++;
					}

					sender.sendMessage(Utils.color("&aIl y a &2" + i + " &aguildes."));

				} else if(args[0].equalsIgnoreCase("params")) {
					if(player != null) {
						final EmeraldPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
						if(emeraldPlayer.hasPowerLessThan(EmeraldGroup.RESPMODO)) {
							player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
							return;
						}
					}

					if(args.length > 3) {
						final String arg = args[1];
						final Guild guild = GuildUtils.getGuild(arg);

						if(guild == null) {
							player.sendMessage(Utils.color("&2EmeraldMC &7» &4" + arg + "&c ne correspond à aucun nom de guilde."));
							return;
						}

						if(args[2].equalsIgnoreCase("limit")) {
							guild.setLimit(Integer.parseInt(args[3]));
							player.sendMessage(Utils.color("&2EmeraldMC &7» &cLa guilde " + guild.getName() + " &ca désormais &4" + args[3] + "&c slots."));
						} else if(args[2].equalsIgnoreCase("name")) {
							if(GuildUtils.getGuild(args[3]) != null) {
								player.sendMessage(Utils.color("&2EmeraldMC &7» &cUne guilde avec le même nom existe déjà."));
								return;
							}
							guild.setName(args[3]);
							player.sendMessage(Utils.color("&2EmeraldMC &7» &cLa guilde " + guild.getName() + " &cs'appel désormais &4" + args[3] + "&c."));
						} else if(args[2].equalsIgnoreCase("tag")) {
							if(GuildUtils.getGuildTag(args[3]) != null) {
								player.sendMessage(Utils.color("&2EmeraldMC &7» &cUne guilde avec le même tag existe déjà."));
								return;
							}
							guild.setTag(args[3]);
							player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe tag de la guilde " + guild.getName() + " &cest désormais &4" + args[3] + "&c."));
						} else if(args[2].equalsIgnoreCase("friendlyFire")) {
							guild.setFriendlyFire(Boolean.getBoolean(args[3]));
							player.sendMessage(Utils.color("&2EmeraldMC &7» &cLa guilde " + guild.getName() + " &ca désormais le friendlyFire &4" + args[3] + "&c."));
						} else {

							sender.sendMessage(Utils.color("&cUsage &7» &cParamètres: limit, name, tag, friendlyFire"));
						}
					} else {

						sender.sendMessage(Utils.color("&cUsage &7» &c/guilde params <guilde> <params> <data>"));
					}

				} else if(args[0].equalsIgnoreCase("save")) {

					if(player != null) {
						final EmeraldPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
						if(emeraldPlayer.hasPowerLessThan(EmeraldGroup.ADMIN)) {
							player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
							return;
						}
					}
					final int[] result = GuildUtils.saveGuilds();
					sender.sendMessage(Utils.color("&2EmeraldMC &7» &a" + result[0] + "/" + result[1] + " guildes ont été sauvegarder."));

				} else if(args[0].equalsIgnoreCase("get")) {

					if(player != null) {
						final EmeraldPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
						if(emeraldPlayer.hasPowerLessThan(EmeraldGroup.ADMIN)) {
							player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
							return;
						}
					}

					if(GuildUtils.loadGuilds()) {
						sender.sendMessage(Utils.color("&2EmeraldMC &7» &aLes guildes ont bien été charger."));
					} else {
						sender.sendMessage(Utils.color("&2EmeraldMC &7» &cUne erreur avec la base de donné est survenu."));
					}
				} else {
					final TextComponent msg = new TextComponent("");
					msg.addExtra(Utils.color("&7&m=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="));
					msg.addExtra("\n");
					msg.addExtra(Utils.color("&2/guilde info <nom|tag|joueur>  &aInformation d'une guilde"));
					msg.addExtra("\n");
					msg.addExtra(Utils.color("&2/guilde create <nom> <tag>      &aCréée une guilde"));
					msg.addExtra("\n");
					msg.addExtra(Utils.color("&2/guilde invite <joueur>            &aInviter un joueur"));
					msg.addExtra("\n");
					msg.addExtra(Utils.color("&2/guilde leave                            &aQuitter votre guilde"));
					msg.addExtra("\n");
					msg.addExtra(Utils.color("&2/guilde role <joueur> <role>  &aChange le rôle d'un membre"));
					msg.addExtra("\n");
					msg.addExtra(Utils.color("&2/guilde friendlyfire                 &aAutorise les dégats entre membre"));
					msg.addExtra("\n");
					msg.addExtra(Utils.color("&2/guilde kick <joueur>              &aExpulse un membre"));
					msg.addExtra("\n");
					msg.addExtra(Utils.color("&2/guilde disband                        &aSupprime votre guilde"));
					msg.addExtra("\n");
					msg.addExtra(Utils.color("&7&m=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="));
					player.spigot().sendMessage(msg);
				}

			} else {
				sender.sendMessage(Utils.color("&cUsage &7» &c/guilde <help>"));
			}

		});
		return true;
	}
}

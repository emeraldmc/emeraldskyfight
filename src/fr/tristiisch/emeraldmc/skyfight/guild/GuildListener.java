package fr.tristiisch.emeraldmc.skyfight.guild;

import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.customevent.AsyncEmeraldPlayerLoadEvent;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.Guild;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.GuildRole;
import fr.tristiisch.emeraldmc.skyfight.scoreboard.Scoreboards;

public class GuildListener implements Listener {

	@EventHandler
	public void EntityDamageEvent(final EntityDamageByEntityEvent event) {
		if(event.isCancelled()) {
			return;
		}
		final Entity damager = event.getDamager();
		if(!(event.getEntity() instanceof Player)) {
			return;
		}
		final Player victim = (Player) event.getEntity();
		if(damager instanceof Player) {
			final Player attacker = (Player) event.getDamager();
			final Guild guildVictim = GuildUtils.getGuild(victim.getUniqueId());
			if(guildVictim != null) {

				final Guild guildAttacker = GuildUtils.getGuild(attacker.getUniqueId());
				if(guildAttacker != null) {
					if(guildAttacker.getID() == guildVictim.getID() && !guildVictim.isFriendlyFire()) {
						attacker.sendMessage(Utils.color("&2EmeraldMC &7» &cImpossible d'attaquer " + victim.getName() + " car le FriendlyFire de votre guilde est désactivé."));
						event.setCancelled(true);
					}
				}
			}
		} else if(damager instanceof Projectile) {
			final Projectile proj = (Projectile) damager;
			if(proj.getShooter() instanceof Player) {
				final Player attacker = (Player) proj.getShooter();
				final Guild guildVictim = GuildUtils.getGuild(victim.getUniqueId());
				if(guildVictim != null) {

					final Guild guildAttacker = GuildUtils.getGuild(attacker.getUniqueId());
					if(guildAttacker != null) {
						if(guildAttacker.getID() == guildVictim.getID() && !guildVictim.isFriendlyFire()) {
							attacker.sendMessage(Utils.color("&2EmeraldMC &7» &cImpossible d'attaquer " + victim.getName() + " car le FriendlyFire de votre guilde est désactivé."));
							event.setCancelled(true);
						}
					}
				}
			}
		}
	}

	@EventHandler
	private void AsyncPlayerChatEvent(final AsyncPlayerChatEvent event) {
		if(event.isCancelled()) {
			return;
		}
		final Player player = event.getPlayer();
		final String msg = event.getMessage();
		final AccountProvider account = new AccountProvider(player.getUniqueId());
		final EmeraldPlayer emeraldPlayer = account.getEmeraldPlayer();
		if(emeraldPlayer.hasPowerMoreThan(EmeraldGroup.RESPBUILD)) {
			event.setMessage(Utils.color(msg));
		}

		final Guild guild = GuildUtils.getGuild(player.getUniqueId());
		if(guild != null) {
			final GuildRole role = guild.getRole(player.getUniqueId());
			if(msg.startsWith("!")) {
				event.setCancelled(true);
				guild.sendGuildMessage(player, msg.substring(1));
			}
			event.setFormat(ChatColor.GRAY + role.getSymbol() + "[" + guild.getGuildType().getColor() + guild.getTag() + ChatColor.GRAY + "] " + emeraldPlayer.getGroup()
					.getPrefix() + "%s" + emeraldPlayer.getGroup().getTchatSuffix() + "%s");
		} else {

			event.setFormat(emeraldPlayer.getGroup().getPrefix() + "%s" + emeraldPlayer.getGroup().getTchatSuffix() + "%s");
		}
	}


	@EventHandler
	public void AsyncEmeraldPlayerLoad(final AsyncEmeraldPlayerLoadEvent event) {
		final Player player = event.getPlayer();
		final Guild guild = GuildUtils.getGuild(player.getUniqueId());
		if(guild != null) {
			guild.getMembersOnline().stream().map(me -> me.getUniqueID()).filter(uuid -> uuid != player.getUniqueId()).forEach(uuid -> Scoreboards.updateScoreboardSign(uuid));
		}
	}

	@EventHandler
	public void PlayerQuitEvent(final PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		final Guild guild = GuildUtils.getGuild(player.getUniqueId());
		if(guild != null) {
			guild.getMembersOnline().stream().map(me -> me.getUniqueID()).filter(uuid -> uuid != player.getUniqueId()).forEach(uuid -> Scoreboards.updateScoreboardSign(uuid));
		}
	}
}

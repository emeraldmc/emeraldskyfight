package fr.tristiisch.emeraldmc.skyfight.guild;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.tristiisch.emeraldmc.skyfight.guild.objects.GuildType;

public class GuildCreate implements Listener {

	private static Map<UUID, GuildType> players = new HashMap<>();

	public static boolean isIn(final Player player) {
		return players.containsKey(player.getUniqueId());
	}

	public static GuildType getType(final Player player) {
		return players.get(player.getUniqueId());
	}

	public static void add(final Player player, final GuildType type) {
		players.put(player.getUniqueId(), type);
	}

	public static void remove(final Player player) {
		players.remove(player.getUniqueId());
	}

	@EventHandler
	public void PlayerQuitEvent(final PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		if(isIn(player)) {
			players.remove(player.getUniqueId());
		}
	}
}

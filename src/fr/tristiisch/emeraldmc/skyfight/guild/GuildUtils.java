package fr.tristiisch.emeraldmc.skyfight.guild;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayer;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayers;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.Guild;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.GuildMember;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.GuildRole;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.GuildType;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class GuildUtils {

	private static List<Guild> guilds = new ArrayList<>();

	static {
		loadGuilds();
	}

	public static void removeGuild(final Guild guild) {
		GuildMySQL.dissolvedGuild(guild);
		guilds.remove(guild);
	}

	public static void addGuild(final Guild guild) {
		guilds.add(guild);
	}

	public static Guild getGuild(final int id) {
		return guilds.stream().filter(guild -> guild.getID() == id).findFirst().orElse(null);
	}

	public static Guild getGuild(final UUID member) {
		return guilds.stream().filter(guild -> guild.containsMember(member)).findFirst().orElse(null);
	}

	public static Guild getGuild(final String name) {
		return guilds.stream().filter(guild -> guild.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
	}

	public static Guild getGuildTag(final String tag) {
		return guilds.stream().filter(guild -> guild.getTag().equalsIgnoreCase(tag)).findFirst().orElse(null);
	}

	public static List<Guild> getGuilds() {
		return guilds;
	}

	public static boolean loadGuilds() {
		final List<Guild> guilds2 = GuildMySQL.getGuilds();
		if(guilds2 == null){
			return false;
		}
		guilds.clear();
		guilds.addAll(guilds2);
		return true;
	}

	public static int[] saveGuilds() {
		int succeed = 0;
		for(final Guild guild : guilds) {
			if(GuildMySQL.saveGuild(guild)) {
				succeed++;
			}
		}
		return new int[] { succeed, guilds.size() };
	}

	public static int getLimit(final EmeraldGroup group) {
		if(group.getPower() <= EmeraldGroup.JOUEUR.getPower()) {
			return 5;
		} else if(group.getPower() <= EmeraldGroup.RUBIS.getPower() || group.getPower() <= EmeraldGroup.SAPHIR.getPower()) {
			return 7;
		} else {
			return 9;
		}
	}

	public static int getNextID(){
		return MySQL.getnextID("guild", "id");
	}


	public static Guild createNewGuild(final String name, final String tag, final GuildType guildType, final UUID creatorUuid) {
		final EmeraldPlayer emeraldPlayer = new AccountProvider(creatorUuid).getEmeraldPlayer();
		final Guild guild = new Guild(getNextID(), name, tag, guildType, creatorUuid, getLimit(emeraldPlayer.getGroup()));
		final boolean result = GuildMySQL.createNewGuild(guild);
		if(result) {
			GuildUtils.addGuild(guild);
			return guild;
		}
		return null;
	}

	public static TextComponent getGuildInfo(final Guild guild){
		final HashMap<EmeraldPlayer, GuildRole> offline = new HashMap<>();
		final HashMap<EmeraldPlayer, GuildRole> online = new HashMap<>();
		for(final GuildMember member : guild.getMembers()) {

			final UUID uuid = member.getUniqueID();
			final GuildRole role = member.getGuildRole();
			final Player player = Bukkit.getPlayer(uuid);

			// connectét
			if(player != null) {
				online.put(new AccountProvider(uuid).getEmeraldPlayer(), role);
			} else {
				offline.put(MySQL.getPlayer(uuid), role);
			}
		}
		final TextComponent msg = new TextComponent("");
		final ChatColor guildColor = guild.getGuildType().getColor();
		msg.addExtra(Utils.color(guildColor + "&m=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="));msg.addExtra("\n");
		msg.addExtra(Utils.color("&2Nom: " + guildColor + guild.getName()));msg.addExtra("\n");
		msg.addExtra(Utils.color("&2Tag: " + guildColor + guild.getTag()));msg.addExtra("\n");
		msg.addExtra(Utils.color("&2Niveau: " + guildColor + guild.getLevel()));msg.addExtra("\n");
		msg.addExtra(Utils.color("&2Type: " + guildColor + guild.getGuildType().getName()));msg.addExtra("\n");
		msg.addExtra(Utils.color("&2Membre: " + guildColor + guild.getMembers().size() + "/" + guild.getLimit()));msg.addExtra("\n");
		msg.addExtra(Utils.color("&2Connectés: "));
		msg.addExtra(GuildUtils.getPlayersInfo(online, guildColor, ChatColor.GREEN));msg.addExtra("\n");
		msg.addExtra(Utils.color("&2Déconnectés: "));
		msg.addExtra(GuildUtils.getPlayersInfo(offline, guildColor, ChatColor.RED));msg.addExtra("\n");
		msg.addExtra(Utils.color(guildColor + "&m=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="));
		return msg;
	}

	public static TextComponent getPlayersInfo(final HashMap<EmeraldPlayer, GuildRole> map, final ChatColor guildColor, final ChatColor color){
		final TextComponent msg = new TextComponent("");
		int i = map.size();
		String separator = Utils.color("&7 , ");
		for(final Entry<EmeraldPlayer, GuildRole> entry : map.entrySet()) {
			if(i > 1) {
				i--;
			} else {
				separator = "";
			}
			msg.addExtra(GuildUtils.getPlayerInfo(entry.getKey(), entry.getValue(), guildColor, color));
			msg.addExtra(separator);
		}
		return msg;
	}

	public static TextComponent getPlayerInfo(final EmeraldPlayer emeraldPlayer, final GuildRole role, final ChatColor guildColor, final ChatColor color){
		final TextComponent msg = new TextComponent("");
		final SkyFightPlayer skyfightPlayer = SkyFightPlayers.getPlayerInfo(emeraldPlayer.getUniqueId());

		/*Player member = Bukkit.getPlayer(emeraldPlayer.getUniqueId());*/

		msg.addExtra(color + emeraldPlayer.getName());

		final BaseComponent[] tooltip = new ComponentBuilder("")
				.append(Utils.color("&2Role: " + guildColor + role.getName())).append("\n")
				.append(Utils.color("&2Grade: " + guildColor + emeraldPlayer.getGroup().getName())).append("\n")
				.append(Utils.color("&2Tué" + Utils.withOrWithoutS(skyfightPlayer.getKill()) + ": " + guildColor + skyfightPlayer.getKill()))
				.append("\n")
				.append(Utils.color("&2Mort" + Utils.withOrWithoutS(skyfightPlayer.getDeath()) + ": " + guildColor + skyfightPlayer.getDeath()))
				.create();
		msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, tooltip));
		return msg;
	}
}

package fr.tristiisch.emeraldmc.skyfight.guild.objects;

import java.util.UUID;

public class GuildMember {
	
	final private UUID uuid;
	private GuildRole guildRole;
	
	public GuildMember(UUID uuid, GuildRole guildRole) {
		this.uuid = uuid;
		this.guildRole = guildRole;
	}
	
	public UUID getUniqueID() {
		return uuid;
	}
	
	public GuildRole getGuildRole() {
		return guildRole;
	}
	
	public void changeGuildRole(GuildRole guildRole) {
		this.guildRole = guildRole;
	}
}

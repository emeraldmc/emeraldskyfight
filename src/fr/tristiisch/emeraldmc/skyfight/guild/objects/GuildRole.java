package fr.tristiisch.emeraldmc.skyfight.guild.objects;

import java.util.Arrays;

import fr.tristiisch.emeraldmc.api.commons.Utils;

public enum GuildRole {

	CREATEUR(4, "Créateur", 30, "**"),
	GERANT(3, "Gérant", 20, "*"),
	MEMBRE(2, "Membre", 10, ""),
	NOUVEAU(1, "Nouveau", 0, "");
	
	final private int id;
	final private String name;
	final private int power;
	final private String symbol;
	
	private GuildRole(int id, String name, int power, String symbol) {
		this.id = id;
		this.name = name;
		this.power = power;
		this.symbol = symbol;
	}
	
	public int getID(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getPower(){
		return this.power;
	}

	public String getSymbol() {
		return symbol;
	}

	public static GuildRole getRole(String string) {
		return Arrays.stream(GuildRole.values()).filter(role -> Utils.equalsIgnoreAccents(role.getName(), string)).findFirst().orElse(null);
	}

	public static GuildRole getRole(int i) {
		return Arrays.stream(GuildRole.values()).filter(role -> i == role.getID()).findFirst().orElse(null);
	}
	
	public boolean hasPowerMoreThan(GuildRole role) {
		return this.power >= role.getPower();
	}
	
	public boolean hasPowerLessThan(GuildRole role) {
		return this.power < role.getPower();
	}
	
	public boolean hasPower(GuildRole role) {
		return this.power == role.getPower();
	}

}

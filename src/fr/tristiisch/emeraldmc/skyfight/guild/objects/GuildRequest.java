package fr.tristiisch.emeraldmc.skyfight.guild.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;

public class GuildRequest {
	private static List<GuildRequest> requests = new ArrayList<>();
	
	final UUID target;
	final UUID author;
	final int guildId;
	final long time;
	
	private GuildRequest(UUID target, UUID author, int guildId, long time) {
		this.target = target;
		this.author = author;
		this.guildId = guildId;
		this.time = time;
	}
	
	public UUID getTargetUUID(){
		return this.target;
	}
	
	public Player getTarget(){
		return Bukkit.getPlayer(this.target);
	}
	
	public UUID getAuthorUUID(){
		return this.author;
	}
	
	public Player getAuthor(){
		return Bukkit.getPlayer(this.author);
	}
	
	public int getGuildID(){
		return this.guildId;
	}
	
	public long getTime(){
		return this.time;
	}
	
	public long getExpireTime(){
		return this.time + 60 * 5;
	}
	
	public static boolean hasRequest(UUID targetUuid, int guildId){
		return getRequest(targetUuid, guildId) != null;
	}
	
	public static GuildRequest getRequest(UUID targetUuid, int guildId){
		return requests.stream().filter(request -> request.getTargetUUID() == targetUuid && request.getGuildID() == guildId).findFirst().orElse(null);
	}

	public static List<GuildRequest> getRequests(UUID targetUuid){
		return requests.stream().filter(request -> request.getTargetUUID() == targetUuid).collect(Collectors.toList());
	}
	
	public static void addRequest(UUID targetUuid, UUID authorUuid, int guildId){
		if(!hasRequest(targetUuid, guildId)) {
			GuildRequest req = new GuildRequest(targetUuid, authorUuid, guildId, Utils.getCurrentTimeinSeconds());
			requests.add(req);
		}
	}
	
	public static void removeRequest(UUID targetUuid, int guildId){
		GuildRequest req = getRequest(targetUuid, guildId);
		if(req != null) {
			requests.remove(req);
		}
	}
	
	public static void removeRequest(GuildRequest req){
		requests.remove(req);
	}

	public static boolean canJoin(UUID targetUuid, int guildId){
		GuildRequest req = getRequest(targetUuid, guildId);
		if(req != null && req.getExpireTime() <= Utils.getCurrentTimeinSeconds()) {
			return true;
		} else {
			return false;
		}
		/*Arrays.stream(GuildType.values()).filter(guild -> guild.getName().equalsIgnoreCase(guildName)).findFirst().orElse(null);*/
	}
}

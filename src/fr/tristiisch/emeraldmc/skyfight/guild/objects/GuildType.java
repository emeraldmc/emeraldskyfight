package fr.tristiisch.emeraldmc.skyfight.guild.objects;

import java.util.Arrays;

import org.bukkit.ChatColor;

public enum GuildType {

	ANGE(1, "Ange", ChatColor.AQUA),
	DEMON(2, "Démon", ChatColor.GOLD);

	final private int id;
	final private String name;
	final private ChatColor color;

	private GuildType(final int id, final String name, final ChatColor color) {
		this.id = id;
		this.name = name;
		this.color = color;
	}

	public int getID(){
		return this.id;
	}

	public String getName(){
		return this.name;
	}

	public ChatColor getColor() {
		return this.color;
	}

	public static GuildType getGuildType(final int id){
		return Arrays.stream(GuildType.values()).filter(guild -> guild.getID() == id).findFirst().orElse(null);
	}

	public static GuildType getGuildType(final String name) {
		return Arrays.stream(GuildType.values()).filter(guild -> guild.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
	}

	public String getNameColored() {
		return this.color + this.name;
	}
}

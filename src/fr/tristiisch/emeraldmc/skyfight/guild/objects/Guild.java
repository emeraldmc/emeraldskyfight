package fr.tristiisch.emeraldmc.skyfight.guild.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.google.gson.Gson;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.skyfight.guild.GuildMySQL;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class Guild {

	final private int id;
	private String name;
	private String tag;
	final private GuildType guildType;
	private List<GuildMember> members = new ArrayList<>();
	private int limit;
	private float xp;
	private boolean friendlyFire;
	/*private List<GuildCommonChest> chests;*/

	// load
	public Guild(final int id, final String name, final String tag, final GuildType guildType, final List<GuildMember> members, final int limit, final int xp, final boolean friendlyFire) {
		this.id = id;
		this.name = name;
		this.tag = tag;
		this.guildType = guildType;
		this.members = members;
		this.limit = limit;
		this.xp = xp;
		this.friendlyFire = friendlyFire;
		this.sortMembers();
	}

	// create
	public Guild(final int id, final String name, final String tag, final GuildType guildType, final UUID creator, final int limit) {
		this.id = id;
		this.name = name;
		this.tag = tag;
		this.guildType = guildType;
		this.members.add(new GuildMember(creator, GuildRole.CREATEUR));
		this.limit = limit;
		this.friendlyFire = false;
		this.xp = 0;
	}

	public int getID() {
		return this.id;
	}

	public String getNameColored() {
		return this.getGuildType().getColor() + this.name;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getTag() {
		return this.tag;
	}

	public void setTag(final String tag) {
		this.tag = tag;
	}

	public GuildType getGuildType() {
		return this.guildType;
	}

	/**
	 * return false if the player is already in the SkyFightGuild
	 */
	public boolean addMember(final UUID target) {
		if(this.containsMember(target)) {
			return false;
		}
		this.members.add(new GuildMember(target, GuildRole.NOUVEAU));
		this.sortMembers();
		GuildMySQL.saveGuild(this);
		return true;
	}

	public boolean removeMember(final UUID uuid) {
		this.members.remove(this.getMember(uuid));
		GuildMySQL.saveGuild(this);
		return true;
	}

	public GuildMember getMember(final UUID uuid) {
		return this.members.stream().filter(member -> member.getUniqueID().equals(uuid)).findFirst().orElse(null);
	}

	public GuildRole getRole(final UUID uuid) {
		return this.members.stream().filter(member -> member.getUniqueID().equals(uuid)).map(member -> member.getGuildRole()).findFirst().orElse(null);
	}

	public boolean containsMember(final UUID uuid) {
		return this.members.stream().filter(member -> member.getUniqueID().equals(uuid)).findFirst().isPresent();
	}

	public void changeRole(final UUID uuid, final GuildRole newrole) {
		this.getMember(uuid).changeGuildRole(newrole);
	}

	public void sortMembers() {
		this.members.stream().sorted((o1, o2) -> o1.getGuildRole().compareTo(o2.getGuildRole()));
	}

	public List<GuildMember> getMembers() {
		return this.members;
	}

	public List<Player> getPlayersOnline() {
		return this.members.stream().map(member -> Bukkit.getPlayer(member.getUniqueID())).filter(player -> player != null).collect(Collectors.toList());
	}

	public List<GuildMember> getMembersOnline() {
		return this.members.stream().filter(member -> Bukkit.getPlayer(member.getUniqueID()) != null).collect(Collectors.toList());
	}

	public List<GuildMember> getMembersOffline() {
		return this.members.stream().filter(member -> Bukkit.getPlayer(member.getUniqueID()) == null).collect(Collectors.toList());
	}

	/*
	 * public static HashMap<UUID, GuildRole> getMembersFromJSON(String gson) {
	 * return new Gson().fromJson(gson, new TypeToken<HashMap<UUID,
	 * GuildRole>>(){}.getType()); }
	 */

	public String getMembersToJSON() {
		return new Gson().toJson(this.members);
	}

	public String getMembersToString() {
		return this.members.stream().map(member -> member.getUniqueID() + ":" + member.getGuildRole().getID()).collect(Collectors.joining(" "));
	}

	public static List<GuildMember> getMembersFromString(final String s) {
		final List<GuildMember> members = new ArrayList<>();
		for(final String member : s.split(" ")) {
			final String[] memberRole = member.split(":");
			final UUID uuid = UUID.fromString(memberRole[0]);
			final GuildRole role = GuildRole.getRole(Integer.parseInt(memberRole[1]));
			members.add(new GuildMember(uuid, role));
		}
		return members;
	}

	public UUID getCreator() {
		return this.members.stream().filter(member -> member.getGuildRole().getID() == GuildRole.GERANT.getID()).findFirst().map(member -> member.getUniqueID()).orElse(null);
	}

	public int getLimit() {
		return this.limit;
	}

	public void setLimit(final int limit) {
		this.limit = limit;
		GuildMySQL.saveGuild(this);
	}

	public int getLevel() {
		int level = 1;
		float xp2 = this.xp;
		int range = 20;

		while(xp2 > range) {
			level++;
			xp2 -= range;
			range += 10;
		}
		return level;
	}


	public void addXP(final int xp) {
		this.xp += xp;
	}

	public float getXP() {
		return this.xp;
	}

	public void sendGuildMessage(final String msg) {
		final TextComponent msg2 = new TextComponent(Utils.colorFix(this.getNameColored() + "&7 ➤ &b" + msg));
		for(final Player member : this.getPlayersOnline()) {
			member.spigot().sendMessage(msg2);
		}
	}

	public void sendGuildMessage(final Player player, final String msg) {
		final GuildRole role = this.getRole(player.getUniqueId());
		final TextComponent msg2 = new TextComponent(Utils.colorFix(this.getNameColored() + " [" + role.getName() + "] " + player.getName() + "&7 ➤" + " &e" + msg));
		msg2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&2Cliquez pour répondre à votre guilde")).create()));
		msg2.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "!"));
		for(final Player member : this.getPlayersOnline()) {
			member.spigot().sendMessage(msg2);
		}
	}

	public boolean isFriendlyFire() {
		return this.friendlyFire;
	}

	public void setFriendlyFire(final boolean friendlyFire) {
		this.friendlyFire = friendlyFire;
	}

	@Override
	public Guild clone() {
		try {
			return (Guild) super.clone();
		} catch(final CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
}

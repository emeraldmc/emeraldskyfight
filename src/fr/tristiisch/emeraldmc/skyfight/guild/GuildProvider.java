package fr.tristiisch.emeraldmc.skyfight.guild;

import java.util.UUID;

import com.google.gson.Gson;

import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.RedisAccess;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.Guild;
import redis.clients.jedis.Jedis;

public class GuildProvider {
	public static String REDIS_KEY = "guild:";

	private final RedisAccess redisAccesss;
	private int id;
	@SuppressWarnings("unused")
	private UUID member;

	public GuildProvider(final int id) {
		this.id = id;
		this.redisAccesss = RedisAccess.INSTANCE;
	}

	public GuildProvider(final UUID member) {
		this.member = member;
		this.redisAccesss = RedisAccess.INSTANCE;
	}

	public String getKey() {
		return REDIS_KEY + this.id;
	}

	public Guild get() {
		Guild guild = this.getFromRedis();
		if(guild == null) {
			guild = this.getFromDatbase();
			if(guild != null) {
				this.sendToRedis(guild);
			}
		}
		return guild;
	}

	public Guild getFromDatbase() {
		return GuildMySQL.getGuild(this.id);
	}

	public void sendToRedis(final Guild guild) {
		try(Jedis jedis = this.redisAccesss.connect()) {
			jedis.set(this.getKey(), new Gson().toJson(guild));
		}
		this.redisAccesss.closeResource();
	}

	public void removeFromRedis() {
		try(Jedis jedis = this.redisAccesss.connect()) {
			jedis.del(this.getKey());
		}
		this.redisAccesss.closeResource();
	}

	public Guild getFromRedis() {
		String json = null;
		try(Jedis jedis = this.redisAccesss.connect()) {
			json = jedis.get(this.getKey());
		}
		this.redisAccesss.closeResource();

		if(json == null) {
			return null;
		}
		return new Gson().fromJson(json, Guild.class);
	}
}

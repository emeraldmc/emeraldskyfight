package fr.tristiisch.emeraldmc.skyfight.guild.pnj;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.skyfight.guild.GuildUtils;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.Guild;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.GuildType;

public class GuildPNJGui {

	public static ItemStack info = ItemTools
			.create(Material.PAPER, 1, (short) 0, "&7Info", Arrays.asList("", "&b➤ &3Lorsque vous créez votre guilde", "   &3vous avez le choix entre 2 nations:", "", " &b➡ &7Ange", " &b➡ &7Démon"));
	public static ItemStack create = ItemTools.create(Material.STAINED_GLASS_PANE, 1, (short) 5, "&aCréer une guilde", Arrays.asList("", "&b➤ &3Cela coûte 64 emeraude."));
	public static ItemStack already = ItemTools.create(Material.STAINED_GLASS_PANE, 1, (short) 14, "&cCréer une guilde", Arrays.asList("", "&b➤ &3Cela coûte 64 emeraude."));
	public static ItemStack history = ItemTools.create(Material.BOOK, 1, (short) 0, "&7Histoire", Arrays.asList("", "&b➤ &3En développement"));

	public static void openMenu(final Player player, final GuildType type) {
		final GuiTools inventory = new GuiTools("&7Créer sa guilde " + type.getNameColored(), "create_guild", 1 * 9, String.valueOf(type.getID()));

		final Guild guild = GuildUtils.getGuild(player.getUniqueId());
		inventory.setItem(inventory.getFirstSlot(), info);
		if(guild == null) {
			inventory.setItem(inventory.getMiddleSlot(), create);
		} else {
			inventory.setItem(inventory.getMiddleSlot(), already);
		}
		inventory.setItem(inventory.getLastSlot(), history);
		inventory.openInventory(player);
	}
}

package fr.tristiisch.emeraldmc.skyfight.guild.pnj;

import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools.GuiData;
import fr.tristiisch.emeraldmc.skyfight.guild.GuildCreate;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.GuildType;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class GuildPNJListener implements Listener {

	@EventHandler
	public void PlayerInteractEntityEvent(final PlayerInteractEntityEvent event) {
		if(!(event.getRightClicked() instanceof Villager)) {
			return;
		}
		final Villager villager = (Villager) event.getRightClicked();
		final Player player = event.getPlayer();
		if(GuildPNJ.isPNJ(villager)) {
			final GuildType type = GuildType.getGuildType(ChatColor.stripColor(villager.getCustomName()));
			if(type == null) {
				return;
			}
			GuildPNJGui.openMenu(player, type);
		}
	}

	@EventHandler
	public void EntityDamageEvent(final EntityDamageEvent event) {
		if(event.getEntityType() == EntityType.VILLAGER) {
			if(GuildPNJ.isPNJ((Villager) event.getEntity())) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	private void InventoryClickEvent(final InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();
		final Inventory clickedInventory = event.getClickedInventory();

		if(clickedInventory == null) {
			return;
		}

		final GuiData guiData = GuiTools.getGuiData(player);
		if(guiData != null) {
			switch(guiData.getId()) {
			case "create_guild":
				event.setCancelled(true);

				if(event.getCurrentItem().isSimilar(GuildPNJGui.already)) {
					GuiTools.cancelItem(event, "&cVous avez déjà une guilde");
				} else if(event.getCurrentItem().isSimilar(GuildPNJGui.create)) {
					final GuildType type = GuildType.getGuildType(Integer.parseInt(guiData.getData()));
					GuildCreate.add(player, type);
					final TextComponent msg = new TextComponent(Utils.colorFix("&2EmeraldMC &7» " + type.getColor() + "Pour créer votre guilde, faites "));

					final HoverEvent hover = new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("&aCliquez pour crée votre guilde").create());
					final ClickEvent click = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/guilde create");

					for(final BaseComponent b : new ComponentBuilder(Utils.colorFix("&7/guilde create <nom> <tag>")).event(hover).event(click).create()) {
						msg.addExtra(b);
					}

					msg.addExtra(Utils.colorFix(type.getColor() + " ."));
					player.spigot().sendMessage(msg);
				}
				break;
			}
		}
	}
}

package fr.tristiisch.emeraldmc.skyfight.guild.pnj;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftVillager;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;

import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;
import fr.tristiisch.emeraldmc.api.spigot.utils.LocationUtils;
import fr.tristiisch.emeraldmc.skyfight.Main;
import fr.tristiisch.emeraldmc.skyfight.guild.objects.GuildType;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.EntityVillager;
import net.minecraft.server.v1_8_R3.NBTTagCompound;

public class GuildPNJ {

	public static List<Villager> pnj = new ArrayList<>();

	public static void init() {
		if(!pnj.isEmpty()) {
			pnj.stream().forEach(vil -> vil.remove());
			pnj.clear();
		}

		final FileConfiguration config = Main.getInstance().getConfig();
		final ConfigurationSection section = config.getConfigurationSection("pnj");
		for(final String key : section.getKeys(false)) {

			final String typeName = section.getString(key + ".type");
			final GuildType type = GuildType.getGuildType(typeName);
			final Location loc = LocationUtils.convertStringToLocation(section.getString(key + ".location"));

			if(!loc.getChunk().isLoaded()) {
				loc.getChunk().load();
			}

			final String taskName = TaskManager.getTaskName("GuildPnjSpawn");
			TaskManager.scheduleSyncRepeatingTask(taskName, () -> {

				final Villager oldPnj = pnj.stream().filter(vill -> vill.getName().equalsIgnoreCase(type.getNameColored())).findFirst().orElse(null);
				if(oldPnj == null || !oldPnj.isValid()) {

					final Villager villager = (Villager) loc.getWorld().spawnEntity(loc, EntityType.VILLAGER);
					villager.setCustomName(type.getNameColored());

					if(villager.isValid()) {
						villager.getNearbyEntities(20, 5, 20)
						.stream()
						.filter(e -> e.getType() == EntityType.VILLAGER && (e.getCustomName() == null || e.getCustomName().equalsIgnoreCase(type.getNameColored())))
						.forEach(
								e -> e.remove());
						villager.setProfession(Profession.PRIEST);
						villager.setAdult();
						villager.setCustomNameVisible(true);
						setNoAI(villager);
						pnj.add(villager);
					}
				}
			}, 0, 60 * 20);

		}
	}

	private static void setNoAI(final Villager villager) {
		final EntityVillager nmsVillager = ((CraftVillager) villager).getHandle();
		NBTTagCompound tag = ((Entity) nmsVillager).getNBTTag();
		if(tag == null) {
			tag = new NBTTagCompound();
		}
		nmsVillager.c(tag);
		tag.setInt("NoAI", 1);
		tag.setInt("Silent", 1);
		nmsVillager.f(tag);
	}

	public static boolean isPNJ(final Villager villager) {
		return pnj.stream().filter(pnj -> pnj.getUniqueId() == villager.getUniqueId()).findFirst().isPresent();
	}
}

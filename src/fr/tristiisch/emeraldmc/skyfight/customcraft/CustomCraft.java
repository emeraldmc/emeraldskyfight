package fr.tristiisch.emeraldmc.skyfight.customcraft;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.plugin.Plugin;

import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;

public class CustomCraft {

	public static final ItemStack emerald = ItemTools.create(Material.EMERALD, 1, "&2Emeraude", Arrays.asList("", "&2➤ &aMonnaie du shop"));
	public static final ItemStack emeraldBlock = ItemTools.create(Material.EMERALD_BLOCK, 1, "&2Bloc d'Emeraude", Arrays.asList("", "&2➤ &aMonnaie du shop"));

	public static ItemStack getEmerald() {
		return getEmerald(new Random().nextInt(3) + 2);
	}

	public static ItemStack getEmerald(final int amount) {
		final ItemStack item = emerald.clone();
		item.setAmount(amount);
		return item;
	}

	public static ItemStack getEmeraldBlock(final int amount) {
		final ItemStack item = emeraldBlock.clone();
		item.setAmount(amount);
		return item;
	}

	public static void init(final Plugin plugin) {
		ShapedRecipe shapedRecipe;
		final Server server = plugin.getServer();

		final Iterator<Recipe> iter = server.recipeIterator();
		while(iter.hasNext()) {
			final Recipe r = iter.next();
			final ItemStack item = r.getResult();
			if(item.isSimilar(new ItemStack(emeraldBlock.getType())) || item.isSimilar(new ItemStack(emerald.getType()))) {
				iter.remove();
			}
		}

		shapedRecipe = new ShapedRecipe(emerald);
		shapedRecipe.shape("a");
		shapedRecipe.setIngredient('a', Material.EMERALD);
		plugin.getServer().addRecipe(shapedRecipe);

		final ItemStack emerald2 = emerald.clone();
		emerald2.setAmount(9);
		shapedRecipe = new ShapedRecipe(emerald2);
		shapedRecipe.shape("a");
		shapedRecipe.setIngredient('a', Material.EMERALD_BLOCK);
		server.addRecipe(shapedRecipe);

		shapedRecipe = new ShapedRecipe(emeraldBlock);
		shapedRecipe.shape("aaa", "aaa", "aaa");
		shapedRecipe.setIngredient('a', Material.EMERALD);
		server.addRecipe(shapedRecipe);
	}
}

package fr.tristiisch.emeraldmc.skyfight.group;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemEnchant;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.skyfight.kit.SkyFightKit;

public enum SkyFightGroup {

	EMERAUDE("Emeraude", EmeraldGroup.EMERAUDE, 7, new SkyFightKit(
			86400,
			ItemTools.create(Material.DIAMOND_SWORD, new ItemEnchant(Enchantment.DAMAGE_ALL, 5), new ItemEnchant(Enchantment.LOOT_BONUS_MOBS, 2)),
			ItemTools.create(Material.GOLD_SPADE, new ItemEnchant(Enchantment.KNOCKBACK), new ItemEnchant(Enchantment.DURABILITY, 3)),
			ItemTools.create(Material.DIAMOND_HELMET, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4)),
			ItemTools.create(Material.DIAMOND_CHESTPLATE, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4), new ItemEnchant(Enchantment.THORNS, 1)),
			ItemTools.create(Material.DIAMOND_LEGGINGS, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4)),
			ItemTools.create(Material.DIAMOND_BOOTS, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4)))),
	DIAMANT("Diamant", EmeraldGroup.DIAMANT, 6,
			new SkyFightKit(
					43200,
					ItemTools.create(Material.DIAMOND_SWORD, new ItemEnchant(Enchantment.DAMAGE_ALL, 4)),
					ItemTools.create(Material.GOLD_SPADE, new ItemEnchant(Enchantment.KNOCKBACK), new ItemEnchant(Enchantment.DURABILITY, 2)),
					ItemTools.create(Material.DIAMOND_HELMET, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3)),
					ItemTools.create(Material.DIAMOND_CHESTPLATE, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3)),
					ItemTools.create(Material.DIAMOND_LEGGINGS, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3)),
					ItemTools.create(Material.DIAMOND_BOOTS, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3))
					)),
	SAPHIR("Saphir", EmeraldGroup.SAPHIR, 5, new SkyFightKit(
			21600,
			ItemTools.create(Material.DIAMOND_SWORD, new ItemEnchant(Enchantment.DAMAGE_ALL, 2)),
			ItemTools.create(Material.DIAMOND_HELMET, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2)),
			ItemTools.create(Material.DIAMOND_CHESTPLATE, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2)),
			ItemTools.create(Material.DIAMOND_LEGGINGS, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2)),
			ItemTools.create(Material.DIAMOND_BOOTS, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2))
			)),
	RUBIS("Rubis", EmeraldGroup.RUBIS, 4, new SkyFightKit(
			7200,
			ItemTools.create(Material.DIAMOND_SWORD, new ItemEnchant(Enchantment.DAMAGE_ALL, 1)),
			ItemTools.create(Material.DIAMOND_HELMET, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
			ItemTools.create(Material.DIAMOND_CHESTPLATE, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
			ItemTools.create(Material.DIAMOND_LEGGINGS, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
			ItemTools.create(Material.DIAMOND_BOOTS, new ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1))
			));

	final private String name;
	final private EmeraldGroup emeraldGroup;
	final private int enderSize;
	final private SkyFightKit skyfightKit;

	/*
	 * SkyFightGroup(final SkyFightKit skyfightKit, final int minEmeraldLoot, final
	 * int maxEmeraldLoot) { this.skyfightKit = skyfightKit; }
	 */
	private SkyFightGroup(final String name, final EmeraldGroup emeraldGroup, final int enderSize, final SkyFightKit skyfightKit) {
		this.name = name;
		this.emeraldGroup = emeraldGroup;
		this.enderSize = enderSize;
		this.skyfightKit = skyfightKit;
	}

	public String getName() {
		return this.name;
	}

	public SkyFightKit getSkyfightKit() {
		return this.skyfightKit;
	}

	public EmeraldGroup getEmeraldGroup() {
		return this.emeraldGroup;
	}

	public int getEnderSize() {
		return this.enderSize;
	}

	public static SkyFightGroup get(final EmeraldGroup group) {
		return Arrays.stream(SkyFightGroup.values()).filter(v -> v.getEmeraldGroup().getPower() <= group.getPower()).findFirst().orElse(null);
	}

	public static SkyFightGroup get(final SkyFightKit skyfightKit) {
		return Arrays.stream(SkyFightGroup.values()).filter(v -> v.getSkyfightKit().equals(skyfightKit)).findFirst().orElse(null);
	}
}

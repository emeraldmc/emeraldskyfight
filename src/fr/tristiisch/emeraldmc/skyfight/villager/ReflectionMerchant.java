package fr.tristiisch.emeraldmc.skyfight.villager;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.ItemStack;

public class ReflectionMerchant {

	@SuppressWarnings("deprecation")
	public static Object createNMSGameMode(final GameMode gameMode) {
		Class<?> c = getClassByName(getNMSPackageName() + ".EnumGamemode");
		if(c == null) {
			c = getClassByName(getNMSPackageName() + ".WorldSettings$EnumGamemode");
		}

		try {
			final Method method = c.getDeclaredMethod("getById", new Class[] { Integer.TYPE });
			method.setAccessible(true);
			return method.invoke((Object) null, new Object[] { Integer.valueOf(gameMode.getValue()) });
		} catch(final Exception var3) {
			var3.printStackTrace();
			return null;
		}
	}

	public static Object createPlayerInfoData(final Object gameProfile, final GameMode gameMode, final int ping, final String nickName) {
		Class<?> playerInfoDataClass = getClassByName(getNMSPackageName() + ".PacketPlayOutPlayerInfo$PlayerInfoData");
		if(playerInfoDataClass == null) {
			playerInfoDataClass = getClassByName(getNMSPackageName() + ".PlayerInfoData");
		}

		final Object nmsGameMode = createNMSGameMode(gameMode);

		try {
			final Constructor<?> constructor = playerInfoDataClass
					.getDeclaredConstructor(new Class[] { getClassByName(getNMSPackageName() + ".PacketPlayOutPlayerInfo"), getClassByName("com.mojang.authlib.GameProfile"), Integer.TYPE, nmsGameMode
							.getClass(), getClassByName(getNMSPackageName() + ".IChatBaseComponent") });
			constructor.setAccessible(true);
			return constructor.newInstance(new Object[] { null, gameProfile, Integer.valueOf(ping), nmsGameMode, createNMSTextComponent(nickName) });
		} catch(final Exception var8) {
			var8.printStackTrace();
			return null;
		}
	}

	public static Object fillProfileProperties(final Object gameProfile) {
		final Class<?> serverClass = getClassByName(getNMSPackageName() + ".MinecraftServer");
		final Class<?> sessionServiceClass = getClassByName("com.mojang.authlib.minecraft.MinecraftSessionService");

		try {
			final Method method = serverClass.getDeclaredMethod("getServer", new Class[0]);
			method.setAccessible(true);
			final Object minecraftServer = method.invoke((Object) null, new Object[0]);
			String methodName;
			if(existsMethod(serverClass, "aC", sessionServiceClass)) {
				methodName = "aC";
			} else {
				methodName = "aD";
			}

			Method method1 = serverClass.getDeclaredMethod(methodName, new Class[0]);
			method1.setAccessible(true);
			final Object sessionService = method1.invoke(minecraftServer, new Object[0]);
			method1 = sessionServiceClass.getDeclaredMethod("fillProfileProperties", new Class[] { gameProfile.getClass(), Boolean.TYPE });
			method1.setAccessible(true);
			final Object result = method1.invoke(sessionService, new Object[] { gameProfile, Boolean.valueOf(true) });
			return result;
		} catch(final Exception var7) {
			var7.printStackTrace();
			return null;
		}
	}

	private static boolean existsMethod(final Class<?> clazz, final String methodName, final Class<?> returnClass) {
		final Method[] var3 = clazz.getDeclaredMethods();
		final int var4 = var3.length;

		for(int var5 = 0; var5 < var4; ++var5) {
			final Method method = var3[var5];
			if(method.getName().equals(methodName) && method.getGenericReturnType() == returnClass) {
				return true;
			}
		}

		return false;
	}

	public static Object searchUUID(final String playerName) {
		final Class<?> serverClass = getClassByName(getNMSPackageName() + ".MinecraftServer");
		final Class<?> userCacheClass = getClassByName(getNMSPackageName() + ".UserCache");

		try {
			final Method method = serverClass.getDeclaredMethod("getServer", new Class[0]);
			method.setAccessible(true);
			final Object minecraftServer = method.invoke((Object) null, new Object[0]);
			Method method1 = serverClass.getDeclaredMethod("getUserCache", new Class[0]);
			method1.setAccessible(true);
			final Object userCache = method1.invoke(minecraftServer, new Object[0]);
			method1 = userCacheClass.getDeclaredMethod("getProfile", new Class[] { String.class });
			method1.setAccessible(true);
			return method1.invoke(userCache, new Object[] { playerName });
		} catch(final Exception var6) {
			var6.printStackTrace();
			return null;
		}
	}

	public static Object createNMSTextComponent(final String text) {
		if(text != null && !text.isEmpty()) {
			final Class<?> c = getClassByName(getNMSPackageName() + ".ChatComponentText");

			try {
				final Constructor<?> constructor = c.getDeclaredConstructor(new Class[] { String.class });
				return constructor.newInstance(new Object[] { text });
			} catch(final Exception var3) {
				var3.printStackTrace();
				return null;
			}
		} else {
			return null;
		}
	}

	public static Object toEntityHuman(final Player player) {
		try {
			final Class<?> c = getClassByName(getOBCPackageName() + ".entity.CraftPlayer");
			final Method m = c.getDeclaredMethod("getHandle", new Class[0]);
			m.setAccessible(true);
			return m.invoke(player, new Object[0]);
		} catch(final Exception var3) {
			var3.printStackTrace();
			return null;
		}
	}

	public static Class<?> getClassByName(final String name) {
		try {
			return Class.forName(name);
		} catch(final Exception var2) {
			return null;
		}
	}

	public static Object getField(final Class<?> c, final Object obj, final String key) throws Exception {
		final Field field = c.getDeclaredField(key);
		field.setAccessible(true);
		return field.get(obj);
	}

	public static void replaceField(final Class<?> c, final Object obj, final String key, final Object value) throws Exception {
		final Field field = c.getDeclaredField(key);
		field.setAccessible(true);
		field.set(obj, value);
	}

	public static String getNMSPackageName() {
		return "net.minecraft.server." + Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
	}

	public static String getOBCPackageName() {
		return "org.bukkit.craftbukkit." + Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
	}

	public static class PlayerInfoAction {

		public static Object UPDATE_GAME_MODE = getNMSAction("UPDATE_GAME_MODE");
		public static Object ADD_PLAYER = getNMSAction("ADD_PLAYER");
		public static Object UPDATE_DISPLAY_NAME = getNMSAction("UPDATE_DISPLAY_NAME");
		public static Object REMOVE_PLAYER = getNMSAction("REMOVE_PLAYER");
		private static Class<?> nmsClass;

		public PlayerInfoAction() {
		}

		private static Object getNMSAction(final String name) {
			try {
				final Field field = getNMSClass().getDeclaredField(name);
				field.setAccessible(true);
				return field.get((Object) null);
			} catch(final Exception var2) {
				var2.printStackTrace();
				return null;
			}
		}

		public static Class<?> getNMSClass() {
			if(nmsClass == null) {
				nmsClass = ReflectionMerchant.getClassByName(ReflectionMerchant.getNMSPackageName() + ".PacketPlayOutPlayerInfo$EnumPlayerInfoAction");
				if(nmsClass == null) {
					nmsClass = ReflectionMerchant.getClassByName(ReflectionMerchant.getNMSPackageName() + ".EnumPlayerInfoAction");
				}
			}

			return nmsClass;
		}
	}

	public static class NMSMerchantRecipe {

		private Object merchantRecipe;

		public NMSMerchantRecipe(final Object merchantRecipe) {
			this.merchantRecipe = merchantRecipe;
		}

		public NMSMerchantRecipe(final Object item1, final Object item3) {
			this(item1, (Object) null, item3);
		}

		public NMSMerchantRecipe(final Object item1, final Object item2, final Object item3) {
			try {
				final Class<?> isClass = ReflectionMerchant.getClassByName(ReflectionMerchant.getNMSPackageName() + ".ItemStack");
				this.merchantRecipe = getNMSClass().getDeclaredConstructor(new Class[] { isClass, isClass, isClass }).newInstance(new Object[] { item1, item2, item3 });
			} catch(final Exception var5) {
				var5.printStackTrace();
			}

		}

		public static Class<?> getNMSClass() {
			return ReflectionMerchant.getClassByName(ReflectionMerchant.getNMSPackageName() + ".MerchantRecipe");
		}

		public Object getBuyItem1() {
			try {
				final Method m = getNMSClass().getDeclaredMethod("getBuyItem1", new Class[0]);
				m.setAccessible(true);
				return m.invoke(this.merchantRecipe, new Object[0]);
			} catch(final Exception var2) {
				var2.printStackTrace();
				return null;
			}
		}

		public Object getBuyItem2() {
			try {
				final Method m = getNMSClass().getDeclaredMethod("getBuyItem2", new Class[0]);
				m.setAccessible(true);
				return m.invoke(this.merchantRecipe, new Object[0]);
			} catch(final Exception var2) {
				var2.printStackTrace();
				return null;
			}
		}

		public Object getBuyItem3() {
			try {
				final Method m = getNMSClass().getDeclaredMethod("getBuyItem3", new Class[0]);
				m.setAccessible(true);
				return m.invoke(this.merchantRecipe, new Object[0]);
			} catch(final Exception var2) {
				var2.printStackTrace();
				return null;
			}
		}

		public int getMaxUses() {
			try {
				final Field field = getNMSClass().getDeclaredField("maxUses");
				field.setAccessible(true);
				return field.getByte(this.merchantRecipe);
			} catch(final Exception var2) {
				var2.printStackTrace();
				return 0;
			}
		}

		public void setMaxUses(final int maxUses) {
			try {
				final Field field = getNMSClass().getDeclaredField("maxUses");
				field.setAccessible(true);
				field.set(this.merchantRecipe, Integer.valueOf(maxUses));
			} catch(final Exception var3) {
				var3.printStackTrace();
			}

		}

		public Object getMerchantRecipe() {
			return this.merchantRecipe;
		}
	}

	public static class NMSMerchantRecipeList {

		private Object handle;

		public static Class<?> getNMSClass() {
			return ReflectionMerchant.getClassByName(ReflectionMerchant.getNMSPackageName() + ".MerchantRecipeList");
		}

		public NMSMerchantRecipeList() {
			try {
				this.handle = getNMSClass().newInstance();
			} catch(final Exception var2) {
				var2.printStackTrace();
			}

		}

		public NMSMerchantRecipeList(final Object handle) {
			this.handle = handle;
		}

		public Object getHandle() {
			return this.handle;
		}

		public void clear() {
			try {
				final Method m = ArrayList.class.getDeclaredMethod("clear", new Class[0]);
				m.setAccessible(true);
				m.invoke(this.handle, new Object[0]);
			} catch(final Exception var2) {
				var2.printStackTrace();
			}

		}

		public void add(final ReflectionMerchant.NMSMerchantRecipe recipe) {
			try {
				final Method m = ArrayList.class.getDeclaredMethod("add", new Class[] { Object.class });
				m.setAccessible(true);
				m.invoke(this.handle, new Object[] { recipe.getMerchantRecipe() });
			} catch(final Exception var3) {
				var3.printStackTrace();
			}

		}

		public List<ReflectionMerchant.NMSMerchantRecipe> getRecipes() {
			final List<ReflectionMerchant.NMSMerchantRecipe> recipeList = new ArrayList<>();
			final Iterator<?> var2 = ((List<?>) this.handle).iterator();

			while(var2.hasNext()) {
				final Object obj = var2.next();
				recipeList.add(new ReflectionMerchant.NMSMerchantRecipe(obj));
			}

			return recipeList;
		}
	}

	public static class OBCCraftItemStack {

		public OBCCraftItemStack() {
		}

		public static Class<?> getOBCClass() {
			return ReflectionMerchant.getClassByName(ReflectionMerchant.getOBCPackageName() + ".inventory.CraftItemStack");
		}

		public static ItemStack asBukkitCopy(final Object nmsItemStack) {
			try {
				final Method m = getOBCClass().getDeclaredMethod("asBukkitCopy", new Class[] { ReflectionMerchant.getClassByName(ReflectionMerchant.getNMSPackageName() + ".ItemStack") });
				m.setAccessible(true);
				return (ItemStack) m.invoke((Object) null, new Object[] { nmsItemStack });
			} catch(final Exception var2) {
				var2.printStackTrace();
				return null;
			}
		}

		public static Object asNMSCopy(final ItemStack stack) {
			try {
				final Method m = getOBCClass().getDeclaredMethod("asNMSCopy", new Class[] { ItemStack.class });
				m.setAccessible(true);
				return m.invoke((Object) null, new Object[] { stack });
			} catch(final Exception var2) {
				var2.printStackTrace();
				return null;
			}
		}
	}
}
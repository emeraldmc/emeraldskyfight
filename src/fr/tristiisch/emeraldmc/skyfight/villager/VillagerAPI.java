package fr.tristiisch.emeraldmc.skyfight.villager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.api.spigot.utils.CustomConfigUtils;
import fr.tristiisch.emeraldmc.api.spigot.utils.InventoryToBase64;
import fr.tristiisch.emeraldmc.api.spigot.utils.LocationUtils;
import fr.tristiisch.emeraldmc.skyfight.Main;

public class VillagerAPI {

	public static ItemStack delete = ItemTools.create(Material.STAINED_CLAY, 1, (short) 14, "§4✖§c DELETE §4✖");
	public static ItemStack rename = ItemTools.create(Material.NAME_TAG, 1, (short) 0, "§8✐§7 RENOMER §8✐");
	public static ItemStack profession = ItemTools.create(Material.STAINED_CLAY, 1, (short) 0, "§3❂§b PROFESSION §3❂");

	public static List<PNJ> villager = new ArrayList<>();

	/*
	 * public static void enable() {
	 * TaskManager.scheduleSyncRepeatingTask("shop_villager", new
	 * VillagerRunnable(), 0, 10 * 20); }
	 */

	public static boolean isPNJ(final Villager vil) {
		return villager.stream().filter(vill -> vill.getVillager().getUniqueId() == vil.getUniqueId()).findFirst().isPresent();
	}

	public static PNJ getPNJ(final Villager vil) {
		return villager.stream().filter(vill -> vill.getVillager().getUniqueId() == vil.getUniqueId()).findFirst().orElse(null);
	}

	public static PNJ getPNJ(final String name) {
		return villager.stream().filter(vill -> vill.getVillager().getName().equalsIgnoreCase(name)).findFirst().orElse(null);
	}

	public static void loadVillagers() {
		CustomConfigUtils.createIfDoesNotExist(Main.getInstance(), "pnj");

		final FileConfiguration config = CustomConfigUtils.getConfig(Main.getInstance(), "pnj");
		for(final String str : config.getKeys(false)) {
			final ConfigurationSection s = config.getConfigurationSection(str);
			final String name = s.getString("name");
			final String inv = s.getString("inv");
			final String profession = s.getString("profession");
			final String loc = s.getString("loc");
			try {
				new PNJ(name, Profession.valueOf(profession), InventoryToBase64.fromBase64(inv), LocationUtils.convertStringToLocation(loc));

			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void saveVillagers() {
		for(final PNJ pnj : villager) {
			pnj.save();
		}
	}

	public static void removeVillagers() {
		for(final PNJ pnj : villager) {
			if(!pnj.getVillager().getLocation().getChunk().isLoaded()) {
				pnj.getVillager().getLocation().getChunk().load();
			}
			pnj.getVillager().remove();
		}
		villager.clear();
	}


	public enum SkyFightVillager{

		BLACKSMITH("Forgeron", Profession.BLACKSMITH),
		GUNSMITH("Armurier", Profession.BLACKSMITH),
		GUNSMITH2("Armurier", Profession.LIBRARIAN);

		final private String name;
		final private Profession profession;

		private SkyFightVillager(final String name, final Profession prefession) {
			this.name = name;
			this.profession = prefession;
		}

		public String getName() {
			return this.name;
		}

		public Profession getProfession() {
			return this.profession;
		}
	}

}

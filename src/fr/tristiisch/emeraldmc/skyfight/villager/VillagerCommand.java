package fr.tristiisch.emeraldmc.skyfight.villager;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager.Profession;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class VillagerCommand implements CommandExecutor, TabCompleter {

	int power;

	public VillagerCommand(final int power) {
		this.power = power;
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			if(sender instanceof Player) {
				final Player player = (Player) sender;
				final EmeraldPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
				if(emeraldPlayer.hasPowerLessThan(this.power)) {
					player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
					return;
				}
			}

			Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> {
				if(args.length == 1) {
					if(args[0].equalsIgnoreCase("reload")) {
						VillagerAPI.removeVillagers();
						VillagerAPI.loadVillagers();
						sender.sendMessage(Utils.color("Villager reload"));
					} else if(args[0].equalsIgnoreCase("info")) {
						for(final PNJ pnj : VillagerAPI.villager) {
							System.out.println(pnj.getName());
						}
					}
				} else if(args.length == 2) {
					final String name = String.valueOf(args[0]);
					Profession profession = null;
					try {
						profession = Profession.valueOf(args[1]);
					} catch(final Exception e) {
						profession = Profession.BLACKSMITH;
					}
					final Location location = ((Player) sender).getLocation();
					VillagerAPI.villager
					.add(new PNJ(name.replaceAll("_", " "), profession, null, new Location(location.getWorld(), location.getBlockX() + 0.5, location.getBlockY(), location.getBlockZ() + 0.5)));
				} else {
					this.sendHelp(sender, cmd);
				}
			});
		});
		return true;
	}

	private void sendHelp(final CommandSender sender, final Command cmd) {
		sender.sendMessage("/" + cmd.getName() + " <name> <profession>");
	}

	@Override
	public List<String> onTabComplete(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		if(cmd.getName().equalsIgnoreCase("villager")) {
			if(sender.isOp() && sender instanceof Player) {
				if(args.length == 2) {
					if(args[1].toLowerCase().startsWith("b")) {
						if(args.length > 2) {
							return Arrays.asList("");
						}
						if(args[1].toLowerCase().startsWith("bu")) {
							if(args.length > 2) {
								return Arrays.asList("");
							}
							return Arrays.asList(Profession.BUTCHER.name());
						} else if(args[1].toLowerCase().startsWith("bl")) {
							if(args.length > 2) {
								return Arrays.asList("");
							}
							return Arrays.asList(Profession.BLACKSMITH.name());
						}
						return Arrays.asList(Profession.BUTCHER.name(), Profession.BLACKSMITH.name());
					} else if(args[1].toLowerCase().startsWith("f")) {
						if(args.length > 2) {
							return Arrays.asList("");
						}
						return Arrays.asList(Profession.FARMER.name());
					} else if(args[1].toLowerCase().startsWith("l")) {
						if(args.length > 2) {
							return Arrays.asList("");
						}
						return Arrays.asList(Profession.LIBRARIAN.name());
					} else if(args[1].toLowerCase().startsWith("p")) {
						if(args.length > 2) {
							return Arrays.asList("");
						}
						return Arrays.asList(Profession.PRIEST.name());
					} else {
						return Arrays.asList(Profession.BUTCHER.name(), Profession.BLACKSMITH.name(), Profession.FARMER.name(), Profession.LIBRARIAN.name(), Profession.PRIEST.name());
					}
				} else {
					return Arrays.asList("");
				}
			}
		}
		return null;
	}
	/*
	 * @Override public boolean onCommand2(final CommandSender sender, final Command
	 * cmd, final String label, final String[] args) { if(!(sender instanceof
	 * Player)) {
	 * sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().
	 * getString("commun.messages.cantconsole"))); return true; } final Player
	 * player = (Player) sender; final EmeraldPlayer emeraldPlayer =
	 * EmeraldPlayers.getPlayer(player.getName());
	 * if(emeraldPlayer.hasPowerLessThan(this.power)) {
	 * player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().
	 * getString("commun.messages.noperm"))); return true; } final Villager villager
	 * = VillagerAPI.spawn(SkyFightVillager.BLACKSMITH, player.getLocation());
	 * VillagerTradeApi.clearTrades(villager); VillagerTradeApi.addTrade(villager,
	 * new VillagerTrade(new ItemStack(Material.STONE), new
	 * ItemStack(Material.STONE, 1))); player.sendMessage(Utils.color("&6done"));
	 * Entity nmsVillager = ((CraftEntity) villager).getHandle(); NBTTagCompound tag
	 * = nmsVillager.getNBTTag(); if(tag == null){ tag = new NBTTagCompound(); }
	 * nmsVillager.c(tag); tag.setInt("CareerLevel", 100); tag.setInt("rewardExp",
	 * 0); nmsVillager.f(tag); Location loc = player.getLocation(); Villager npc =
	 * (Villager) loc.getWorld().spawnEntity(loc, EntityType.VILLAGER); //custom le
	 * mob nms nmsVillager.setCustomName("§eVendeur de §6Kebab");
	 * nmsVillager.setCustomNameVisible(true);
	 * nmsVillager.setPositionRotation(loc.getX(), loc.getY(), loc.getZ(),
	 * loc.getYaw(), loc.getPitch()); //desactiver l'ai NBTTagCompound tag =
	 * nmsVillager.getNBTTag(); if(tag == null){ tag = new NBTTagCompound(); }
	 * return true; }
	 */
}
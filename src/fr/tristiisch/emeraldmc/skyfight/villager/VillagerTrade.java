package fr.tristiisch.emeraldmc.skyfight.villager;

import org.bukkit.inventory.ItemStack;

public final class VillagerTrade {

	private final ItemStack item1;
	private ItemStack item2;
	private final ItemStack rewardItem;

	public VillagerTrade(final ItemStack item1, final ItemStack item2, final ItemStack rewardItem) {
		this.item1 = item1;
		this.item2 = item2;
		this.rewardItem = rewardItem;
	}

	public VillagerTrade(final ItemStack item1, final ItemStack rewardItem) {
		this.item1 = item1;
		this.rewardItem = rewardItem;
	}

	public boolean hasItem2() {
		return this.item2 != null;
	}

	public ItemStack getItem1() {
		return this.item1;
	}

	public ItemStack getItem2() {
		return this.item2;
	}

	public ItemStack getRewardItem() {
		return this.rewardItem;
	}
}

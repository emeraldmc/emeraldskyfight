package fr.tristiisch.emeraldmc.skyfight.villager;

import java.lang.reflect.Field;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftVillager;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.inventory.Inventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;
import fr.tristiisch.emeraldmc.api.spigot.utils.CustomConfigUtils;
import fr.tristiisch.emeraldmc.api.spigot.utils.InventoryToBase64;
import fr.tristiisch.emeraldmc.api.spigot.utils.LocationUtils;
import fr.tristiisch.emeraldmc.skyfight.Main;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.EntityVillager;
import net.minecraft.server.v1_8_R3.PathfinderGoalFloat;
import net.minecraft.server.v1_8_R3.PathfinderGoalLookAtPlayer;
import net.minecraft.server.v1_8_R3.PathfinderGoalLookAtTradingPlayer;
import net.minecraft.server.v1_8_R3.PathfinderGoalSelector;
import net.minecraft.server.v1_8_R3.PathfinderGoalTradeWithPlayer;

public class PNJ {

	private String name;
	private Villager villager;
	private Profession profession;
	private Inventory inventory;
	private Location location;

	public PNJ(final String name, final Profession profession, final Inventory inventory, final Location location) {
		this.name = Utils.color(name);
		/* this.name = name; */
		this.profession = profession;
		this.inventory = inventory;
		this.location = location;
		this.initPNJ();
	}

	public void initPNJ() {
		if(this.inventory == null) {
			this.setInventory(Bukkit.createInventory(null, 3 * 9, this.name));
		}
		this.getInventory().setItem(26, VillagerAPI.delete);
		this.getInventory().setItem(17, VillagerAPI.rename);
		this.getInventory().setItem(8, VillagerAPI.profession);

		if(!this.location.getChunk().isLoaded()) {
			this.location.getChunk().load();
		}

		final String taskName = TaskManager.getTaskName("ShopPnjSpawn");
		TaskManager.scheduleSyncRepeatingTask(taskName, () -> {
			final PNJ oldPnj = VillagerAPI.getPNJ(this.getName());
			if(oldPnj == null || !oldPnj.getVillager().isValid()) {

				this.villager = (Villager) this.getLocation().getWorld().spawnEntity(this.getLocation(), EntityType.VILLAGER);
				this.villager.setCustomName(this.getName());

				if(this.villager.isValid()) {
					this.villager.getNearbyEntities(20, 5, 20)
					.stream()
					.filter(e -> e.getType() == EntityType.VILLAGER && (e.getCustomName() == null || e.getCustomName().equalsIgnoreCase(this.getName())))
					.forEach(e -> e.remove());
					this.villager.setProfession(this.getProfession());
					this.villager.setAdult();
					this.villager.setCustomNameVisible(true);
					this.villager.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 999999 * 20, 255, true, false), true);

					this.overwriteVillagerAI(this.villager);
					VillagerAPI.villager.add(this);
				}
			}
		}, 0, 60 * 20);




		/*
		 * new BukkitRunnable() {
		 * @Override public void run() { if(PNJ.this.villager.getLocation() !=
		 * PNJ.this.location) { PNJ.this.villager.teleport(PNJ.this.location); } }
		 * }.runTaskTimer(this.plugin, 1L, 2L);
		 */
	}

	private void overwriteVillagerAI(final LivingEntity villager) {
		try {
			final EntityVillager ev = ((CraftVillager) villager).getHandle();

			final Field goalsField = EntityInsentient.class.getDeclaredField("goalSelector");
			goalsField.setAccessible(true);
			final PathfinderGoalSelector goals = (PathfinderGoalSelector) goalsField.get(ev);

			Field listField = PathfinderGoalSelector.class.getDeclaredField("b");
			listField.setAccessible(true);
			List<?> list = (List<?>) listField.get(goals);
			list.clear();
			listField = PathfinderGoalSelector.class.getDeclaredField("c");
			listField.setAccessible(true);
			list = (List<?>) listField.get(goals);
			list.clear();

			goals.a(0, new PathfinderGoalFloat(ev));
			goals.a(1, new PathfinderGoalTradeWithPlayer(ev));
			goals.a(1, new PathfinderGoalLookAtTradingPlayer(ev));
			goals.a(2, new PathfinderGoalLookAtPlayer(ev, EntityHuman.class, 20.0F, 1.0F));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void save() {
		String dta64 = InventoryToBase64.toBase64(this.getInventory());
		dta64 = dta64.replaceAll("[\r\n]+", "");
		final FileConfiguration config = CustomConfigUtils.getConfig(Main.getInstance(), "pnj");

		int size = config.getKeys(false).size() + 1;
		for(final String keys : config.getKeys(false)) {
			if(config.getConfigurationSection(keys).getString("name").equalsIgnoreCase(this.getName())) {
				size = Integer.parseInt(keys.replaceAll("<", "").replaceAll(">", ""));
			}
		}

		config.set(size + ".name", this.getName());
		config.set(size + ".inv", dta64);
		config.set(size + ".profession", this.getProfession().name());
		config.set(size + ".loc", LocationUtils.convertLocationToString(this.getLocation()));
		CustomConfigUtils.saveConfig(Main.getInstance(), config, "pnj");
	}

	public void delete() {
		final FileConfiguration config = CustomConfigUtils.getConfig(Main.getInstance(), "pnj");
		int size = 0;
		for(final String keys : config.getKeys(false)) {
			if(config.getConfigurationSection(keys)
					.getString("name")
					.equalsIgnoreCase(this.getName())) {
				size = Integer.parseInt(keys.replaceAll("<", "").replaceAll(">", ""));
			}
		}
		config.set(size + "", null);
		CustomConfigUtils.saveConfig(Main.getInstance(), config, "pnj");
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Profession getProfession() {
		return this.profession;
	}

	public Villager getVillager() {
		return this.villager;
	}

	public void setProfession(final Profession profession) {
		this.profession = profession;
	}

	public Inventory getInventory() {
		return this.inventory;
	}

	public void setInventory(final Inventory inventory) {
		this.inventory = inventory;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(final Location location) {
		this.location = location;
	}

}

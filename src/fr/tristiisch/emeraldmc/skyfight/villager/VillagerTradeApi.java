package fr.tristiisch.emeraldmc.skyfight.villager;

import java.lang.reflect.Field;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftVillager;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.spigot.utils.Reflection;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityVillager;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IMerchant;
import net.minecraft.server.v1_8_R3.MerchantRecipe;
import net.minecraft.server.v1_8_R3.MerchantRecipeList;

public final class VillagerTradeApi {

	public static void clearTrades(final Villager villager) {
		final EntityVillager entityVillager = ((CraftVillager) villager).getHandle();
		try {
			final Field recipes = entityVillager.getClass().getDeclaredField("br");
			recipes.setAccessible(true);
			final MerchantRecipeList list = new MerchantRecipeList();
			recipes.set(entityVillager, list);
		} catch(final Exception e) {
			e.printStackTrace();
		}
	}

	public static void addTrade(final Villager villager, final VillagerTrade villagerTrade) {
		addTrade(villager, villagerTrade, Integer.MAX_VALUE);
	}

	public static void addTrade(final Villager villager, final VillagerTrade villagerTrade, final int maxUses) {
		final EntityVillager entityVillager = ((CraftVillager) villager).getHandle();
		try {
			final Field recipes = entityVillager.getClass().getDeclaredField("br");
			recipes.setAccessible(true);
			final MerchantRecipeList list = (MerchantRecipeList) recipes.get(entityVillager);
			if(villagerTrade.hasItem2()) {
				final net.minecraft.server.v1_8_R3.ItemStack item1 = CraftItemStack.asNMSCopy(villagerTrade.getItem1());
				final net.minecraft.server.v1_8_R3.ItemStack item2 = CraftItemStack.asNMSCopy(villagerTrade.getItem2());
				final net.minecraft.server.v1_8_R3.ItemStack rewardItem = CraftItemStack.asNMSCopy(villagerTrade.getRewardItem());
				final MerchantRecipe recipe = new MerchantRecipe(item1, item2, rewardItem);
				Reflection.setField(recipe, "maxUses", maxUses);
				list.add(recipe);
			} else {
				final net.minecraft.server.v1_8_R3.ItemStack item1 = CraftItemStack.asNMSCopy(villagerTrade.getItem1());
				final net.minecraft.server.v1_8_R3.ItemStack rewardItem2 = CraftItemStack.asNMSCopy(villagerTrade.getRewardItem());
				final MerchantRecipe recipe2 = new MerchantRecipe(item1, rewardItem2);
				Reflection.setField(recipe2, "maxUses", maxUses);
				list.add(recipe2);
			}
			recipes.set(entityVillager, list);
		} catch(final Exception e) {
			e.printStackTrace();
		}
	}
	private final String invname;
	private final MerchantRecipeList l;

	public VillagerTradeApi(final String invname) {
		this.invname = invname;
		this.l = new MerchantRecipeList();
	}

	public VillagerTradeApi addTrande(final ItemStack in, final ItemStack out) {
		this.addTrande(in, new ItemStack(Material.AIR), out);
		return this;
	}

	public VillagerTradeApi addTrande(final ItemStack inOne, final ItemStack inTwo, final ItemStack out) {
		this.l.add(new MerchantRecipe(CraftItemStack.asNMSCopy(inOne), CraftItemStack.asNMSCopy(inTwo), CraftItemStack.asNMSCopy(out), Integer.MIN_VALUE, Integer.MAX_VALUE));
		return this;
	}

	public void openTrande(final Player who) {
		final EntityHuman e = ((CraftPlayer) who).getHandle();
		e.openTrade(new IMerchant() {

			@Override
			public MerchantRecipeList getOffers(final EntityHuman arg0) {
				return VillagerTradeApi.this.l;
			}


			@Override
			public void a_(final net.minecraft.server.v1_8_R3.ItemStack arg0) {
			}

			@Override
			public void a_(final EntityHuman arg0) {
			}

			@Override
			public IChatBaseComponent getScoreboardDisplayName() {
				return (IChatBaseComponent) ReflectionMerchant.createNMSTextComponent(VillagerTradeApi.this.invname);
				/* return ChatSerializer.a(VillagerTradeApi.this.invname); */
			}

			@Override
			public EntityHuman v_() {
				return e;
			}

			@Override
			public void a(final MerchantRecipe arg0) {
			}
		});
	}
	/*
	 * public void openTrading(final Object player) { this.c = player; try { final
	 * Class<?> classs =
	 * ReflectionMerchant.getClassByName(ReflectionMerchant.getNMSPackageName() +
	 * ".EntityPlayer"); Method m; if(this.getMethodArgs(classs, "openTrade") == 2)
	 * { m = classs.getDeclaredMethod("openTrade", new Class[] {
	 * ReflectionMerchant.getClassByName(ReflectionMerchant.getNMSPackageName() +
	 * ".IMerchant"), String.class }); m.setAccessible(true); m.invoke(player, new
	 * Object[] { this.proxy, this.title }); } else { m =
	 * classs.getDeclaredMethod("openTrade", new Class[] {
	 * ReflectionMerchant.getClassByName(ReflectionMerchant.getNMSPackageName() +
	 * ".IMerchant") }); m.setAccessible(true); m.invoke(player, new Object[] {
	 * this.proxy }); } } catch(final Exception var4) { var4.printStackTrace(); } }
	 */
}

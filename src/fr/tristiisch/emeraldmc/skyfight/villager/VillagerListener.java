package fr.tristiisch.emeraldmc.skyfight.villager;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.sneak.Sneak;

public class VillagerListener implements Listener {

	/* private final HashMap<String, String> nametag = new HashMap<>(); */


	@EventHandler
	public void PlayerInteractEntityEvent(final PlayerInteractEntityEvent event) {
		final Player player = event.getPlayer();
		if(event.getRightClicked() instanceof Villager) {

			Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
				final PNJ pnj = VillagerAPI.getPNJ((Villager) event.getRightClicked());
				if(pnj == null) {
					return;
				}


				// Admin mode
				final EmeraldPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
				if(Sneak.isSneak(player) && player.getGameMode().equals(GameMode.CREATIVE) && emeraldPlayer.hasPowerMoreThan(EmeraldGroup.ADMIN)) {
					final Inventory inventory = Bukkit.createInventory(null, 3 * 9, pnj.getName());
					inventory.setContents(pnj.getInventory().getContents());
					event.getPlayer().openInventory(inventory);
					event.setCancelled(true);
					return;
				}

				final Inventory inv = pnj.getInventory();
				final VillagerTradeApi trade = new VillagerTradeApi(pnj.getName());
				for(int i = 0; i < 8; i++) {
					if(inv.getItem(i) != null && inv.getItem(i + 18) != null) {
						if(inv.getItem(i + 9) != null) {
							final ItemStack item1 = inv.getItem(i);
							final ItemStack item2 = inv.getItem(i + 9);
							final ItemStack item3 = inv.getItem(i + 18);
							trade.addTrande(item1, item2, item3);
						} else {
							final ItemStack item1 = inv.getItem(i);
							final ItemStack item3 = inv.getItem(i + 18);
							trade.addTrande(item1, item3);
						}
					}

				}
				trade.openTrande(event.getPlayer());
				event.setCancelled(true);
			});
		}
	}


	@EventHandler
	public void InventoryCloseEvent(final InventoryCloseEvent event) {
		final PNJ pnj = VillagerAPI.getPNJ(event.getInventory().getName());
		if(pnj == null) {
			return;
		}

		if(event.getInventory().getType() == InventoryType.CHEST) {
			final Inventory modify = event.getInventory();
			pnj.setInventory(modify);
			pnj.save();
		}
	}

	@EventHandler
	public void InventoryClickEvent(final InventoryClickEvent event) {
		final PNJ pnj = VillagerAPI.getPNJ(event.getInventory().getName());
		if(pnj == null) {
			return;
		}
		final Player player = (Player) event.getWhoClicked();
		if(event.getInventory().getType() == InventoryType.CHEST) {
			if(event.getCurrentItem() != null) {
				if(event.getCurrentItem().isSimilar(VillagerAPI.delete)) {
					pnj.getVillager().remove();
					pnj.delete();
					event.getWhoClicked().closeInventory();
					event.setCancelled(true);

				} else if(event.getCurrentItem().isSimilar(VillagerAPI.rename)) {
					event.setCancelled(true);
					player.sendMessage("§c>> En dev");
					player.playSound(event.getWhoClicked().getEyeLocation(), Sound.BURP, 10.0F, 10.0F);
				} else if(event.getCurrentItem().isSimilar(VillagerAPI.profession)) {
					event.setCancelled(true);
					player.sendMessage("§c>> En dev");
					player.playSound(event.getWhoClicked().getEyeLocation(), Sound.BURP, 10.0F, 10.0F);
				}
			}
		}
	}

	/*
	 * @EventHandler public void onClickInterAnvil(final InventoryClickEvent event)
	 * { if(this.nametag.get(((Player) event.getWhoClicked()).getName()) == null) {
	 * return; } PNJ pnj = null; for(final PNJ str : VillagerAPI.villager) {
	 * Bukkit.broadcastMessage("Str: " + str.getName() + " <=> " + this.nametag
	 * .get(((Player) event.getWhoClicked()).getName()) + "\n" +
	 * event.getInventory().getName().equals(this.nametag.get(((Player)
	 * event.getWhoClicked()).getName()).toString()));
	 * if(event.getInventory().getName().equals(this.nametag.get(((Player)
	 * event.getWhoClicked()).getName()).toString())) { pnj = str; } } if(pnj ==
	 * null) { Bukkit.broadcastMessage("return ->" + this.nametag.get(((Player)
	 * event.getWhoClicked()).getName())); return; }
	 * if(event.getInventory().getTitle().equalsIgnoreCase(pnj.getName())) {
	 * if(event.getCurrentItem() != null) { Bukkit.broadcastMessage("1");
	 * if(this.nametag.containsKey(((Player) event.getWhoClicked()).getName())) {
	 * Bukkit.broadcastMessage("2"); event.setCancelled(true); if(event.getRawSlot()
	 * == 2) { Bukkit.broadcastMessage("3"); final ItemMeta meta =
	 * event.getCurrentItem().getItemMeta(); if(meta != null &&
	 * meta.hasDisplayName()) { Bukkit.broadcastMessage("4");
	 * event.getInventory().clear(); event.getView().close();
	 * pnj.setName(meta.getDisplayName()); this.nametag.remove(((Player)
	 * event.getWhoClicked()).getName()); ((Player)
	 * event.getWhoClicked()).playSound(event.getWhoClicked().getEyeLocation(),
	 * Sound.NOTE_PLING, 10.0F, 10.0F); } } } } } }
	 */

	@EventHandler
	public void EntityDamageEvent(final EntityDamageEvent event) {
		if(event.getEntityType() == EntityType.VILLAGER) {
			if(VillagerAPI.isPNJ((Villager) event.getEntity())) {
				event.setCancelled(true);
			}
		}
	}

	/*
	 * @EventHandler public void damage(final PlayerDropItemEvent event) {
	 * if(event.getItemDrop().getItemStack().getType() == Material.NAME_TAG) {
	 * if(this.nametag.containsKey(event.getPlayer().getName())) {
	 * event.getItemDrop().remove();
	 * this.nametag.remove(event.getPlayer().getName()); } } }
	 */
}

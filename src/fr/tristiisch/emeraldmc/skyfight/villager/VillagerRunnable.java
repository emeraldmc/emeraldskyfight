package fr.tristiisch.emeraldmc.skyfight.villager;

import org.bukkit.scheduler.BukkitRunnable;

public class VillagerRunnable extends BukkitRunnable {

	@Override
	public void run() {
		for(final PNJ pnj : VillagerAPI.villager) {
			if(pnj.getVillager().isDead()) {
				pnj.initPNJ();
			}
		}
	}
}

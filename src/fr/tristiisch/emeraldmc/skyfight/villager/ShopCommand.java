package fr.tristiisch.emeraldmc.skyfight.villager;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import fr.tristiisch.emeraldmc.api.commons.Utils;

public class ShopCommand implements CommandExecutor {

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		sender.sendMessage(Utils.color("&2EmeraldMC &7» &bPour accéder au shop, utilisez les portails en obsidienne."));
		return true;
	}
}

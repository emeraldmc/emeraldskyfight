package fr.tristiisch.emeraldmc.skyfight.armor;

import java.util.Arrays;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.spigot.command.SpigotCommand;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayer;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayers;

public class ArmorCommand extends SpigotCommand {

	public ArmorCommand(final Plugin plugin, final EmeraldGroup... groups) {
		super(plugin, "armor", Arrays.asList("armure"), groups);
		this.allowConsole = false;
		this.register();
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {

		final GuiTools gui = new GuiTools("&2Personnalisation de l'armure ", "armor", ColorUtils.values().length - 1);

		for(final ColorUtils colorUtils : ColorUtils.values()) {

			final SkyFightPlayer skyFightPlayer = SkyFightPlayers.getPlayer(this.player);
			final Color oldColor = skyFightPlayer.getArmorColor();
			final Color color = colorUtils.getColor();

			final ItemStack item = ItemTools.createColoredArmor(Material.LEATHER_CHESTPLATE, color, "&7" + colorUtils.getName(), null);
			if(oldColor != null && color.equals(oldColor)) {
				ItemTools.addGlow(item);
			}
			gui.addItem(item);

		}

		gui.openInventory(this.player);
		return true;
	}

}

package fr.tristiisch.emeraldmc.skyfight.armor;

import org.bukkit.Color;

public enum ColorUtils {

	AQUA(Color.AQUA),
	BLACK(Color.BLACK),
	BLUE(Color.BLUE),
	FUCHSIA(Color.FUCHSIA),
	GRAY(Color.GRAY),
	GREEN(Color.GREEN),
	LIME(Color.LIME),
	MAROON(Color.MAROON),
	NAVY(Color.NAVY),
	OLIVE(Color.OLIVE),
	ORANGE(Color.ORANGE),
	PURPLE(Color.PURPLE),
	RED(Color.RED),
	SILVER(Color.SILVER),
	TEAL(Color.TEAL),
	WHITE(Color.WHITE),
	YELLOW(Color.YELLOW);

	private final Color color;

	private ColorUtils(final Color color) {
		this.color = color;
	}

	public Color getColor() {
		return this.color;
	}

	public String getName() {
		return this.toString().substring(0, 1).toUpperCase() + this.toString().substring(1).toLowerCase();
	}
}

package fr.tristiisch.emeraldmc.skyfight.armor;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import fr.tristiisch.emeraldmc.api.commons.Prefix;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools.GuiData;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayer;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayers;
import fr.tristiisch.emeraldmc.skyfight.fight.Stuff;

public class ArmorListener implements Listener {

	@EventHandler
	private void InventoryClickEvent(final InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();
		final Inventory clickedInventory = event.getClickedInventory();

		if(clickedInventory == null) {
			return;
		}
		final ItemStack currentItem = event.getCurrentItem();

		final GuiData guiData = GuiTools.getGuiData(player);
		if(guiData != null) {
			switch(guiData.getId()) {
			case "armor":
				event.setCancelled(true);
				final Color armorColor = ItemTools.getColorOfArmor(currentItem);
				final SkyFightPlayer skyFightPlayer = SkyFightPlayers.getPlayer(player);
				skyFightPlayer.setArmorColor(armorColor);
				player.sendMessage(Prefix.DEFAULT + Utils.color("Vous avez changer votre couleur d'amure."));

				final PlayerInventory playerInventory = player.getInventory();

				final List<Material> leatherArmor = Arrays.asList(Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.LEATHER_BOOTS);
				if(Arrays.stream(playerInventory.getArmorContents()).filter(item -> item != null && !leatherArmor.contains(item.getType())).count() == 0) {
					Stuff.setArmor(player);
				}

				player.closeInventory();
				break;
			}
		}
	}

}

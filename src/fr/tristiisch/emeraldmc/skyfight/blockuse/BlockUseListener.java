package fr.tristiisch.emeraldmc.skyfight.blockuse;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.Door;
import org.bukkit.material.Gate;
import org.bukkit.material.TrapDoor;

import fr.tristiisch.emeraldmc.api.spigot.core.build.Build;
import fr.tristiisch.emeraldmc.skyfight.Main;

public class BlockUseListener implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.LOW)
	public void PlayerInteractEvent(final PlayerInteractEvent event) {
		if(event.isCancelled()) {
			return;
		}
		final Player player = event.getPlayer();
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {

			if(Build.isBuildModeEnable(player)) {
				return;
			}
			final Block block = event.getClickedBlock();
			final BlockState blockState = block.getState();
			switch(block.getType()) {
			case BREWING_STAND:
			case FURNACE:
			case DROPPER:
			case HOPPER:
			case DISPENSER:
			case TRAPPED_CHEST:
			case CHEST:
			case BEACON:
			case BED:
				event.setCancelled(true);
				break;
			case TRAP_DOOR:
			case IRON_TRAPDOOR:

				/*				if(SpigotUtils.getBlockAround(block.getLocation(), 2).stream().filter(location -> location.getBlock().getType() == Material.TRAP_DOOR).findFirst().isPresent()) {
									event.setCancelled(true);
									return;
								}*/
				final TrapDoor trapDoor = (TrapDoor) block.getState().getData();
				if(this.isOpen(trapDoor.getData())) {
					Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {
						trapDoor.setOpen(false);
						blockState.update(true);
					}, 3 * 20);
				} else {
					event.setCancelled(true);
				}
				break;
			case FENCE_GATE:
			case DARK_OAK_FENCE_GATE:
			case BIRCH_FENCE_GATE:
			case ACACIA_FENCE_GATE:
			case SPRUCE_FENCE_GATE:
			case JUNGLE_FENCE_GATE:

				final Gate gate = (Gate) block.getState().getData();
				if(this.isOpen(gate.getData())) {
					Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {
						gate.setOpen(false);
						blockState.update(true);
					}, 3 * 20);
				} else {
					event.setCancelled(true);
				}
				break;
			case WOODEN_DOOR:
			case DARK_OAK_DOOR:
			case BIRCH_DOOR:
			case ACACIA_DOOR:
			case SPRUCE_DOOR:
			case JUNGLE_DOOR:
			case IRON_DOOR:
				final Door door = (Door) block.getState().getData();
				if(this.isOpen(door.getData())) {
					Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {
						door.setOpen(false);
						blockState.update(true);
					}, 3 * 20);
				} else {
					event.setCancelled(true);
				}
				break;
				/*				Block other = null;

								if((block.getData() & 0x8) == 0x8) {
									block = block.getRelative(BlockFace.DOWN); // get the lower block if necessary
								}

												if(!this.isOpen(block.getData())) {
													event.setCancelled(true);
													break;
												}

								for(final BlockFace bf : FACES) {
									final Block b = block.getRelative(bf);
									if(BlockUseListener.this.doorsAreConnected(block, b)) {
										other = b;
									}
								}
								if(other == null) {
									return;
								}

								if(block.getData() != other.getData()) {
									other.setData((byte) (other.getData() ^ 0x4)); // flip the open/closed bit
									other.getState().update(true);
									if(this.isOpen(other.getData())) {
										final Block other2 = other;

										Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {
											if(this.isOpen(other2.getData())) {
											other2.setData((byte) (other2.getData() ^ 0x4));
											other2.getState().update(true);
											}
										}, 3 * 20);
									}
								}*/
				/*				final List<Block> doors = SpigotUtils.getBlockAround(block.getLocation(), 1)
								.stream()
								.filter(location -> location.getBlock().getType() == block.getType())
								.map(location -> location.getBlock())
								.collect(Collectors.toList());
								doors.forEach(doorBlock -> {
									final Door door = (Door) doorBlock.getState().getData();
									if(doorBlock.getLocation().getY() == block.getLocation().getY()) {
										if(this.isOpen(door.getData())) {
											Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {
												door.setOpen(false);
												blockState.update(true);
											}, 3 * 20);
										} else {
											event.setCancelled(true);
										}
									}
								});*/
			default:
				break;
			}
		}

	}

	@SuppressWarnings({ "deprecation", "unused" })
	private boolean doorsAreConnected(final Block a, final Block b) {
		if(a.getType() != b.getType()) {
			return false;
		}

		if((a.getRelative(BlockFace.UP).getData() & 0x1) == (b.getRelative(BlockFace.UP).getData() & 0x1)) {
			return false;
		}
		return (a.getData() & 0x3) == (b.getData() & 0x3);
	}

	private boolean isOpen(final byte materialData) {
		final boolean open = true;
		switch(materialData) {
		case 0: {
			return open;
		}
		case 1: {
			return open;
		}
		case 2: {
			return open;
		}
		case 3: {
			return open;
		}
		case 8: {
			return open;
		}
		case 9: {
			return open;
		}
		case 10: {
			return open;
		}
		case 11: {
			return open;
		}
		default: {
			return false;
		}
		}
	}

/*	private static final BlockFace[] FACES = { BlockFace.NORTH, BlockFace.SOUTH, BlockFace.EAST, BlockFace.WEST };*/

}

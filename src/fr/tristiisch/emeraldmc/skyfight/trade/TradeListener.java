package fr.tristiisch.emeraldmc.skyfight.trade;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class TradeListener implements Listener {

	@EventHandler
	public void InventoryClickEvent(final InventoryClickEvent event) {
		if(!(event.getWhoClicked() instanceof Player)) {
			return;
		}
		final Player player = (Player) event.getWhoClicked();
		if(!TradeUtils.isTrading(player)) {
			return;
		}
		event.setCancelled(true);
		player.sendMessage("Bon trade ! ");
	}

	@EventHandler
	public void InventoryCloseEvent(final InventoryCloseEvent event) {
		if(!(event.getPlayer() instanceof Player)) {
			return;
		}
		final Player player = (Player) event.getPlayer();
		final EmeraldTrade trade = TradeUtils.getTrade(player);
		if(trade == null || !trade.isTrading()) {
			return;
		}
		TradeUtils.removeTrade(trade);
	}
}

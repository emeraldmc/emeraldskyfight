package fr.tristiisch.emeraldmc.skyfight.trade;

public enum EmeraldTradeStatus {

	WAITING(),
	TRADING(),
	END();
}

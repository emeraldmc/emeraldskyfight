package fr.tristiisch.emeraldmc.skyfight.trade;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;

public class TradeCommand implements CommandExecutor {

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			if(!(sender instanceof Player)) {
				sender.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.cantconsole")));
			}
			final Player player = (Player) sender;
			final EmeraldPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			if(emeraldPlayer.hasPowerLessThan(EmeraldGroup.ADMIN)) {
				player.sendMessage(Utils.color(EmeraldSpigot.getInstance().getConfig().getString("commun.messages.noperm")));
				return;
			}

			if(args.length >= 1) {
				final Player target = Bukkit.getPlayer(args[0]);
				if(target == null) {
					player.sendMessage(Utils.color("&2EmeraldMC &7» &4" + args[0] + "&c est introuvable."));
					return;
				}
				/*
				 * if(target.getUniqueId().equals(player.getUniqueId())) {
				 * player.sendMessage(Utils.
				 * color("&2EmeraldMC &7» &cLa schizophrénie se soigne, ne vous inquiété pas."))
				 * ; return true; }
				 */
				/*TradeUtils.askTrade(player, target);*/
			} else {
				player.sendMessage(Utils.color("&cUsage &7» &c/echange <pseudo>"));
			}
		});
		return true;
	}
}

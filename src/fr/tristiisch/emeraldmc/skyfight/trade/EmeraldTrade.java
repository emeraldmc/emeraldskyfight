package fr.tristiisch.emeraldmc.skyfight.trade;

import java.util.Arrays;
import java.util.List;

import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.Utils;

public class EmeraldTrade {

	private final Player trader;
	private final Player trader2;
	private final boolean tradeStatus;
	private final long time;

	public EmeraldTrade(final Player trader, final Player trader2) {
		this.trader = trader;
		this.trader2 = trader2;
		this.tradeStatus = false;
		this.time = Utils.getCurrentTimeinSeconds() + 60 * 5;
	}

	public Player getTrader() {
		return this.trader;
	}

	public List<Player> getTraders() {
		return Arrays.asList(this.trader, this.trader2);
	}

	public Player getTrader2() {
		return this.trader2;
	}

	public boolean isTrading() {
		return this.tradeStatus;
	}

	public boolean isExpire() {
		return Utils.getCurrentTimeinSeconds() > this.time;
	}
}

package fr.tristiisch.emeraldmc.skyfight.trade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools.GuiData;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class TradeUtils {

	public static String guiid = "EmeraldTrade";
	private static List<EmeraldTrade> trades = new ArrayList<>();

	public static boolean isGuiTradeOpened(final Player player) {
		final GuiData guiData = GuiTools.getGuiData(player);
		return guiData != null && guiData.getId().equals(guiid);
	}

	public static boolean isTrading(final Player player) {
		return getTrade(player) != null;
	}

	public static boolean isTrading(final Player trader, final Player trader2) {
		return getTrade(trader, trader2) != null;
	}

	public static EmeraldTrade getTrade(final Player player) {
		return trades.stream().filter(trade -> trade.getTrader().getUniqueId().equals(player.getUniqueId())).findFirst().orElse(null);
	}

	public static EmeraldTrade getTrade(final Player trader, final Player trader2) {
		return trades.stream().filter(trade -> trade.getTraders().contains(trader) && trade.getTraders().contains(trader2)).findFirst().orElse(null);
	}

	public static void removeTrade(final Player player) {
		trades.remove(getTrade(player));
	}

	public static void removeTrade(final EmeraldTrade trade) {
		trade.getTrader().closeInventory();
		trade.getTrader2().closeInventory();
		trade.getTrader().sendMessage(Utils.color("&2EmeraldMC &7» &aVotre échange avec &2" + trade.getTrader2() + "&a est terminée."));
		trade.getTrader2().sendMessage(Utils.color("&2EmeraldMC &7» &aVotre échange avec &2" + trade.getTrader() + "&a est terminée."));
		trades.remove(trade);
	}

	public static void addTrade(final EmeraldTrade trade) {
		trades.add(trade);
	}

	public static void askTrade(final Player trader, final Player trader2) {
		EmeraldTrade trade = getTrade(trader, trader2);
		if(trade != null) {
			if(!trade.isTrading()) {
				openTrade(trade);
			}
			return;
		}

		trade = new EmeraldTrade(trader, trader2);
		addTrade(trade);
		trader.sendMessage(Utils.color("&2EmeraldMC &7» &2" + trader2.getName() + "&a a bien reçu votre proposition d'échange."));

		final TextComponent msg = new TextComponent(Utils.colorFix("&2EmeraldMC &7» &2" + trader.getName() + "&a vous propose un échange."));
		msg.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Utils.color("&3Cliquez pour accepter l'échange avec &b" + trader.getName())).create()));
		msg.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/trade " + trader.getName()));
		trader2.spigot().sendMessage(msg);
	}

	public static void openTrade(final EmeraldTrade trade) {
		if(trade.isExpire()) {
			trade.getTrader().sendMessage(Utils.color("&2EmeraldMC &7» &cVotre proposition échange avec &4" + trade.getTrader2() + "&c est expirée."));
			trade.getTrader2().sendMessage(Utils.color("&2EmeraldMC &7» &cVotre proposition échange avec &4" + trade.getTrader() + "&c est expirée."));
			removeTrade(trade);
			return;
		}
		final GuiTools gui = new GuiTools("&6Echange", TradeUtils.guiid, 5);

		final ItemStack divider = ItemTools.create(Material.IRON_FENCE, 1, (short) 0, "divider");
		final ItemStack declineTrade = ItemTools.create(Material.WOOL, 1, (short) 14, "decline-trade", Arrays.asList("decline-trade"));
		final ItemStack acceptTrade = ItemTools.create(Material.WOOL, 1, (short) 5, "accept-trade", Arrays.asList("accept-trade"));
		final ItemStack readyOrNot = ItemTools.create(Material.INK_SACK, 1, (short) 8, "not-ready");
		gui.setItem(4, divider);
		gui.setItem(13, divider);
		gui.setItem(22, divider);
		gui.setItem(31, divider);
		gui.setItem(27, acceptTrade);
		gui.setItem(28, declineTrade);
		gui.setItem(30, readyOrNot);
		final ItemStack addCurrency = ItemTools.create(Material.GOLD_INGOT, 1, (short) 0, "add-currency", Arrays.asList("add-currency"));
		final ItemStack removeCurrency = ItemTools.create(Material.GOLD_INGOT, 1, (short) 0, "remove-currency", Arrays.asList("remove-currency"));
		gui.setItem(38, addCurrency);
		gui.setItem(39, removeCurrency);
		gui.setItem(40, divider);

		gui.openInventory(trade.getTrader(), trade.getTrader2());
	}
}

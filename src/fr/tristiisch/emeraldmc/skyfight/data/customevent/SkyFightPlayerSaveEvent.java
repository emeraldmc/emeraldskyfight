package fr.tristiisch.emeraldmc.skyfight.data.customevent;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayer;

public class SkyFightPlayerSaveEvent extends PlayerEvent {

	public static final HandlerList handlers = new HandlerList();

	final private SkyFightPlayer skyFightPlayer;

	public SkyFightPlayerSaveEvent(final Player who, final SkyFightPlayer skyFightPlayer) {
		super(who);
		this.skyFightPlayer = skyFightPlayer;
	}

	public SkyFightPlayer getSkyFightPlayer() {
		return this.skyFightPlayer;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}

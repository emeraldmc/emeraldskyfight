package fr.tristiisch.emeraldmc.skyfight.data.customevent;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayer;

public class SkyFightPlayerLoadedEvent extends PlayerEvent {

	public static final HandlerList handlers = new HandlerList();

	final private EmeraldPlayer emeraldPlayer;
	final private SkyFightPlayer skyFightPlayer;

	public SkyFightPlayerLoadedEvent(final Player who, final EmeraldPlayer emeraldPlayer, final SkyFightPlayer skyFightPlayer) {
		super(who);
		this.emeraldPlayer = emeraldPlayer;
		this.skyFightPlayer = skyFightPlayer;
	}

	public EmeraldPlayer getEmeraldPlayer() {
		return this.emeraldPlayer;
	}

	public SkyFightPlayer getSkyFightPlayer() {
		return this.skyFightPlayer;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}

package fr.tristiisch.emeraldmc.skyfight.data;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Color;

import com.google.gson.Gson;

import fr.tristiisch.emeraldmc.skyfight.data.enderchest.SkyFightEnderChest;
import fr.tristiisch.emeraldmc.skyfight.data.inventory.SkyFightInventory;

public class SkyFightPlayer {

	public static SkyFightPlayer fromJson(final String s) {
		if(s == null) {
			return new SkyFightPlayer();
		}
		return new Gson().fromJson(s, SkyFightPlayer.class);
	}

	private Color armorColor;
	private int death;
	private SkyFightEnderChest enderChest;
	private SkyFightInventory inventory;
	private int kill;
	private final Map<String, Long> kitsCooldown = new HashMap<>();
	private PlayerInfo playerInfo;

	private int xp;

	public SkyFightPlayer() {
		this.kill = 0;
		this.death = 0;
		this.xp = 0;
	}

	public SkyFightPlayer(final int kill, final int death, final int xp) {
		this.kill = kill;
		this.death = death;
		this.xp = xp;
	}

	public void addDeath() {
		this.addDeath(1);
	}

	public void addDeath(final int death) {
		this.death += death;
	}

	public void addKill() {
		this.addKill(1);
	}

	public void addKill(final int kill) {
		this.kill += kill;
	}

	public void addXP() {
		this.addXP(1);
	}

	public void addXP(final int xp) {
		this.xp += xp;
	}

	public int getDeath() {
		return this.death;
	}

	public SkyFightEnderChest getEnderChest() {
		return this.enderChest;
	}

	public SkyFightInventory getInventory() {
		return this.inventory;
	}

	public int getKD() {
		return this.kill / this.death;
	}

	public int getKill() {
		return this.kill;
	}

	public Map<String, Long> getKitsCooldown() {
		return this.kitsCooldown;
	}

	public int getLevel() {
		return this.xp / 50 + 1;
	}

	public PlayerInfo getPlayerInfo() {
		return this.playerInfo;
	}

	public int getXP() {
		return this.xp;
	}

	public void resetDeath() {
		this.death = 0;
	}

	public void resetKill() {
		this.kill = 0;
	}

	public void resetXP() {
		this.xp = 0;
	}

	public void setEnderChest(final SkyFightEnderChest enderChest) {
		this.enderChest = enderChest;
	}

	public void setInventory(final SkyFightInventory inventory) {
		this.inventory = inventory;
	}

	public void setKitsCooldown(final String kitName, final Long time) {
		this.kitsCooldown.put(kitName, time);
	}

	public void setPlayerInfo(final PlayerInfo playerInfo) {
		this.playerInfo = playerInfo;
	}

	public String toJson() {
		return new Gson().toJson(this);
	}

	public Color getArmorColor() {
		return armorColor;
	}

	public void setArmorColor(Color armorColor) {
		this.armorColor = armorColor;
	}
}

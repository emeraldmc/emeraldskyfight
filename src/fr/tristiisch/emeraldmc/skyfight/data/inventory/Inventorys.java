package fr.tristiisch.emeraldmc.skyfight.data.inventory;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class Inventorys {

	public static Map<UUID, Inventory> playersInventory = new HashMap<>();
	public static Map<UUID, UUID> playersInventoryViewer = new HashMap<>();

	public static void closeInventory(final Player player) {
		closeInventory(getInventory(player));
	}

	public static Inventory getInventory(final Player player) {
		return getInventory(player.getUniqueId());
	}

	public static Inventory getInventory(final UUID playerUUID) {
		return playersInventory.get(playerUUID);
	}

	public static void closeInventory(final Inventory inventaire) {
		inventaire.getViewers().forEach(entity -> {
			entity.closeInventory();
			playersInventoryViewer.remove(entity.getUniqueId());
		});
	}

	public static void removeInventory(final Player player) {
		playersInventory.remove(player.getUniqueId());
	}

	public static void openInventory(final Player player) {
		openInventory(player, player);
	}

	public static void openInventory(final Player player, final Player target) {
		openInventory(player, target.getUniqueId(), getInventory(target));
	}

	public static void openInventory(final Player player, final UUID targetUUID) {
		openInventory(player, targetUUID, getInventory(targetUUID));
	}

	public static void openInventory(final Player player, final UUID targetUUID, final Inventory targetInventory) {
		player.openInventory(targetInventory);
		playersInventoryViewer.put(player.getUniqueId(), targetUUID);
	}

	public static UUID getPlayerLookInventory(final Player player) {
		return playersInventoryViewer.get(player.getUniqueId());
	}

	public static Inventory getLookInventory(final Player player) {
		return playersInventory.get(getPlayerLookInventory(player));
	}
}

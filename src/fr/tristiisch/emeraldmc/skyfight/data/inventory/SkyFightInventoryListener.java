package fr.tristiisch.emeraldmc.skyfight.data.inventory;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;

import fr.tristiisch.emeraldmc.api.spigot.core.invsee.OpenInventoryCommandEvent;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayer;
import fr.tristiisch.emeraldmc.skyfight.data.customevent.SkyFightPlayerSaveEvent;
import fr.tristiisch.emeraldmc.skyfight.data.enderchest.EnderChest;

public class SkyFightInventoryListener implements Listener {


	@EventHandler
	public void onOpenInventoryCommand(final OpenInventoryCommandEvent event) {

	}

	@EventHandler
	public void onSkyFightPlayerSave(final SkyFightPlayerSaveEvent event) {
		final Player player = event.getPlayer();
		final SkyFightPlayer skyFightPlayer = event.getSkyFightPlayer();
		final SkyFightInventory skyFightInventory = skyFightPlayer.getInventory();

		final Inventory enderChest = EnderChest.getEnderChest(player);

		skyFightInventory.save(enderChest);
		if(enderChest.getViewers().isEmpty()) {
			EnderChest.removeEnderChest(player);
		}
	}
}

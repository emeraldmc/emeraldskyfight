package fr.tristiisch.emeraldmc.skyfight.data.inventory;

import java.io.IOException;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.spigot.utils.InventoryUtils;

public class SkyFightInventory {

	private String items;

	public SkyFightInventory(final ItemStack[] items) {
		this.setItems(items);
	}

	public ItemStack[] getItems() {
		try {
			return InventoryUtils.itemStackArrayFromBase64(this.items);
		} catch(final IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void setItems(final ItemStack[] items) {
		this.items = InventoryUtils.itemStackArrayToBase64(items);
	}

	public void save(final Inventory inventory) {
		this.setItems(inventory.getContents());
	}
}

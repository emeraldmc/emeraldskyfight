package fr.tristiisch.emeraldmc.skyfight.data;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.skyfight.mysql.MySQLSkyFight;

public class SkyFightPlayers {

	private static Map<UUID, SkyFightPlayer> players = new HashMap<>();

	public static void addPlayer(final UUID uuid, final SkyFightPlayer skyfightPlayer) {
		players.put(uuid, skyfightPlayer);
	}

	public static void removePlayer(final UUID uuid) {
		players.remove(uuid);
	}

	public static void removePlayer(final Player player) {
		players.remove(player.getUniqueId());
	}

	public static SkyFightPlayer getPlayer(final Player player) {
		return getPlayer(player.getUniqueId());
	}

	public static SkyFightPlayer getPlayer(final UUID uuid) {
		return players.get(uuid);
	}

	public static Map<UUID, SkyFightPlayer> getPlayers() {
		return players;
	}

	public static SkyFightPlayer getPlayerInfo(final UUID uuid) {
		final SkyFightPlayer skyFightPlayer = getPlayer(uuid);
		return skyFightPlayer != null ? skyFightPlayer : MySQLSkyFight.getPlayer(uuid);
	}
}

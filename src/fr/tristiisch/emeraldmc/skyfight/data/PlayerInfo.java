package fr.tristiisch.emeraldmc.skyfight.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.google.common.primitives.Ints;
import com.google.gson.Gson;

import fr.tristiisch.emeraldmc.api.spigot.utils.InventoryUtils;

public class PlayerInfo {

	/*private Location location;*/
	private final int level;
	private float exp;
	private String[] inventoryBase64;
	private long firstPlayed;
	private long lastPlayed;

	public PlayerInfo(final Player player) {
		/*this.location = player.getLocation();*/
		this.exp = player.getExp();
		this.level = player.getLevel();
		this.setInventory(player.getInventory());
		if(player.hasPlayedBefore()) {
			this.firstPlayed = player.getFirstPlayed();
			this.lastPlayed = player.getLastPlayed();
		}
	}

	public final void clearInventory() {
		this.inventoryBase64 = null;
	}

	public final void setInventory(final PlayerInventory inventory) {
		this.inventoryBase64 = InventoryUtils.playerInventoryToBase64(inventory);
	}

	/*	public final void setLocation(final Location location) {
			this.location = location;
		}

		public final Location getLocation() {
			return this.location;
		}*/

	public final void setExp(final float exp) {
		this.exp = exp;
	}

	public final float getExp() {
		return this.exp;
	}

	public long getFirstPlayed() {
		return this.firstPlayed;
	}

	public long getLastPlayed() {
		return this.lastPlayed;
	}

	public final void setToPlayer(final Player player) {
		/* inventoryOld.setArmorContents(this.getInventory().get); */
		if(this.inventoryBase64 != null) {
			InventoryUtils.setPlayerInventory(player, this.inventoryBase64);
		}
		player.setLevel(this.level);
		player.setExp(this.exp);
	}

	@Override
	public final String toString() {
		return new Gson().toJson(this);
	}

	public static PlayerInfo fromJson(final String json) {
		return new Gson().fromJson(json, PlayerInfo.class);
	}

	public static class JsonItemStack {

		private final String material;
		private final String name;
		private final List<String> lore;
		private final Map<String, Long> enchantments = new HashMap<>();
		private final long amount;

		public JsonItemStack(final ItemStack item) {
			this.material = item.getType().name();
			final ItemMeta meta = item.getItemMeta();
			this.name = meta.getDisplayName();
			this.lore = meta.getLore();
			for(final Entry<Enchantment, Integer> entry : meta.getEnchants().entrySet()) {
				this.enchantments.put(entry.getKey().getName(), Long.valueOf(entry.getValue()));
			}
			this.amount = item.getAmount();
		}

		public JsonItemStack(final String material, final String name, final List<String> lore, final HashMap<String, Long> enchantments, final Long amount) {
			this.material = material;
			this.name = name;
			this.lore = lore;
			if(enchantments != null) {
				this.enchantments.putAll(enchantments);
			}
			this.amount = amount == null ? 1L : amount;
		}

		@SuppressWarnings("unchecked")
		public final String toJson() {
			final JSONObject object = new JSONObject();
			object.put("material", this.material);
			object.put("name", this.name);
			object.put("lore", this.lore);
			object.put("enchantments", this.enchantments);
			object.put("amount", this.amount);
			return object.toJSONString();
		}

		@SuppressWarnings("unchecked")
		public static JsonItemStack fromJson(final String jsonItemStack) {
			final JSONObject array = (JSONObject) JSONValue.parse(jsonItemStack);
			return new JsonItemStack(
					(String) array.get("material"),
					(String) array.get("name"),
					(List<String>) array.get("lore"),
					(HashMap<String, Long>) array.get("enchantments"),
					(Long) array.get("amount"));
		}

		public final ItemStack toItemStack() {
			final ItemStack itemStack = new ItemStack(this.material == null ? Material.GRASS : Material.valueOf(this.material));
			final ItemMeta meta = itemStack.getItemMeta();
			boolean applyMeta = false;
			if(this.name != null) {
				meta.setDisplayName(this.name);
				applyMeta = true;
			}
			if(this.lore != null) {
				meta.setLore(this.lore);
				applyMeta = true;
			}
			if(this.enchantments.size() != 0) {
				for(final Entry<String, Long> entry : this.enchantments.entrySet()) {
					meta.addEnchant(Enchantment.getByName(entry.getKey()), Ints.checkedCast(entry.getValue()), true);
				}
				applyMeta = true;
			}
			itemStack.setItemMeta(meta);
			if(applyMeta) {
				itemStack.setAmount(Ints.checkedCast(this.amount));
			}
			return itemStack;
		}

	}

}
package fr.tristiisch.emeraldmc.skyfight.data;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.core.clear.Clear;
import fr.tristiisch.emeraldmc.api.spigot.customevent.AsyncEmeraldPlayerLoadEvent;
import fr.tristiisch.emeraldmc.skyfight.data.customevent.SkyFightPlayerLoadedEvent;
import fr.tristiisch.emeraldmc.skyfight.data.customevent.SkyFightPlayerSaveEvent;
import fr.tristiisch.emeraldmc.skyfight.mysql.MySQLSkyFight;

public class SkyFightPlayerListener implements Listener {

	/*	@EventHandler
		public void AsyncPlayerPreLoginEvent(final AsyncPlayerPreLoginEvent event) {
			if(SkyFightPlayers.getPlayer(event.getUniqueId()) != null) {
				event.disallow(Result.KICK_OTHER, "Reconnection trop rapide ...");
			}
		}*/

	@EventHandler
	public void PlayerJoinEvent(final PlayerJoinEvent event) {
		final Player player = event.getPlayer();
		Clear.player(player);
		player.setExp(0);
	}

	@EventHandler
	public void AsyncEmeraldPlayerLoad(final AsyncEmeraldPlayerLoadEvent event) {
		final Player player = event.getPlayer();

		final SkyFightPlayer skyFightPlayer = MySQLSkyFight.getPlayer(player.getUniqueId());
		if(skyFightPlayer.getPlayerInfo() != null) {
			skyFightPlayer.getPlayerInfo().setToPlayer(player);
		}
		final EmeraldPlayer emeraldPlayer = event.getEmeraldPlayer();

		final SkyFightPlayerLoadedEvent skyFightPlayerLoadedEvent = new SkyFightPlayerLoadedEvent(player, emeraldPlayer, skyFightPlayer);
		Bukkit.getPluginManager().callEvent(skyFightPlayerLoadedEvent);

		SkyFightPlayers.addPlayer(player.getUniqueId(), skyFightPlayer);
	}

	@EventHandler
	public void PlayerQuitEvent(final PlayerQuitEvent event) {
		/*TaskManager.runTaskLater(() -> {*/
		final Player player = event.getPlayer();
		final SkyFightPlayer skyFightPlayer = SkyFightPlayers.getPlayer(player.getUniqueId());
		if(skyFightPlayer == null) {
			return;
		}
		skyFightPlayer.setPlayerInfo(new PlayerInfo(player));

		final SkyFightPlayerSaveEvent skyFightPlayerSavEvent = new SkyFightPlayerSaveEvent(player, skyFightPlayer);
		Bukkit.getPluginManager().callEvent(skyFightPlayerSavEvent);

		MySQLSkyFight.savePlayer(player.getUniqueId(), skyFightPlayer);
		SkyFightPlayers.removePlayer(player.getUniqueId());
		/*}, 10);*/
		/*
		 * TaskManager.runTask(() -> { final File file = new
		 * File(Bukkit.getServer().getWorlds().get(0).getWorldFolder(), "playerdata/" +
		 * player.getUniqueId() + ".dat"); file.delete(); });
		 */
	}
}

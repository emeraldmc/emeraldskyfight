package fr.tristiisch.emeraldmc.skyfight.data.enderchest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.sql.MySQL;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.core.endersee.OpenEnderChestCommandEvent;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayer;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayers;
import fr.tristiisch.emeraldmc.skyfight.data.customevent.SkyFightPlayerLoadedEvent;
import fr.tristiisch.emeraldmc.skyfight.data.customevent.SkyFightPlayerSaveEvent;
import fr.tristiisch.emeraldmc.skyfight.group.SkyFightGroup;
import fr.tristiisch.emeraldmc.skyfight.mysql.MySQLSkyFight;

public class SkyFightEnderChestListener implements Listener {

	@EventHandler
	public void onSkyFightPlayerLoad(final SkyFightPlayerLoadedEvent event) {
		final Player player = event.getPlayer();
		final EmeraldPlayer emeraldPlayer = event.getEmeraldPlayer();
		final SkyFightPlayer skyFightPlayer = event.getSkyFightPlayer();

		final SkyFightGroup sfGroup = SkyFightGroup.get(emeraldPlayer.getGroup());
		final int size;
		if(sfGroup != null) {
			size = sfGroup.getEnderSize() * 9;
		} else {
			size = 3 * 9;
		}
		if(skyFightPlayer.getEnderChest() == null) {
			skyFightPlayer.setEnderChest(new SkyFightEnderChest(player.getEnderChest().getContents(), size));
		} else {
			final Inventory enderChest = EnderChest.getEnderChest(player);
			if(enderChest != null) {
				skyFightPlayer.getEnderChest().save(enderChest);
				return;
			}
			skyFightPlayer.getEnderChest().setSize(size);
			final List<ItemStack> items2 = new ArrayList<>();

			items2.addAll(Arrays.asList(skyFightPlayer.getEnderChest().getItems()));

			while(items2.size() > size) {
				final int index = items2.size() - 1;
				final ItemStack item = items2.get(index);
				if(item != null) {
					final PlayerInventory playerInventory = player.getInventory();
					if(SpigotUtils.hasEnoughPlace(playerInventory, item)) {
						playerInventory.addItem(item);
					} else {
						player.getWorld().dropItem(player.getEyeLocation(), item);
					}
				}
				items2.remove(index);
			}
			skyFightPlayer.getEnderChest().setItems(items2.toArray(new ItemStack[items2.size()]));
		}
		EnderChest.createInventory(player, skyFightPlayer.getEnderChest());
	}

	@EventHandler
	public void onSkyFightPlayerSave(final SkyFightPlayerSaveEvent event) {
		final Player player = event.getPlayer();
		final SkyFightPlayer skyFightPlayer = event.getSkyFightPlayer();
		final SkyFightEnderChest skyFightEnderChest = skyFightPlayer.getEnderChest();

		final Inventory enderChest = EnderChest.getEnderChest(player);

		skyFightEnderChest.save(enderChest);
		if(enderChest.getViewers().isEmpty()) {
			EnderChest.removeEnderChest(player);
		}
	}

	@EventHandler
	public void onPlayerInteractEvent(final PlayerInteractEvent event) {
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock().getType() == Material.ENDER_CHEST) {
			event.setCancelled(true);
			final Player player = event.getPlayer();
			final SkyFightPlayer skyFightPlayer = SkyFightPlayers.getPlayer(player);
			if(skyFightPlayer == null || skyFightPlayer.getEnderChest() == null) {
				player.sendMessage(Utils.color("&2EmeraldMC &7» &cUne erreur est survenu lors du chargement de votre enderchest."));
				return;
			}
			EnderChest.openEnderChest(player);
		}
	}

	@EventHandler
	public void onCustomOpenEnder(final OpenEnderChestCommandEvent event) {
		event.setCancelled(true);
		final Player player = event.getPlayer();
		if(event.isTargetConnected()) {
			final Player target = event.getTarget();
			EnderChest.openEnderChest(player, target);

		} else {
			String targetName = event.getTargetName();

			final EmeraldPlayer emeraldTarget = MySQL.getPlayer(targetName);
			if(emeraldTarget == null) {
				@SuppressWarnings("deprecation")
				final OfflinePlayer targetOffline = Bukkit.getOfflinePlayer(targetName);
				targetName = targetOffline != null ? targetOffline.getName() : targetName;
				player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &4%player%&c n'a jamais rejoint le serveur.".replaceAll("%player%", targetName)));
				return;
			}
			final SkyFightPlayer skyFightTarget = MySQLSkyFight.getPlayer(emeraldTarget.getUniqueId());
			final SkyFightEnderChest skyFightEnderChest = skyFightTarget.getEnderChest();
			if(skyFightEnderChest == null) {
				player.sendMessage(Utils.color("&2EmeraldMC &7» &cLe joueur &4%player%&c n'a pas rejoint le serveur depuis longtemps.".replaceAll("%player%", targetName)));
				return;
			}

			EnderChest.createInventory(emeraldTarget.getUniqueId(), skyFightEnderChest);
			EnderChest.openEnderChest(player, emeraldTarget.getUniqueId());
		}
	}

	@EventHandler
	public void onInventoryClose(final InventoryCloseEvent event) {
		final Player player = (Player) event.getPlayer();
		final Inventory enderChestClose = event.getInventory();
		if(enderChestClose.getType() != InventoryType.CHEST) {
			return;
		}

		final UUID targetUUID = EnderChest.getPlayerLookEnderChest(player);
		if(Bukkit.getPlayer(targetUUID) == null && enderChestClose.getViewers().isEmpty()) {
			final SkyFightPlayer skyFightTarget = MySQLSkyFight.getPlayer(targetUUID);
			skyFightTarget.getEnderChest().save(enderChestClose);
			MySQLSkyFight.savePlayer(targetUUID, skyFightTarget);
			EnderChest.removeEnderChest(player);
		}
	}
}


/*
		@EventHandler
		public void onInventoryOpen(final InventoryOpenEvent event) {
			final Player player = (Player) event.getPlayer();
			if(event.getInventory().getType() == InventoryType.ENDER_CHEST) {
				event.setCancelled(true);
				final SkyFightPlayer skyFightPlayer = SkyFightPlayers.getPlayer(player);
				if(skyFightPlayer == null || skyFightPlayer.getEnderChest() == null) {
					player.sendMessage(Utils.color("&2EmeraldMC &7» &cUne erreur est survenu lors du chargement de votre enderchest."));
					return;
				}
				skyFightPlayer.getEnderChest().openToPlayer(player);
			}
		}*/


/*	@EventHandler
		public void onGuiClose(final GuiCloseEvent event) {

			final Player player = event.getPlayer();
			final Inventory customEnderChest = event.getInventoryCloseEvent().getInventory();

			if(customEnderChest == null) {
				return;
			}

			final GuiData guiData = GuiTools.getGuiData(player);
			if(guiData != null) {
				switch(guiData.getId()) {
				case "SkyfightEnderChest":
					final SkyFightPlayer skyFightPlayer = SkyFightPlayers.getPlayer(player);
					skyFightPlayer.getEnderChest().setItems(customEnderChest.getContents());
				}
			}
		}*/


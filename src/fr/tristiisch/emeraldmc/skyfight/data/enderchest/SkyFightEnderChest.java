package fr.tristiisch.emeraldmc.skyfight.data.enderchest;

import java.io.IOException;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.spigot.utils.InventoryUtils;

public class SkyFightEnderChest {

	private String items;
	private int size;

	public SkyFightEnderChest(final ItemStack[] items, final int size) {
		this.setItems(items);
		this.size = size;
	}

	public ItemStack[] getItems() {
		try {
			return InventoryUtils.itemStackArrayFromBase64(this.items);
		} catch(final IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void setItems(final ItemStack[] items) {
		this.items = InventoryUtils.itemStackArrayToBase64(items);
	}

	public int getSize() {
		return this.size;
	}

	public void setSize(final int size) {
		this.size = size;
	}

	public void save(final Inventory inventory) {
		this.setItems(inventory.getContents());
	}
}

package fr.tristiisch.emeraldmc.skyfight.data.enderchest;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;

public class EnderChest {

	public static Map<UUID, Inventory> playersEnderChest = new HashMap<>();
	public static Map<UUID, UUID> playersEnderChestViewer = new HashMap<>();

	public static void createInventory(final Player player, final SkyFightEnderChest skyFightEnderChest) {
		createInventory(player.getUniqueId(), skyFightEnderChest);
	}

	public static void createInventory(final UUID playerUUID, final SkyFightEnderChest skyFightEnderChest) {

		final GuiTools gui = new GuiTools("&2SkyFight &7- &aEnderchest", "SkyfightEnderChest", skyFightEnderChest.getSize());
		gui.setItem(skyFightEnderChest.getItems());
		final Inventory inventory = gui.getInventory();

		playersEnderChest.put(playerUUID, inventory);
	}

	public static void closeEnderChest(final Player player) {
		closeEnderChest(getEnderChest(player));
	}

	public static Inventory getEnderChest(final Player player) {
		return getEnderChest(player.getUniqueId());
	}

	public static Inventory getEnderChest(final UUID playerUUID) {
		return playersEnderChest.get(playerUUID);
	}

	public static void closeEnderChest(final Inventory inventaire) {
		inventaire.getViewers().forEach(entity -> {
			entity.closeInventory();
			playersEnderChestViewer.remove(entity.getUniqueId());
		});
	}

	public static void removeEnderChest(final Player player) {
		playersEnderChest.remove(player.getUniqueId());
	}

	public static void openEnderChest(final Player player) {
		openEnderChest(player, player);
	}

	public static void openEnderChest(final Player player, final Player target) {
		openEnderChest(player, target.getUniqueId(), getEnderChest(target));
	}

	public static void openEnderChest(final Player player, final UUID targetUUID) {
		openEnderChest(player, targetUUID, getEnderChest(targetUUID));
	}

	public static void openEnderChest(final Player player, final UUID targetUUID, final Inventory targetEnderChest) {
		player.openInventory(targetEnderChest);
		playersEnderChestViewer.put(player.getUniqueId(), targetUUID);
	}

	public static UUID getPlayerLookEnderChest(final Player player) {
		return playersEnderChestViewer.get(player.getUniqueId());
	}

	public static Inventory getLookEnderChest(final Player player) {
		return playersEnderChest.get(getPlayerLookEnderChest(player));
	}
}

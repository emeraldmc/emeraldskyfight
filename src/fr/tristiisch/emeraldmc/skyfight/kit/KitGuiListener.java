package fr.tristiisch.emeraldmc.skyfight.kit;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools.GuiData;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;
import fr.tristiisch.emeraldmc.skyfight.group.SkyFightGroup;

public class KitGuiListener implements Listener {

	@EventHandler
	private void InventoryClickEvent(final InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();
		final Inventory clickedInventory = event.getClickedInventory();
		if(clickedInventory == null || clickedInventory.getType() != InventoryType.CHEST) {
			return;
		}
		final ItemStack currentItem = event.getCurrentItem();

		final GuiData guiData = GuiTools.getGuiData(player);
		if(guiData != null) {
			switch(guiData.getId()) {
			case "kits":
				event.setCancelled(true);
				if(currentItem.getItemMeta() == null) {
					return;
				}
				final String currentItemName = ChatColor.stripColor(currentItem.getItemMeta().getDisplayName());
				final SkyFightGroup skyFightGroup = Arrays.stream(SkyFightGroup.values()).filter(v -> v.getName().equals(currentItemName)).findFirst().orElse(null);
				if(skyFightGroup == null) {
					return;
				}
				if(event.getClick() == ClickType.RIGHT) {
					Kits.openKit(player, skyFightGroup);
				} else {
					player.closeInventory();
					TaskManager.runTaskAsynchronously(() -> {
						Kits.giveKit(player, player, new AccountProvider(player.getUniqueId()).getEmeraldPlayer(), skyFightGroup.getName());
					});
				}
				break;
			case "kit":
				event.setCancelled(true);
				if(currentItem.isSimilar(ItemTools.create(Material.BARRIER, 1, (short) 0, "&7Retour"))) {
					player.closeInventory();
					Kits.openMenu(player);
				}
			}
		}
	}
}

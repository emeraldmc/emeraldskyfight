package fr.tristiisch.emeraldmc.skyfight.kit;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;
import fr.tristiisch.emeraldmc.skyfight.group.SkyFightGroup;

public class SkyFightKit {

	/*
	 * EMERAUDE(EmeraldGroup.EMERAUDE, 86400,
	 * ItemTools.create(Material.DIAMOND_HELMET, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3)),
	 * ItemTools.create(Material.DIAMOND_CHESTPLATE, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3)),
	 * ItemTools.create(Material.DIAMOND_LEGGINGS, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3)),
	 * ItemTools.create(Material.DIAMOND_BOOTS, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3)),
	 * ItemTools.create(Material.DIAMOND_SWORD, new
	 * ItemEnchant(Enchantment.DAMAGE_ALL, 3)),
	 * ItemTools.create(Material.FISHING_ROD), ItemTools.create(Material.GOLD_SPADE,
	 * new ItemEnchant(Enchantment.KNOCKBACK)) ), DIAMANT(EmeraldGroup.DIAMANT,
	 * 86400, ItemTools.create(Material.DIAMOND_HELMET, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2)),
	 * ItemTools.create(Material.DIAMOND_CHESTPLATE, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2)),
	 * ItemTools.create(Material.DIAMOND_LEGGINGS, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2)),
	 * ItemTools.create(Material.DIAMOND_BOOTS, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2)),
	 * ItemTools.create(Material.DIAMOND_SWORD, new
	 * ItemEnchant(Enchantment.DAMAGE_ALL, 2)),
	 * ItemTools.create(Material.FISHING_ROD), ItemTools.create(Material.GOLD_SPADE,
	 * new ItemEnchant(Enchantment.KNOCKBACK)) ), SAPHIR(EmeraldGroup.SAPHIR, 86400,
	 * ItemTools.create(Material.DIAMOND_HELMET, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
	 * ItemTools.create(Material.DIAMOND_CHESTPLATE, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
	 * ItemTools.create(Material.DIAMOND_LEGGINGS, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
	 * ItemTools.create(Material.DIAMOND_BOOTS, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
	 * ItemTools.create(Material.DIAMOND_SWORD, new
	 * ItemEnchant(Enchantment.DAMAGE_ALL, 1)),
	 * ItemTools.create(Material.FISHING_ROD), ItemTools.create(Material.GOLD_SPADE,
	 * new ItemEnchant(Enchantment.KNOCKBACK)) ), RUBIS(EmeraldGroup.RUBIS, 86400,
	 * ItemTools.create(Material.IRON_HELMET, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
	 * ItemTools.create(Material.IRON_CHESTPLATE, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
	 * ItemTools.create(Material.IRON_LEGGINGS, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
	 * ItemTools.create(Material.IRON_BOOTS, new
	 * ItemEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)),
	 * ItemTools.create(Material.IRON_SWORD, new ItemEnchant(Enchantment.DAMAGE_ALL,
	 * 1)), ItemTools.create(Material.GOLD_SPADE, new
	 * ItemEnchant(Enchantment.KNOCKBACK)) );
	 */

	final private int cooldown;
	final private ItemStack[] items;

	public SkyFightKit(final int cooldown, final ItemStack... items) {
		this.cooldown = cooldown;
		this.items = items;
	}

	public int getCooldown() {
		return this.cooldown;
	}

	public ItemStack[] getItems() {
		return this.items;
	}

	public boolean giveItems(final Player player) {
		/* Inventory inventory = player.getInventory(); */
		final SkyFightGroup skyfightGroup = SkyFightGroup.get(this);
		if(SpigotUtils.hasEnoughPlace(player.getInventory(), this.items)) {

			player.getInventory().addItem(this.items);
			player.sendMessage(Utils.color("&2EmeraldMC &7» &aVous avez reçu votre kit &2" + skyfightGroup.getName() + "&a."));
			return true;
		} else {

			player.sendMessage(Utils.color("&2EmeraldMC &7» &cVous n'avez pas assez de place pour le kit &2" + skyfightGroup.getName() + "&c."));
			return false;
		}
	}
}

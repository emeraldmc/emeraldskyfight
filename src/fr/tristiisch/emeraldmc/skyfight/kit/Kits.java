package fr.tristiisch.emeraldmc.skyfight.kit;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.gui.GuiTools;
import fr.tristiisch.emeraldmc.api.spigot.items.ItemTools;
import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;
import fr.tristiisch.emeraldmc.api.spigot.utils.CustomConfigUtils;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;
import fr.tristiisch.emeraldmc.skyfight.Main;
import fr.tristiisch.emeraldmc.skyfight.group.SkyFightGroup;

public class Kits {


	static {
		CustomConfigUtils.createIfDoesNotExist(Main.getInstance(), "kit");
	}

	public static void setPlayerCoolDown(final Player player, final SkyFightGroup skyFightGroup, final long time) {
		final FileConfiguration config = CustomConfigUtils.getConfig(Main.getInstance(), "kit");
		final String uuid = String.valueOf(player.getUniqueId());
		config.set(uuid + "." + skyFightGroup.getName(), time);
		CustomConfigUtils.saveConfig(Main.getInstance(), config, "kit");
	}

	public static void removePlayerCoolDown(final Player player, final SkyFightGroup skyFightGroup) {
		final FileConfiguration config = CustomConfigUtils.getConfig(Main.getInstance(), "kit");
		final String uuid = String.valueOf(player.getUniqueId());
		config.set(uuid + "." + skyFightGroup.getName(), null);
		CustomConfigUtils.saveConfig(Main.getInstance(), config, "kit");
	}

	public static long getPlayerCoolDown(final Player player, final SkyFightGroup skyFightGroup) {
		final FileConfiguration config = CustomConfigUtils.getConfig(Main.getInstance(), "kit");
		final String uuid = String.valueOf(player.getUniqueId());
		return config.getInt(uuid + "." + skyFightGroup.getName());
	}

	public static void giveKit(final CommandSender sender, final Player target, final EmeraldPlayer emeraldTarget, final String kitName) {
		final SkyFightGroup skyfightGroup = Arrays.stream(SkyFightGroup.values()).filter(v -> v.getName().toLowerCase().equalsIgnoreCase(kitName)).findFirst().orElse(null);
		if(skyfightGroup == null) {
			sender.sendMessage(Utils.color("&2EmeraldMC &7» &cCe kit n'existe pas"));
			return;
		}
		final SkyFightKit kits = skyfightGroup.getSkyfightKit();
		if(emeraldTarget.hasPowerLessThan(skyfightGroup.getEmeraldGroup())) {
			sender.sendMessage(
					Utils.color("&2EmeraldMC &7» &cPour avoir le kit &b" + skyfightGroup.getName() + "&c, vous devez avoir le grade &4" + skyfightGroup.getEmeraldGroup()
					.getName() + "&c ou plus."));
			return;
		}

		final long kitCooldown = Kits.getPlayerCoolDown(target, skyfightGroup);
		if(kitCooldown != 0 && kitCooldown > Utils.getCurrentTimeinSeconds() && !emeraldTarget.hasPower(EmeraldGroup.ADMIN)) {
			sender.sendMessage(Utils.color("&2EmeraldMC &7» &cVous devez patientez encore " + Utils.timestampToDuration(kitCooldown) + " pour le kit " + skyfightGroup.getName()));
			return;
		}

		if(kits.giveItems(target)) {
			Kits.setPlayerCoolDown(target, skyfightGroup, Utils.getCurrentTimeinSeconds() + kits.getCooldown());
		}
	}


	public static void openMenu(final Player player) {

		TaskManager.runTaskAsynchronously(() -> {
			final EmeraldPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			final GuiTools gui = new GuiTools("&2Kits", "kits", SkyFightGroup.values().length * 2);
			int i = gui.getFirstSlot() + 1;
			for(final SkyFightGroup skyFightGroup : SkyFightGroup.values()) {
				final long kitCooldown = getPlayerCoolDown(player, skyFightGroup);
				String message;
				if(emeraldPlayer.hasPowerLessThan(skyFightGroup.getEmeraldGroup())) {
					message = "&4➤ &cAchete le grade " + skyFightGroup.getEmeraldGroup().getColor() + skyFightGroup.getName() + "&csur la boutique.";
				} else {
					if(kitCooldown == 0 || kitCooldown < Utils.getCurrentTimeinSeconds()) {
						message = "&2➤ &aClic gauche pour prendre le kit";
					} else {
						message = "&4➤ &cPatiente encore &4" + Utils.timestampToDuration(kitCooldown) + "&c.";
					}

				}

				final ItemStack itemStack = ItemTools.create(Material.STAINED_GLASS_PANE,
						1,
						SpigotUtils.getDataValueByChatColor(skyFightGroup.getEmeraldGroup().getColor()),
						skyFightGroup.getEmeraldGroup().getColor() + skyFightGroup.getName(),
						"",
						message,
						"",
						"&8➤ &7Clic droit pour voir le contenu",
						"",
						"&8➤ &7Delai:&8 " + Utils.timestampToDuration(Utils.getCurrentTimeinSeconds() + skyFightGroup.getSkyfightKit().getCooldown()));
				gui.setItem(i, itemStack);
				i += 2;
			}
			gui.openInventory(player);
		});
	}

	public static void openKit(final Player player, final SkyFightGroup skyFightGroup) {
		final SkyFightKit kit = skyFightGroup.getSkyfightKit();
		final String kitName = skyFightGroup.getName();
		final GuiTools gui = new GuiTools("&2Kit " + skyFightGroup.getEmeraldGroup().getColor() + kitName, "kit", 9 * 3);
		gui.setItem(kit.getItems());
		gui.setItem(gui.getLastSlot(), ItemTools.create(Material.BARRIER, 1, (short) 0, "&7Retour"));
		gui.openInventory(player);
	}
}

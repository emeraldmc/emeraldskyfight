package fr.tristiisch.emeraldmc.skyfight.kit;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.skyfight.group.SkyFightGroup;

public class KitCommand implements CommandExecutor, TabCompleter {

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			Player player = null;
			EmeraldPlayer emeraldPlayer = null;
			if(sender instanceof Player) {
				player = (Player) sender;
				emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			}

			if(args.length == 0) {
				if(player != null) {
					Kits.openMenu(player);
				}
			} else {
				Kits.giveKit(sender, player, emeraldPlayer, args[0]);
			}
		});
		return true;
	}

	@Override
	public List<String> onTabComplete(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		final String check = args[args.length - 1].toLowerCase();
		if(args.length == 0) {
			return Arrays.stream(SkyFightGroup.values()).map(v -> v.getName().toLowerCase()).collect(Collectors.toList());
		} else if(args.length == 1) {
			return Arrays.stream(SkyFightGroup.values()).map(v -> v.getName().toLowerCase()).filter(kitName -> kitName.startsWith(check)).collect(Collectors.toList());
		} else {
			return Arrays.asList("");
		}
	}
}

package fr.tristiisch.emeraldmc.skyfight.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import fr.tristiisch.emeraldmc.api.spigot.customevent.AsyncEmeraldPlayerChangeEvent;
import fr.tristiisch.emeraldmc.skyfight.scoreboard.Scoreboards;

public class GroupListener implements Listener {

	@EventHandler
	public void AsyncEmeraldPlayerChange(final AsyncEmeraldPlayerChangeEvent event) {
		final Player player = event.getPlayer();
		Scoreboards.updateScoreboards(player, event.getEmeraldPlayer());
	}
}

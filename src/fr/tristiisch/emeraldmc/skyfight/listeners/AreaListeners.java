package fr.tristiisch.emeraldmc.skyfight.listeners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import fr.tristiisch.emeraldmc.api.spigot.title.ActionBar;
import fr.tristiisch.emeraldmc.api.spigot.utils.EmeraldArea;
import fr.tristiisch.emeraldmc.api.spigot.utils.LocationUtils;
import fr.tristiisch.emeraldmc.skyfight.Main;

public class AreaListeners implements Listener {

	private static List<EmeraldArea> areas = new ArrayList<>();
	private static Map<Player, EmeraldArea> playerAreas = new HashMap<>();

	public AreaListeners() {
		final FileConfiguration config = Main.getInstance().getConfig();
		final ConfigurationSection section = config.getConfigurationSection("area");
		int i = 0;
		for(final String key : section.getKeys(false)) {
			i++;
			final EmeraldArea area = new EmeraldArea(i, key, LocationUtils.getCuboid(section, key));
			area.setPvp(section.getBoolean(key + ".pvp"));
			area.setUseEnderpearlTo(section.getBoolean(key + ".useEnderpearlTo"));
			area.setUseEnderpearlFrom(section.getBoolean(key + ".useEnderpearlFrom"));
			area.setTakeDamage(section.getBoolean(key + ".takedamage"));
			areas.add(area);
		}
	}

	public static boolean isInArea(final Player player, final String areaName) {
		final EmeraldArea area = isInArea(player);
		return area != null && area.getName().equalsIgnoreCase(areaName);
	}

	public static EmeraldArea isInArea(final Player player) {
		if(playerAreas.containsKey(player)) {
			return playerAreas.get(player);
		}
		return null;
	}

	public static boolean isLocationSafe(final Location loc) {
		for(final EmeraldArea area : areas) {
			if(area.getCuboid().isIn(loc) && !area.canPvp()) {
				return false;
			}
		}
		return true;
	}

	public void checkArea(final Player player, final Location loc, final boolean withmsg) {
		final boolean ck = playerAreas.containsKey(player);
		for(final EmeraldArea area : areas) {
			if(area.getCuboid().isIn(loc)) {
				if(!ck || playerAreas.get(player).getId() != area.getId()) {
					if(withmsg && (!ck || playerAreas.get(player).canPvp() != area.canPvp())) {
						ActionBar.sendActionBar(player, "§aZone sans §2PVP");
					}
					playerAreas.put(player, area);
				}
				return;
			}
		}
		if(playerAreas.containsKey(player)) {
			if(withmsg && (!ck || !playerAreas.get(player).canPvp())) {
				ActionBar.sendActionBar(player, "§cZone avec §4PVP");
			}
			playerAreas.remove(player);
		}
	}

	@EventHandler
	public void PlayerJoinEvent(final PlayerJoinEvent event) {
		final Player player = event.getPlayer();
		this.checkArea(player, player.getLocation(), false);
	}

	@EventHandler
	public void PlayerMoveEvent(final PlayerMoveEvent event) {
		final Player player = event.getPlayer();
		this.checkArea(player, event.getTo(), true);
	}

	@EventHandler
	public void PlayerTeleportEvent(final PlayerTeleportEvent event) {
		final Player player = event.getPlayer();
		this.checkArea(player, event.getTo(), true);
	}

	@EventHandler
	public void EntityShootBowEvent(final EntityShootBowEvent event) {
		if(!(event.getEntity() instanceof Player)) {
			return;
		}
		final Player player = (Player) event.getEntity();
		final EmeraldArea area = isInArea(player);
		if(area != null && !area.canPvp()) {
			event.getProjectile().remove();
			event.setCancelled(true);
			player.updateInventory();
		}
	}

	@EventHandler
	public void EntityDamageByEntityEvent(final EntityDamageByEntityEvent event) {
		if(event.isCancelled() || !(event.getEntity() instanceof Player)) {
			return;
		}
		if(!(event.getEntity() instanceof Player)) {
			return;
		}
		final Player victim = (Player) event.getEntity();

		Player attacker = null;
		if(event.getDamager() instanceof Player) {
			attacker = (Player) event.getDamager();

		} else if(event.getDamager() instanceof Projectile){
			final Projectile projectile = (Projectile) event.getDamager();
			if(projectile.getShooter() instanceof Player) {
				attacker = (Player) projectile.getShooter();
			}

		}
		if(attacker != null) {
			final EmeraldArea areaAttacker = isInArea(attacker);
			final EmeraldArea areaVictim = isInArea(victim);
			if(areaAttacker != null && !areaAttacker.canPvp() || areaVictim != null && !areaVictim.canPvp()) {
				attacker.playSound(attacker.getLocation(), Sound.VILLAGER_NO, 3, 0);
				event.setCancelled(true);
				return;
			}
		}
	}

	@EventHandler
	public void EntityDamageEvent(final EntityDamageEvent event) {
		if(event.isCancelled() || !(event.getEntity() instanceof Player)) {
			return;
		}
		final Player player = (Player) event.getEntity();
		if(event.getCause() != DamageCause.SUICIDE) {
			final EmeraldArea area = isInArea(player);
			if(area != null && !area.canTakeDamage()) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void ProjectileLaunchEvent(final ProjectileLaunchEvent event) {
		if(event.isCancelled()) {
			return;
		}
		final Projectile projectile = event.getEntity();
		if(!(projectile.getShooter() instanceof Player)) {
			return;
		}
		final Player player = (Player) projectile.getShooter();
		final EmeraldArea areaAttacker = isInArea(player);
		if((projectile.getType() == EntityType.SNOWBALL || projectile.getType() == EntityType.ARROW) && areaAttacker != null && !areaAttacker.canPvp()) {
			player.playSound(player.getLocation(), Sound.VILLAGER_NO, 3, 0);
			event.setCancelled(true);
			return;
		}
	}


}
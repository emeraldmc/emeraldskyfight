package fr.tristiisch.emeraldmc.skyfight.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.GameMode;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.Vector;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.customevent.AsyncEmeraldPlayerLoadEvent;
import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;
import fr.tristiisch.emeraldmc.api.spigot.title.Title;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;
import fr.tristiisch.emeraldmc.skyfight.fight.Stuff;
import fr.tristiisch.emeraldmc.skyfight.scoreboard.Scoreboards;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand.EnumClientCommand;

public class JoinListener implements Listener {

	public static void init(final Player player) {
		player.setGameMode(GameMode.ADVENTURE);
		if(player.getMaxHealth() != 20) {
			player.setMaxHealth(20);
			player.setHealth(20);
		}
		player.setFoodLevel(20);
		player.setWalkSpeed(0.2f);
		player.setAllowFlight(false);
		player.setFlySpeed(0.1f);
		player.setFlying(false);
		player.setCanPickupItems(true);
		player.setVelocity(new Vector(0, 0, 0));
		SpigotUtils.addNightVision(player);
	}

	@EventHandler
	public void AsyncEmeraldPlayerLoad(final AsyncEmeraldPlayerLoadEvent event) {
		final Player player = event.getPlayer();
		final EmeraldPlayer emeraldPlayer = event.getEmeraldPlayer();
		Bukkit.getScheduler().runTask(EmeraldSpigot.getInstance(), () -> Scoreboards.setScoreboards(player, emeraldPlayer));

		if(emeraldPlayer.hasPowerMoreThan(EmeraldGroup.RUBIS) && emeraldPlayer.hasPowerLessThan(EmeraldGroup.EMERAUDE)) {
			event.setJoinMessageFormat("&7[&a+&7] %prefix%%name%");
		}

		Stuff.give(player, emeraldPlayer);
		if(!player.hasPlayedBefore()) {
			TaskManager.runTask(() -> {
				final Firework fw = (Firework) player.getWorld().spawnEntity(player.getEyeLocation(), EntityType.FIREWORK);
				final FireworkMeta meta = fw.getFireworkMeta();
				meta.setPower(10);
				final FireworkEffect.Builder builder = FireworkEffect.builder();
				builder.withTrail()
						.flicker(false)
						.trail(false)
						.withFade(Color.GREEN, Color.WHITE, Color.YELLOW, Color.BLUE, Color.FUCHSIA, Color.PURPLE, Color.MAROON, Color.LIME, Color.ORANGE)
						.withColor(Color.GREEN)
						.with(FireworkEffect.Type.STAR);

				meta.addEffect(builder.build());
				fw.setFireworkMeta(meta);
			});
			Bukkit.broadcastMessage(Utils.color("&bBienvenue au SkyFight &3" + player.getName() + "&b."));
			player.sendMessage(Utils.color("&2EmeraldMC &7» &bBienvenue au SkyFight !"));
		}

	}

	@EventHandler
	public void PlayerJoinEvent(final PlayerJoinEvent event) {
		final Player player = event.getPlayer();
		if(player.isDead()) {
			TaskManager.runTaskLater(() -> {
				final PacketPlayInClientCommand packet = new PacketPlayInClientCommand(EnumClientCommand.PERFORM_RESPAWN);
				((CraftPlayer) player).getHandle().playerConnection.a(packet);
			}, 5);
		}

		player.teleport(EmeraldSpigot.getSpawn());
		Title.sendTitle(player, "&2SkyFight", "&6Trouve les panneaux !", 20, 60, 20);
		init(player);
	}

	@EventHandler
	public void PlayerQuitEvent(final PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		event.setQuitMessage(null);

		for(final Team team : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) {
			team.removeEntry(player.getName());
		}
		Scoreboards.removeScoreboard(player);

		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			final EmeraldPlayer emeraldPlayer = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
			if(emeraldPlayer.hasPowerMoreThan(EmeraldGroup.RUBIS) && emeraldPlayer.hasPowerLessThan(EmeraldGroup.EMERAUDE)) {
				Bukkit.broadcastMessage(Utils.color(EmeraldSpigot.getInstance()
						.getConfig()
						.getString("spigot.settings.quitmsg")
						.replaceAll("%prefix%", emeraldPlayer.getGroup().getPrefix())
						.replaceAll("%name%", player.getName())));
			}
		});

	}

	/*
	 * @EventHandler public void EmeraldPlayerLoad(final EmeraldPlayerLoad event) {
	 * final Player player = event.getPlayer(); final EmeraldPlayer emeraldPlayer =
	 * event.getEmeraldPlayer(); Scoreboards.setScoreboards(player);
	 * if(!event.getPlayer().hasPlayedBefore()) {
	 * event.getEmeraldPlayer().addPierres(50);
	 * Scoreboards.updateScoreboardSign(player, emeraldPlayer);
	 * EmeraldPlayers.save(emeraldPlayer); player.sendMessage(Utils.
	 * color("&2EmeraldMC &7» &bBienvenue au SkyFight ! Voici 50 pierres pour commencer, c'est cadeaux !"
	 * )); } }
	 */
}

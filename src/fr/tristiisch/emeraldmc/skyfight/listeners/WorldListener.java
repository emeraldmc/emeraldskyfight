package fr.tristiisch.emeraldmc.skyfight.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.CreeperPowerEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.StructureGrowEvent;

public class WorldListener implements Listener {

	@EventHandler
	public void onLeaveDecay(final LeavesDecayEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void onStructureSpawn(final StructureGrowEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void onCreepeurExplode(final CreeperPowerEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void ChunkLoadEvent(final ChunkLoadEvent event) {
		if(event.isNewChunk()) {

		}
	}
}

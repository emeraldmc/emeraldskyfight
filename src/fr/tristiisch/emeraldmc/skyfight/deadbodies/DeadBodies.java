package fr.tristiisch.emeraldmc.skyfight.deadbodies;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;

import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;
import fr.tristiisch.emeraldmc.api.spigot.utils.Reflection;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.ChatMessage;
import net.minecraft.server.v1_8_R3.DataWatcher;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.MathHelper;
import net.minecraft.server.v1_8_R3.PacketPlayOutBed;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityTeleport;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_8_R3.WorldSettings;


public class DeadBodies {


	public static void playFakeBed(final Player p) {
		final BlockPosition pos = new BlockPosition(p.getLocation().getBlockX(), 0, p.getLocation().getBlockZ());
		playFakeBed(p, pos);
	}

	public static DataWatcher clonePlayerDatawatcher(final Player player, final int currentEntId) {

		final EntityHuman h = new EntityHuman(
				((CraftWorld) player.getWorld()).getHandle(),

				((CraftPlayer) player).getProfile()) {

			@Override
			public void sendMessage(final IChatBaseComponent arg0) {

				return;

			}

			@Override
			public boolean a(final int arg0, final String arg1) {

				return false;

			}

			@Override
			public BlockPosition getChunkCoordinates() {

				return null;

			}

			@Override
			public boolean isSpectator() {

				return false;

			}

		};

		h.d(currentEntId);

		return h.getDataWatcher();

	}

	static int entityId = 0;

	@SuppressWarnings("deprecation")
	static void playFakeBed(final Player p, final BlockPosition pos) {

		final PacketPlayOutNamedEntitySpawn packetEntitySpawn = new PacketPlayOutNamedEntitySpawn();

		final CraftPlayer p1 = (CraftPlayer) p;

		final double locY = ((EntityHuman) p1.getHandle()).locY;

		final DataWatcher dw = clonePlayerDatawatcher(p, entityId);
		dw.watch(10, p1.getHandle().getDataWatcher().getByte(10));

		final GameProfile prof = new GameProfile(p1.getUniqueId(), p1.getName());

		final PacketPlayOutPlayerInfo packetInfo = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER);
		final PacketPlayOutPlayerInfo.PlayerInfoData data = packetInfo.new PlayerInfoData(prof, 0, WorldSettings.EnumGamemode.SURVIVAL, new ChatMessage("", new Object[0]));
		final List<PacketPlayOutPlayerInfo.PlayerInfoData> dataList = Lists.newArrayList();
		dataList.add(data);
		Reflection.setFieldValue(packetInfo, "b", dataList);

		Reflection.setFieldValue(packetEntitySpawn, "a", entityId);
		Reflection.setFieldValue(packetEntitySpawn, "b", prof.getId());
		Reflection.setFieldValue(packetEntitySpawn, "c", MathHelper.floor(((EntityHuman) p1.getHandle()).locX * 32D));
		Reflection.setFieldValue(packetEntitySpawn, "d", MathHelper.floor(locY * 32D));
		Reflection.setFieldValue(packetEntitySpawn, "e", MathHelper.floor(((EntityHuman) p1.getHandle()).locZ * 32D));
		Reflection.setFieldValue(packetEntitySpawn, "f", (byte) (int) (((EntityHuman) p1.getHandle()).yaw * 256.0F / 360.0F));
		Reflection.setFieldValue(packetEntitySpawn, "g", (byte) (int) (((EntityHuman) p1.getHandle()).pitch * 256.0F / 360.0F));
		Reflection.setFieldValue(packetEntitySpawn, "i", dw);

		final PacketPlayOutBed packetBed = new PacketPlayOutBed();

		Reflection.setFieldValue(packetBed, "a", entityId);
		Reflection.setFieldValue(packetBed, "b", pos);

		final PacketPlayOutEntityTeleport packetTeleport = new PacketPlayOutEntityTeleport();
		Reflection.setFieldValue(packetTeleport, "a", entityId);
		Reflection.setFieldValue(packetTeleport, "b", MathHelper.floor(((EntityHuman) p1.getHandle()).locX * 32.0D));
		Reflection.setFieldValue(packetTeleport, "c", MathHelper.floor(locY * 32.0D));
		Reflection.setFieldValue(packetTeleport, "d", MathHelper.floor(((EntityHuman) p1.getHandle()).locZ * 32.0D));
		Reflection.setFieldValue(packetTeleport, "e", (byte) (int) (((EntityHuman) p1.getHandle()).yaw * 256.0F / 360.0F));
		Reflection.setFieldValue(packetTeleport, "f", (byte) (int) (((EntityHuman) p1.getHandle()).pitch * 256.0F / 360.0F));
		Reflection.setFieldValue(packetTeleport, "g", true);

		final PacketPlayOutEntityTeleport packetTeleportDown = new PacketPlayOutEntityTeleport();
		Reflection.setFieldValue(packetTeleportDown, "a", entityId);
		Reflection.setFieldValue(packetTeleportDown, "b", MathHelper.floor(((EntityHuman) p1.getHandle()).locX * 32.0D));
		Reflection.setFieldValue(packetTeleportDown, "c", 0);
		Reflection.setFieldValue(packetTeleportDown, "d", MathHelper.floor(((EntityHuman) p1.getHandle()).locZ * 32.0D));
		Reflection.setFieldValue(packetTeleportDown, "e", (byte) (int) (((EntityHuman) p1.getHandle()).yaw * 256.0F / 360.0F));
		Reflection.setFieldValue(packetTeleportDown, "f", (byte) (int) (((EntityHuman) p1.getHandle()).pitch * 256.0F / 360.0F));
		Reflection.setFieldValue(packetTeleportDown, "g", true);

		TaskManager.runTaskLater(() -> {
			for(final Player player : Bukkit.getOnlinePlayers()) {
				final Location loc = p.getLocation().clone();
				player.sendBlockChange(loc.subtract(0, loc.getY(), 0), Material.BED_BLOCK, (byte) 0);

				final CraftPlayer pl = (CraftPlayer) player;
				if(player != p) {
					pl.getHandle().playerConnection.sendPacket(packetInfo);
					pl.getHandle().playerConnection.sendPacket(packetEntitySpawn);
					pl.getHandle().playerConnection.sendPacket(packetTeleportDown);
					pl.getHandle().playerConnection.sendPacket(packetBed);
					pl.getHandle().playerConnection.sendPacket(packetTeleport);
				}
			}
			dataList.clear();

			DeadBodies.entityId++;

		}, 5 * 20);

	}
}

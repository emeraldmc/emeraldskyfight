package fr.tristiisch.emeraldmc.skyfight;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.core.build.BuildListener;
import fr.tristiisch.emeraldmc.api.spigot.core.clear.ClearCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.commands.HatCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.endersee.EnderseeCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.listeners.CancelFoodListener;
import fr.tristiisch.emeraldmc.api.spigot.core.teleport.Teleport;
import fr.tristiisch.emeraldmc.api.spigot.utils.CustomConfigUtils;
import fr.tristiisch.emeraldmc.skyfight.anvil.AnvilListener;
import fr.tristiisch.emeraldmc.skyfight.armor.ArmorCommand;
import fr.tristiisch.emeraldmc.skyfight.armor.ArmorListener;
import fr.tristiisch.emeraldmc.skyfight.blockuse.BlockUseListener;
import fr.tristiisch.emeraldmc.skyfight.box.BoxCommand;
import fr.tristiisch.emeraldmc.skyfight.box.BoxListener;
import fr.tristiisch.emeraldmc.skyfight.customcraft.CustomCraft;
import fr.tristiisch.emeraldmc.skyfight.data.SkyFightPlayerListener;
import fr.tristiisch.emeraldmc.skyfight.data.enderchest.SkyFightEnderChestListener;
import fr.tristiisch.emeraldmc.skyfight.enchant.EnchantListener;
import fr.tristiisch.emeraldmc.skyfight.enderpearl.EnderPearlListener;
import fr.tristiisch.emeraldmc.skyfight.fakeinventory.FakeInventoryCommand;
import fr.tristiisch.emeraldmc.skyfight.fakeinventory.FakeInventoryListener;
import fr.tristiisch.emeraldmc.skyfight.fight.CombatCommand;
import fr.tristiisch.emeraldmc.skyfight.fight.DamageEvent;
import fr.tristiisch.emeraldmc.skyfight.fight.DeadListener;
import fr.tristiisch.emeraldmc.skyfight.fight.PreventDisconnectFightListener;
import fr.tristiisch.emeraldmc.skyfight.guild.GuildCreate;
import fr.tristiisch.emeraldmc.skyfight.guild.GuildListener;
import fr.tristiisch.emeraldmc.skyfight.guild.GuildUtils;
import fr.tristiisch.emeraldmc.skyfight.guild.GuildeCommand;
import fr.tristiisch.emeraldmc.skyfight.guild.commonchest.GuildCommonChestListener;
import fr.tristiisch.emeraldmc.skyfight.guild.listener.PlayerGuildJoinListener;
import fr.tristiisch.emeraldmc.skyfight.guild.pnj.GuildPNJ;
import fr.tristiisch.emeraldmc.skyfight.guild.pnj.GuildPNJListener;
import fr.tristiisch.emeraldmc.skyfight.kit.KitCommand;
import fr.tristiisch.emeraldmc.skyfight.kit.KitGuiListener;
import fr.tristiisch.emeraldmc.skyfight.listeners.AreaListeners;
import fr.tristiisch.emeraldmc.skyfight.listeners.GroupListener;
import fr.tristiisch.emeraldmc.skyfight.listeners.JoinListener;
import fr.tristiisch.emeraldmc.skyfight.listeners.WorldListener;
import fr.tristiisch.emeraldmc.skyfight.scoreboard.ScoreboardListener;
import fr.tristiisch.emeraldmc.skyfight.sign.SignCommand;
import fr.tristiisch.emeraldmc.skyfight.sign.SignListener;
import fr.tristiisch.emeraldmc.skyfight.stats.StatsCommand;
import fr.tristiisch.emeraldmc.skyfight.stockender.StockEnderListener;
import fr.tristiisch.emeraldmc.skyfight.trade.TradeCommand;
import fr.tristiisch.emeraldmc.skyfight.trade.TradeListener;
import fr.tristiisch.emeraldmc.skyfight.villager.ShopCommand;
import fr.tristiisch.emeraldmc.skyfight.villager.VillagerAPI;
import fr.tristiisch.emeraldmc.skyfight.villager.VillagerCommand;
import fr.tristiisch.emeraldmc.skyfight.villager.VillagerListener;
import fr.tristiisch.emeraldmc.skyfight.zap.ZapListener;
import fr.tristiisch.emeraldmc.skyfight.zap.ZapListener2;

public class Main extends JavaPlugin {

	public static Main instance;

	public static Main getInstance() {
		return instance;
	}

	@Override
	public void onDisable() {
		Bukkit.getServer().getScoreboardManager().getMainScoreboard().getTeams().forEach(t -> t.unregister());
		GuildUtils.saveGuilds();
		final ConsoleCommandSender console = Bukkit.getConsoleSender();
		console.sendMessage(Utils.color("&c" + this.getDescription().getName() + " (V" + this.getDescription().getVersion() + ") &cest désactivé."));
	}

	@Override
	public void onEnable() {
		instance = this;

		// Configs
		CustomConfigUtils.createIfDoesNotExist(this, "config");

		final PluginManager pluginmanager = this.getServer().getPluginManager();

		// Gestion Commands
		this.getCommand("sign").setExecutor(new SignCommand(75));
		this.getCommand("villager").setExecutor(new VillagerCommand(75));
		this.getCommand("kit").setExecutor(new KitCommand());
		this.getCommand("guilde").setExecutor(new GuildeCommand());
		this.getCommand("trade").setExecutor(new TradeCommand());
		this.getCommand("shop").setExecutor(new ShopCommand());
		this.getCommand("combat").setExecutor(new CombatCommand());
		this.getCommand("stats").setExecutor(new StatsCommand());
		this.getCommand("fakeinventory").setExecutor(new FakeInventoryCommand());
		new BoxCommand(this, EmeraldGroup.RESPMODO);
		new ArmorCommand(this, EmeraldGroup.RUBIS);

		// Gestion Events
		pluginmanager.registerEvents(new PreventDisconnectFightListener(), this);
		pluginmanager.registerEvents(new SkyFightPlayerListener(), this);
		pluginmanager.registerEvents(new SkyFightEnderChestListener(), this);
		pluginmanager.registerEvents(new AreaListeners(), this);
		pluginmanager.registerEvents(new ZapListener(), this);
		pluginmanager.registerEvents(new ZapListener2(), this);
		pluginmanager.registerEvents(new CancelFoodListener(), this);
		pluginmanager.registerEvents(new JoinListener(), this);
		pluginmanager.registerEvents(new WorldListener(), this);
		pluginmanager.registerEvents(new DeadListener(), this);
		pluginmanager.registerEvents(new DamageEvent(), this);
		pluginmanager.registerEvents(new GroupListener(), this);
		pluginmanager.registerEvents(new EnderPearlListener(), this);
		pluginmanager.registerEvents(new SignListener(), this);
		pluginmanager.registerEvents(new GuildListener(), this);
		pluginmanager.registerEvents(new TradeListener(), this);
		pluginmanager.registerEvents(new EnchantListener(), this);
		pluginmanager.registerEvents(new VillagerListener(), this);
		pluginmanager.registerEvents(new GuildPNJListener(), this);
		pluginmanager.registerEvents(new GuildCreate(), this);
		pluginmanager.registerEvents(new PlayerGuildJoinListener(), this);
		pluginmanager.registerEvents(new ScoreboardListener(), this);
		pluginmanager.registerEvents(new GuildCommonChestListener(), this);
		pluginmanager.registerEvents(new KitGuiListener(), this);
		pluginmanager.registerEvents(new FakeInventoryListener(), this);
		pluginmanager.registerEvents(new AnvilListener(), this);
		pluginmanager.registerEvents(new BlockUseListener(), this);
		pluginmanager.registerEvents(new BoxListener(), this);
		pluginmanager.registerEvents(new StockEnderListener(), this);
		pluginmanager.registerEvents(new ArmorListener(), this);

		Teleport.setStrictMode(true);
		BuildListener.disableCancelDrop();
		CustomCraft.init(this);
		ClearCommand.setGroup(EmeraldGroup.RUBIS);
		EnderseeCommand.setOwnEcGroup(EmeraldGroup.RUBIS);
		HatCommand.setPower(EmeraldGroup.SAPHIR);

		EmeraldSpigot.setSpawn(CustomConfigUtils.getLocation(Main.getInstance(), "config", "spawn"));
		this.getServer().setDefaultGameMode(GameMode.ADVENTURE);

		for(final Player player : Bukkit.getOnlinePlayers()) {
			JoinListener.init(player);
		}

		Bukkit.getScheduler().runTaskLater(this, () -> {
			VillagerAPI.loadVillagers();
			GuildPNJ.init();
		}, 10 * 20);

		final ConsoleCommandSender console = Bukkit.getConsoleSender();
		console.sendMessage(Utils.color("&a" + this.getDescription().getName() + " (V" + this.getDescription().getVersion() + ") est activé."));
	}

}
